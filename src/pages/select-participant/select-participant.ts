import { Component } from '@angular/core';
import { App, NavController, NavParams, ToastController, Platform, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FunctProvider } from '../../providers/funct/funct';
import { GlobalProvider } from '../../providers/global/global';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-select-participant',
  templateUrl: 'select-participant.html',
})
export class SelectParticipantPage {

  rootNavCtrl: NavController;
  cont: any = [];
  groupedContacts: any = [];
  selectedContacts: any = [];
  participant: any;
  group: any = [];
  // userData: any;
  isLoading: any;
  nores: any;
  UserList: any = [];
  tempUserList: any = [];
  groupsyskey: any = [];
  isgroup: any = "";
  isenabled: boolean = true;
  imgLink: any;
  tempuser: any;
  searchText: any;
  registerData = {
    syskey: "", phone: "", username: "", realname: "", stateCaption: "",
    photo: "", fathername: "", nrc: "", id: "", passbook: "",
    address: "", dob: "", city: "", t1: "", t2: "", t3: "",
    t4: "", t5: "", state: "", status: "", n1: "",
    n2: "", n3: "", n4: "", n5: ""
  };
  dataResult: string = "";
  ipaddress: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public storage: Storage, 
    private toastCtrl: ToastController,
    public http: Http, 
    public funct: FunctProvider,
    public platform: Platform, 
    public global: GlobalProvider,
    public events: Events, 
    public appCtrl: App
  ) {
    this.ipaddress = this.global.ipaddressChat;

    // this.rootNavCtrl = navParams.get('rootNavCtrl');
    this.participant = navParams.get('data');
    this.imgLink = this.global.digitalMediaProfile;
    ////console.log("participant =" + JSON.stringify(this.participant));

    this.storage.get('uari').then((result) => {
      // if (result != null) {
      this.registerData = result;

      // } 
      // else {
      //   this.getRegister();
      // }
      // });
      // this.events.subscribe('uari', result => {
      //   if (result != null) {
      //     this.registerData = result;

      //   } 
      //   // else {
      //   //   this.getRegister();
      //   // }
      // });

      // this.storage.get('userData').then((data) => {
      // //console.log("userData = " + JSON.stringify(data));
      // this.userData = data;

      if (this.participant != undefined) {
        this.participant.select = true;
        if (this.participant.person == undefined) {
          this.participant.person = [];
          let array = { syskey: '', t2: '', select: true, t18: '' };
          array.syskey = this.participant.syskey;
          array.t2 = this.participant.t2;
          array.t18 = this.participant.t18;
          this.participant.person.push(array);
          let array1 = { syskey: '', t2: '', select: true, t18: '' };
          array1.syskey = this.registerData.syskey;
          /*
          accountNo:"0020101100028762"
          balance:"0.00"
          field1:""
          field2:""
          frominstituteCode:""
          institutionName:""
          name:"Phyu phyu"
          region:"MANDALAY/YANGON"
          sKey:"14789"
          sessionID:"853948111825"
          t16:"user-icon.png"
          userID:"+959965990870"
           */
          array1.t2 = this.registerData.username;
          array1.t18 = this.registerData.photo;
          this.groupsyskey.push(array1);
        }
      }
      else {
        this.participant = { person: [] };
        let array1 = { syskey: '', t2: '', select: true, t18: '' };
        array1.syskey = this.registerData.syskey;
        array1.t2 = this.registerData.username;
        array1.t18 = this.registerData.photo;
        this.groupsyskey.push(array1);
        this.selectedContacts.push(array1);
      }

      if (this.participant.person.length > 0) {

        if (this.participant.person.length > 2) {
          this.isgroup = this.participant.channelkey;
          for (let i = 0; i < this.participant.person.length; i++) {
            let array = { syskey: '', t2: '', select: '', t18: '' };
            array.syskey = this.participant.person[i].syskey;
            array.t2 = this.participant.person[i].t2;
            array.t18 = this.participant.person[i].t18;
            if (this.participant.person[i].syskey != this.registerData.syskey) {
              array.select = this.participant.select;
              this.selectedContacts.push(array);
            }
          }
        }
        else {
          this.isgroup = "";
          for (let i = 0; i < this.participant.person.length; i++) {
            let array = { syskey: '', t2: '', select: true, t18: '' };
            array.syskey = this.participant.person[i].syskey;
            array.t2 = this.participant.person[i].t2;
            array.t18 = this.participant.person[i].t18;
            this.groupsyskey.push(array);
            if (this.participant.person[i].syskey != this.registerData.syskey) {
              array.select = this.participant.select;
              this.selectedContacts.push(array);
            }
          }
        }
      }
      /*  else{
           let array = {syskey:'',t2:'',select:true};
           array.syskey = this.participant.syskey;
           array.t2 = this.participant.t2;
           this.groupsyskey.push(array);
         }*/
      ////console.log("participant =" + JSON.stringify(this.participant));
      this.getChatUser();
    });
  }

  // getRegister() {
  //   let parameter = {
  //     hautosys: this.hautosys,
  //     t7: this.global.region
  //   };

  //   //console.log('getRegister param in register:' + JSON.stringify(parameter));

  //   this.http.post(this.ipaddress + '/service001/getRegister', parameter).map(res => res.json()).subscribe(result => {
  //     //console.log("getRegister param in register success or error:" + JSON.stringify(result));

  //     if (result != null && result.messageCode != null && result.messageCode == "0000") {
  //       this.registerData.syskey = result.syskey;
  //       this.registerData.phone = result.userID;
  //       this.registerData.username = result.userName;
  //       this.registerData.realname = result.t3;
  //       this.registerData.stateCaption = result.t7;
  //       this.registerData.photo = result.t16;
  //       this.registerData.fathername = result.t20;
  //       this.registerData.nrc = result.t21;
  //       this.registerData.id = result.t22;
  //       this.registerData.passbook = result.t23;
  //       this.registerData.address = result.t24;
  //       this.registerData.dob = result.t25;
  //       this.registerData.city = result.t8;
  //       this.registerData.t1 = result.t26;
  //       this.registerData.t2 = result.t27;
  //       this.registerData.t3 = result.t28;
  //       this.registerData.t4 = result.t29;
  //       this.registerData.t5 = result.t30;
  //       this.registerData.state = result.t36;
  //       this.registerData.status = result.t38;
  //       this.registerData.n1 = result.t76;
  //       this.registerData.n2 = result.t77;
  //       this.registerData.n3 = result.t78;
  //       this.registerData.n4 = result.t79;
  //       this.registerData.n5 = result.t80;

  //       this.storage.set("uari", this.registerData);
  //       this.events.publish("uari", this.registerData);
  //     } else {
  //       this.storage.set("uari", null);
  //       this.events.publish("uari", null);
  //     }
  //   },
  //     error => {
  //       this.storage.set("uari", null);
  //       this.events.publish("uari", null);

  //       this.getError(error);
  //     });
  // }

  getItems(ev) {
    var val = ev.target.value;
    if (val == undefined || val.trim() == '') {
      val = '';
    } else {
      val = val.trim();
    }
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.UserList = this.tempUserList.filter((users) => {
        return (users.t1.toLowerCase().indexOf(val.toLowerCase()) > -1) || (users.t2.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

      if (this.UserList.length == 0) {
        this.dataResult = "no";
      } else {
        this.dataResult = "yes";
      }
    } else {
      this.UserList = this.tempUserList.filter((users) => {
        return (users.t1.toLowerCase().indexOf(val.toLowerCase()) > -1) || (users.t2.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

      if (this.UserList.length == 0) {
        this.dataResult = "no";
      } else {
        this.dataResult = "yes";
      }
    }
  }

  getChatUser() {
    this.storage.get('uari').then((result) => {
      this.registerData = result;

      let param = {
        userID: this.registerData.phone,
        sessionID: ''
      }
     // //console.log("request getContact =" + JSON.stringify(param));
      if (this.global.netWork == 'connected') {
        this.http.post(this.ipaddress + "/chatservice/getContact", param).map(res => res.json()).subscribe(result => {
         // //console.log("getContact=" + JSON.stringify(result));
          if (result.dataList != undefined && result.dataList != null && result.dataList != "") {
            let tempArray = [];
            if (!Array.isArray(result.dataList)) {
              tempArray.push(result.dataList);
              result.dataList = tempArray;
            }

            if (result.dataList.length > 0) {
              let dataloop = [];
              for (let x = 0; x < result.dataList.length; x++) {
                let array1 = { syskey: '', t2: '', select: false, t18: '', disable: false, t1: '' };
                array1.syskey = result.dataList[x].syskey;
                array1.t1 = result.dataList[x].phone;
                array1.t2 = result.dataList[x].name;
                array1.t18 = result.dataList[x].t18;
                dataloop.push(array1);
              }

              for (let i = 0; i < dataloop.length; i++) {
                if (this.participant.person.length > 0) {
                  for (let k = 0; k < this.participant.person.length; k++) {

                    if (this.participant.person[k].syskey == dataloop[i].syskey) {
                      dataloop[i].select = true;
                      dataloop[i].disable = true;
                    } else {
                      if (dataloop[i].disable == false) {
                        dataloop[i].select = false;
                        dataloop[i].disable = false;
                      }
                    }
                  }
                } else {
                  ////console.log("no person data");
                  if (this.participant.syskey != dataloop[i].syskey) {
                    dataloop[i].disable = false;
                    dataloop[i].select = false;
                  } else {
                    dataloop[i].select = true;
                    dataloop[i].disable = true;
                  }
                }

                for (let n = 0; n < this.selectedContacts.length; n++) {
                  if (dataloop[i].syskey == this.selectedContacts[n].syskey) {
                    dataloop[i].select = true;
                    dataloop[i].disable = true;
                  }
                }

                for (let p = 0; p < this.groupsyskey.length; p++) {
                  if (dataloop[i].syskey == this.groupsyskey[p].syskey) {
                    dataloop[i].select = true;
                    dataloop[i].disable = true;
                  }
                }

                this.UserList.push(dataloop[i]);
                this.tempUserList.push(dataloop[i]);
              }

              this.dataResult = "yes";
              this.nores = 1;
            } else {
              this.dataResult = "no";
              this.nores = 0;
            }

            this.isLoading = false;
          }
        }, error => {
          ////console.log("signin error=" + error.status);
          this.dataResult = "no";
          this.nores = 0;
          this.isLoading = false;
          this.getError(error);
        });
      }
    });
  }

  /* getGroupData(d) {

    let sortedContacts = this.groupedContacts.sort(function (a, b) {
      if (a.t4 < b.t4) return -1;
      if (a.t4 > b.t4) return 1;
      return 0;
    });
    let group_to_values = this.groupedContacts.reduce(function (obj, item) {
      obj[item.t4] = obj[item.t4] || [];
      obj[item.t4].push(item);
      return obj;
    }, {});
    let groups = Object.keys(group_to_values).map(function (key) {
      return { t4: key, data: group_to_values[key] };
    });

    for (let i = 0; i < groups.length; i++) {

      for (let j = 0; j < groups[i].data.length; j++) {
        if (this.participant.person.length > 0) {
          for (let k = 0; k < this.participant.person.length; k++) {
            if (this.participant.person[k].syskey == groups[i].data[j].syskey) {
              groups[i].data[j].select = true;
              groups[i].data[j].disable = true;
              break;
            }
            else {
              groups[i].data[j].select = false;
              groups[i].data[j].disable = false;
            }
          }
        }
        else {
          if (this.participant.syskey != groups[i].data[j].syskey) {
            groups[i].data[j].disable = false;
            groups[i].data[j].select = false;
          }
          else {
            groups[i].data[j].select = true;
            groups[i].data[j].disable = true;
          }
        }

      }
    }
    for (let x = 0; x < groups.length; x++) {
      for (let y = 0; y < groups[x].data.length; y++) {
        for (let z = 0; z < this.selectedContacts.length; z++) {
          if (groups[x].data[y].syskey == this.selectedContacts[z].syskey) {
            groups[x].data[y].select = true;
            groups[x].data[y].disable = true;
          }
        }
      }
    }

    for (let x = 0; x < groups.length; x++) {
      for (let y = 0; y < groups[x].data.length; y++) {
        for (let z = 0; z < this.groupsyskey.length; z++) {
          if (groups[x].data[y].syskey == this.groupsyskey[z].syskey) {
            groups[x].data[y].select = true;
            groups[x].data[y].disable = false;
          }
        }
      }
    }
    this.UserList = groups;
    if (d == 1)
      this.tempuser = groups;


    //console.log("List=" + JSON.stringify(this.UserList));
  } */


  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    // this.loading.dismiss();
   // //console.log("Oops!");
  }

  select(name, skey, k) {
    ////console.log("will selected =" + JSON.stringify(this.UserList[k].select));
    if (this.UserList[k].select == false) {
      this.UserList[k].select = true;
      let array = { syskey: '', t2: '', select: '', t18: '' };
      array.syskey = this.UserList[k].syskey;
      array.t2 = this.UserList[k].t2;
      array.t18 = this.UserList[k].t18;
      array.select = this.UserList[k].select;
      this.selectedContacts.push(array);
      this.groupsyskey.push(array);
      //this.isenabled = false;
      //this.groupedContacts[k].t2=name;
      ////console.log("selected =" + JSON.stringify(this.selectedContacts));
      ////console.log("selected =" + JSON.stringify(this.groupsyskey));
      //.log("select check length=>" + this.selectedContacts.length);
      ////console.log("groupsyskey check length=>" + this.groupsyskey.length);

      if (this.selectedContacts.length <= 2 && this.groupsyskey.length <= 2) {
        this.isenabled = true;
      } else {
        this.isenabled = false;
      }
      // else if (this.selectedContacts.length > 2 && this.groupsyskey.length > 0) {
      //   this.isenabled = false;
      // }
    }
    else {
      this.UserList[k].select = false;

      for (let i = 0; i < this.selectedContacts.length; i++) {
        ////console.log(skey + "/" + this.selectedContacts[i].syskey)
        if (skey == this.selectedContacts[i].syskey) {
          this.selectedContacts.splice(i, 1);
        }
      }

      for (let j = 0; j < this.groupsyskey.length; j++) {
        ////console.log(skey + "/" + this.groupsyskey[j].syskey)
        if (skey == this.groupsyskey[j].syskey) {
          this.groupsyskey.splice(j, 1);
        }
      }

      ////console.log("select uncheck length=>" + this.selectedContacts.length);
      ////console.log("groupsyskey uncheck length=>" + this.groupsyskey.length);

      if (this.selectedContacts.length <= 2 && this.groupsyskey.length <= 2) {
        this.isenabled = true;
      } else {
        this.isenabled = false;
      }

      // if (this.selectedContacts.length == 1 && this.groupsyskey.length == 2) {
      //   this.isenabled = true;
      // }
      // else if (this.selectedContacts.length >= 2 && this.groupsyskey.length == 0) {
      //   this.isenabled = true;
      // } else {
      //   this.isenabled = false;
      // }
    }
  }

  createGroup() {
    if (!this.isenabled) {
      this.isLoading = true;
      let params = {
        chatData: this.groupsyskey,
        t17: this.isgroup,
        t2: this.participant.t2,
        t20: this.participant.isGroup
      }
      ////console.log("request msg =" + JSON.stringify(params));
      this.http.post(this.global.ipaddress3 + "serviceChat/groupChat", params).map(res => res.json()).subscribe(result => {
        ////console.log("response msg =" + JSON.stringify(result));
        if (result.state) {
          // if (this.isgroup != undefined && this.isgroup != null && this.isgroup != "" && this.isgroup.length != 0) {
          //   this.navCtrl.pop();
          // } else {
          //this.navCtrl.setRoot(ChatHomeTabsPage)
          let params = { tabIndex: 1, tabTitle: "Messages" };

          // this.navCtrl.setRoot(TabsPage, params).catch((err: any) => {
          //   //console.log(`Didn't set nav root: ${err}`);
          // });

          // this.navCtrl.push(Chat, {
          //   data: d,
          //   docData: this.docData,
          //   from: 'selectParticipantPage',
          // });

          this.appCtrl.getRootNav().setRoot(TabsPage, params);
          // }
          this.nores = 1;
        }
        else {
          this.nores = 0;
        }
        this.isLoading = false;
      }, error => {
        ////console.log("signin error=" + error.status);
        this.nores = 0;
        this.getError(error);
      });
    }
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      dismissOnPageChange: true,
    });
    toast.present();
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad SelectParticipantPage');
    //this.backButtonExit();
  }

  // backButtonExit() {
  //   this.platform.registerBackButtonAction(() => {
  //     //console.log("Active Page=" + this.navCtrl.getActive().name);
  //     this.navCtrl.pop();
  //   });
  // }
  onInput(ev: any) {
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.searchText = val;
    }
    else {
      this.UserList = [];
      ////console.log("this.tempuser=", this.tempuser)
      for (let i = 0; i < this.tempuser.length; i++) {
        this.UserList.push(this.tempuser[i]);
      }
      this.nores = 1;
      this.searchText = "";
    }

  }
}
