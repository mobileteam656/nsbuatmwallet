import { Component, ViewChild } from '@angular/core';
import { NavController, ToastController, Events, Platform, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyPopOverListPage } from '../my-pop-over-list/my-pop-over-list';
import { GlobalProvider } from '../../providers/global/global';
import { Login } from '../login/login';
import { Changefont } from '../changefont/changeFont';
import { Receipt } from '../receipt/receipt';
import { Http } from '@angular/http';
import { TopUpDetails } from '../top-up-details/top-up-details';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { UtilProvider } from '../../providers/util/util';

@Component({
  selector: 'page-top-up',
  templateUrl: 'top-up.html',
  providers: [CreatePopoverPage],
})
export class TopUp {

  @ViewChild('customNumber') customNumber: any;

  textEng: any = ["Top Up", "Amount", "Top Up", "Code", "Invalid Amount."];
  textMyan: any = ["Top Up", "ငွေပမာဏ", "Top Up", "Code", "Invalid Amount."];
  textData: string[] = [];
  font: string = '';
  passTemp1: any;
  passTemp2: any;
  passTemp3: any;
  dataValue: any = [];
  userData: any;
  amount: any;
  reference: string = '';
  ipaddress: string;
  popover: any;
  errormsg1: string = '';

  constructor(public util: UtilProvider, public http: Http,
    public storage: Storage, public events: Events,
    public global: GlobalProvider, public platform: Platform,
    public popoverCtrl: PopoverController, public changefont: Changefont,
    public toastCtrl: ToastController, public appCtrl: App,
    public navCtrl: NavController, public createPopover: CreatePopoverPage) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    this.storage.get('userData').then((userData) => {
      if (userData != null) {
        this.userData = userData;

        this.storage.get('ipaddressWallet').then((ip) => {
          if (ip !== undefined && ip !== "" && ip != null) {
            this.ipaddress = ip;
          } else {
            this.ipaddress = this.global.ipaddressWallet;
          }
        });
      }
    });

    this.events.subscribe('userData', userData => {
      if (userData != null) {
        this.userData = userData;
      }
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  /* isNumberKey(evt) {
    var charCode = evt.target.value;

    //console.log("key code" + charCode);

    if (charCode.includes("+"))
      return false;
    return true;
  } */

  /* keyUpChecker(ev) {
    let elementChecker: string;
    let format = new RegExp(/\+/);
    let formatTwo = new RegExp(/\-/);
    elementChecker = ev.target.value;
    if (format.test(elementChecker)) {
      this.amount = elementChecker.replace("+", "")
    }
    if (formatTwo.test(elementChecker)) {
      this.amount = elementChecker.replace("-", "")
    }
  } */

  /* keyUpChecker(ev) {
    let elementChecker = ev.key;
    if (elementChecker == "Unidentified") {
      let simple = this.amount;
      this.amount = simple.replace("+", "");
      this.customNumber.value = this.amount;
    }
  }

  checkOnChange(ev) {
    let elementChecker = ev._value;
    //this.customNumber.value = '';

    if (elementChecker.includes("+")) {
      this.amount = elementChecker.replace("+", "");
    }

    if (elementChecker.includes("-")) {
      this.amount = elementChecker.replace("-", "");
    }
  } */

  topup() {
    if (this.util.checkInputIsEmpty(this.amount) || this.util.checkNumberOrLetter(this.amount) ||
      this.util.checkAmountIsZero(this.amount) || this.util.checkAmountIsLowerThanZero(this.amount) ||
      this.util.checkStartZero(this.amount) || this.util.checkPlusSign(this.amount)) {
      this.errormsg1 = this.textData[4];
    } else {
      this.errormsg1 = "";
    }

    if (this.errormsg1 == "") {
      let userID1 = this.userData.userID;//this.userData.userID.replace(/\+/g, '');

      let param = {
        "toAcc": this.userData.accountNo,
        "amount": this.amount,
        "reference": this.reference,
        "userID": userID1,
        "sessionID": this.userData.sessionID,
        "sKey": this.userData.sKey,
        "transferType": "10",
        "bankCharges": "",
        "commissionCharges": "",
        "field1": "",
        "field2": ""
      };

      this.http.post(this.ipaddress + '/service001/topup', param).map(res => res.json()).subscribe(result => {
        if (result.messageDesc == "Invalid Amount.") {
          let toast = this.toastCtrl.create({
            message: result.messageDesc + " from server respnose",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
        } else {
          this.appCtrl.getRootNav().setRoot(TopUpDetails, {
            parameters: result
          });
        }
      },
        error => {
          /* ionic App error
           ............001) url link worng, not found method (404)
           ........... 002) server not response (500)
           ............003) cross fillter not open (403)
           ............004) server stop (-1)
           ............005) app lost connection (0)
           */
          let code;

          if (error.status == 404) {
            code = '001';
          } else if (error.status == 500) {
            code = '002';
          } else if (error.status == 403) {
            code = '003';
          } else if (error.status == -1) {
            code = '004';
          } else if (error.status == 0) {
            code = '005';
          } else if (error.status == 502) {
            code = '006';
          } else {
            code = '000';
          }

          let msg = "Can't connect right now. [" + code + "]";

          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
        });
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
