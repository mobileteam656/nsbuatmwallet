import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, Platform, Events, AlertController, ToastController, LoadingController, App, Item } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';
import { Contacts,ContactFieldType, ContactFindOptions } from '@ionic-native/contacts';
import { TabsPage } from '../tabs/tabs';
import { iterateListLike } from '@angular/core/src/change_detection/change_detection_util';

@Component({
  selector: 'page-contact-list',
  templateUrl: 'contact-list.html',
})
export class ContactListPage {
  selectedAll: boolean ;
  dataValue: boolean;
  errormsg: any;
  hidden: boolean;
  
  items: any;
  checkedItems:any=[];
  //add: any;
  textEng: any = ["Contact List","No Record Found!","Please select contact."];
  textMyan: any = ["ဆက်သွယ်ရန်စာရင်း","No Record Found!","အဆက်အသွယ်ရွေးချယ်ပါ။"];
  showFont: string[] = [];

  ipaddress: string='';
  userdata: any;

  contactObj :any ={
    code: '',
    desc: '',
    dataList: [
        {
            autokey: '',
            syskey: '',
            createdDate: '',
            modifiedDate: '',
            userID: '',
            sessionID: '',
            contactRef: '',
            phone: '',
            name: '',
            photo: '',
            type: '',
            status: '',
            t1: '',
            t2: '',
            t3: '',
            t4: '',
            t5: '',
            t6: '',
            t7: '',
            t8: '',
            t9: '',
            t10: '',
            t11: '',
            t12: '',
            t13: '',
            t14: '',
            t15: '',
            t16: '',
            t17: '',
            t18: '',
            t19: '',
            t20: '',
            n1: '',
            n2: '',
            n3: '',
            n4: '',
            n5: '',
            n6: '',
            n7: '',
            n8: '',
            n9: '',
            n10: '',
            n11: '',
            n12: '',
            n13: '',
            n14: '',
            n15: '',
            n16: '',
            n17: '',
            n18: '',
            n19: '',
            n20: '',
            channelkey: ''
        }]
      }

  isIndeterminate:boolean;
  masterCheck:boolean;
  //contacts: any;
  allContacts: any;
  name: any;
  phNo: any = [];
  phList: any = [];
  profileImageLink: any;
  contactName: any;
  contactPhno: any;
  contactPhone: any = [];
  contactList: any = [] ;
  loading: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public platform: Platform,
    public storage: Storage, public all: AllserviceProvider, public events: Events, public alertCtrl: AlertController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http,
    public global: GlobalProvider, public appCtrl: App,private contacts: Contacts) {     
      this.hidden = true;
      
      this.selectedAll=false;     
      this.storage.get('userData').then((data) => {
        this.userdata = data;
        this.ipaddress = this.global.ipaddress;
        // this.getContact();
        this.getContactsList();
      }); 
      
      this.storage.get('profileImage').then((profileImage) => {
        if (profileImage != undefined && profileImage != null && profileImage != '') {
          this.profileImageLink = profileImage;
        } else {
          this.profileImageLink = this.global.digitalMediaProfile;
        }
      });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
       // this.selectData();
        // this.getChatUser();
      }
      else {       
     
      }
    });  
   
  }

  ionViewDidLoad() {

  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  getContact(){  
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    this.contacts.find(["displayName", "phoneNumbers"], {multiple: true}).then((contacts) => {   
    this.allContacts = contacts;       
    
    for (let i = 0; i < this.allContacts.length; i++) { 
      //console.log("phnolengths >>" + JSON.stringify(this.allContacts[i].phoneNumbers));
      if(this.allContacts[i].phoneNumbers != null){ 
        for (let j=0; j < this.allContacts[i].phoneNumbers.length; j++){  
          
          this.allContacts[i].phoneNumbers[j].value = this.allContacts[i].phoneNumbers[j].value.toString().replace(/ +/g, "");
          if (this.allContacts[i].phoneNumbers[j].value.indexOf("7") == 0 && this.allContacts[i].phoneNumbers[j].value.length == "9") {
            this.allContacts[i].phoneNumbers[j].value = '+959' + this.allContacts[i].phoneNumbers[j].value;
          }
          else if (this.allContacts[i].phoneNumbers[j].value.indexOf("9") == 0 && this.allContacts[i].phoneNumbers[j].value.length == "9") {
            this.allContacts[i].phoneNumbers[j].value = '+959' + this.allContacts[i].phoneNumbers[j].value;
          }
          else if (this.allContacts[i].phoneNumbers[j].value.indexOf("+") != 0 && this.allContacts[i].phoneNumbers[j].value.indexOf("7") != 0 && this.allContacts[i].phoneNumbers[j].value.indexOf("9") != 0 && (this.allContacts[i].phoneNumbers[j].value.length == "8" || this.allContacts[i].phoneNumbers[j].value.length == "9" || this.allContacts[i].phoneNumbers[j].value.length == "7")) {
            this.allContacts[i].phoneNumbers[j].value = '+959' + this.allContacts[i].phoneNumbers[j].value;
          }
          else if (this.allContacts[i].phoneNumbers[j].value.indexOf("09") == 0 && (this.allContacts[i].phoneNumbers[j].value.length == 10 || this.allContacts[i].phoneNumbers[j].value.length == 11 || this.allContacts[i].phoneNumbers[j].value.length == 9)) {
            this.allContacts[i].phoneNumbers[j].value = '+959' + this.allContacts[i].phoneNumbers[j].value.substring(2);
          }
          else if (this.allContacts[i].phoneNumbers[j].value.indexOf("959") == 0 && (this.allContacts[i].phoneNumbers[j].value.length == 11 || this.allContacts[i].phoneNumbers[j].value.length == 12 || this.allContacts[i].phoneNumbers[j].value.length == 10)) {
            this.allContacts[i].phoneNumbers[j].value = '+959' + this.allContacts[i].phoneNumbers[j].value.substring(3);
          }
          //this.phNo = this.allContacts[i].phoneNumbers[j].value;
          this.phList.push({"userID": this.userdata.userID, "phone": this.allContacts[i].phoneNumbers[j].value, "name": "", "type": 1, "t3": 0 })           
             
         
          console.log("phNoList >>" + JSON.stringify(this.phList));
        }          
      }
    } 
    this.getContactsList();     
    });            
  
 }

  getContactsList(){ 
   let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, data: this.phList }
   this.http.post(this.ipaddress + '/chatservice/getContactListByPhoneNo', param).map(res => res.json()).subscribe(data => {
   let code = data.code;
  console.log("response data >>" + JSON.stringify(data));
   if (code == "0000") {
    this.hidden = false;
    this.contactObj = data; 
    this.loading.dismiss();  
   }
   else if (code == "0016") {
    this.loading.dismiss();
     let confirm = this.alertCtrl.create({
       title: 'Warning!',
       enableBackdropDismiss: false,
       buttons: [
         {
           text: 'OK',
           handler: () => {
             this.storage.remove('userData');
             this.events.publish('login_success', false);
             this.events.publish('lastTimeLoginSuccess', '');            
             this.navCtrl.popToRoot();
           }
         }
       ]
     })
     confirm.present();    
   }
   else {
     this.hidden = true; 
     this.loading.dismiss();   
     let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: this.showFont[1],
      buttons: [{
        text: 'OK',
        handler: () => {       
        }
      }]
    });
    alert.present();

     /*let toast = this.toastCtrl.create({
       message: this.showFont[1],
       duration: 3000,
       position: 'bottom',
       dismissOnPageChange: true,
     });
     toast.present(toast);*/
   }
 });  
   
}

checkAll(){ 
  this.checkedItems =  this.contactObj.dataList.forEach(item => {
    item.isChecked = true;
  });  
}

checkMaster() {  
  setTimeout(()=>{
    this.contactObj.dataList.forEach(item => {      
      item.isChecked = this.masterCheck;
    });
  });
}

checkEvent() {
  const totalItems = this.contactObj.dataList.length;
  let checked = 0;
  this.contactObj.dataList.map(item => {
    if (item.isChecked) checked++;
  });
  if (checked > 0 && checked < totalItems) {
    //If even one item is checked but not all
    this.isIndeterminate = true;
    this.masterCheck = false;
  } else if (checked == totalItems) {
    //If all are checked
    this.masterCheck = true;
    this.isIndeterminate = false;
  } else {
    //If none is checked
    this.isIndeterminate = false;
    this.masterCheck = false;
  }
}

addContact(){
  if(this.selectedAll==false)
  {
    this.errormsg = this.showFont[2];
  }
  this.showAlert();
}

showAlert() {
  let alert = this.alertCtrl.create({
    title: '',
    enableBackdropDismiss: false,
    message: "Contact added successfully",
    buttons: [{
      text: 'OK',
      handler: () => {
         this.navCtrl.pop();
      }
    }]
  });

  alert.present();
}


getCheckedvalue () {  
  this.checkedItems =  this.contactObj.dataList.filter(value => {   
    return value.isChecked;  
   });

   if(this.checkedItems.length == null || this.checkedItems.length == undefined || this.checkedItems.length==0){
     this.errormsg = this.showFont[2];
   }else{
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    for(let i=0 ; i< this.checkedItems.length ; i++)
    {
      this.phNo.push({"userID": this.userdata.userID, "phone": this.checkedItems[i].t1, "name": "", "type": 1, "t3": 0 })
     
      //console.log("Checked ph list >>" + JSON.stringify(this.phNo));     

    }
      this.syncContact();
   }  
 }

 syncContact(){
  let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, data: this.phNo }
  //console.log("request data >>" + JSON.stringify(param));
  this.http.post(this.ipaddress + '/chatservice/addContactList', param).map(res => res.json()).subscribe(data => {
    let code = data.code;
    console.log("sync data >>" + JSON.stringify(data));
    if (code == "0000") { 
      this.loading.dismiss();        
     //this.contactObj = data;
     this.showAlert();    
    }
    else if (data.code == "0016") {
      this.loading.dismiss();
      let confirm = this.alertCtrl.create({
        title: 'Warning!',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'OK',
            handler: () => {
 
              this.storage.remove('userData');
              this.events.publish('login_success', false);
              this.events.publish('lastTimeLoginSuccess', '');
              //this.navCtrl.setRoot(Login, {    });
              this.navCtrl.popToRoot();
            }
          }
        ]
      })
      confirm.present();
     
    }
    else {
      this.loading.dismiss();
      let toast = this.toastCtrl.create({
        message: data.desc,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  },
    error => {
      this.loading.dismiss();
      let toast = this.toastCtrl.create({
        message: this.all.getErrorMessage(error),
        duration: 5000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
      //this.closeProcessingLoading();
    });
 }

}
