import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { DatePipe } from '@angular/common';
import { TransferConfirm } from '../transfer-confirm/transfer-confirm';
import { UtilProvider } from '../../providers/util/util';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
import { PaymentPasswordPage } from '../payment-password/payment-password';

@Component({
  selector: 'page-transfer',
  templateUrl: 'transfer.html',
  providers: [CreatePopoverPage],
})
//Transfer Request ( off us ): Payee ( label ) , Balance ( lable ), Beneficiary( Textbox ), Institution Code( Textbox ), Amount( Textbox ), Remark( Textbox )
export class AccountTransfer {

  textEng: any = ["Transfer", "Name", "Balance", "Mobile Number", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Transfer",
    "Mobile Wallet", "Type", "Transfer To", "Reference", "Account Number",
    "Invalid Amount."];
  textMyan: any = ["ငွေလွှဲရန်", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
    "မှတ်ချက်", "မှားယွင်းနေပါသည်", "Does not match! Please try again.", "Please insert name field!", "ငွေလွှဲမည်",
    "Mobile Wallet", "အမျိုးအစား", "ငွေလွှဲရန်", "အကြောင်းအရာ", "အကောင့်နံပါတ်",
    "ငွေပမာဏ ရိုက်ထည့်ပါ"];
  textData: string[] = [];
  type: any = { name: "mWallet", code: "mWallet" };
  institute: any = { name: "", code: "" };
  address: string = '';
  userID: string = '';
  balance: string = '';
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: any;
  remark: string = '';
  ipaddress: string = '';
  font: string = '';
  lan: any;
  popover: any;
  rkey:any;
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: ""
  }
  errormsg1: any;
  temp: any;
  normalizePhone: any;
  //contact : any;
  accountList: any;
  accNumber: string = '';
  contact: any = { 'phone': '', 'name': '' };
  keyboardfont: any;
  public alertPresented: any;
  isLoading: any;
  txnType: any;
  btnflag: boolean;
  flag:any;
  flag1:any;
  flag2:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public menuCtrl: MenuController, public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public sqlite: SQLite,
    public changefont: Changefont, 
    public events: Events, public alertCtrl: AlertController,
    public popoverCtrl: PopoverController, public platform: Platform,
    public util: UtilProvider, public global: GlobalProvider,private eventlog:EventLoggerProvider,
    public viewCtrl: ViewController, private slimLoader: SlimLoadingBarService,
    public appCtrl: App, public createPopover: CreatePopoverPage,
    private firebase: FirebaseAnalytics) {
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      this.flag1=0;
      console.log(JSON.stringify(this._obj));
      if (userData != null || userData != '') {
        this.balance = userData.balance;
      }
    });

    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    this.institute.name = this._obj.toInstitutionName;
    this.institute.code = this._obj.toInstitutionCode;

    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
    });

    // this.storage.get('ipaddressWallet').then((ip) => {
    //   if (ip !== undefined && ip != null && ip !== "") {
    //     this.ipaddress = ip;
    //     this.getAccountList();
    //   } else {
    //     this.ipaddress = this.global.ipaddressWallet;
    //     this.getAccountList();
    //   }
    // });
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
        this.getAccountList();
      } else {
        this.ipaddress = this.global.ipaddress;
       this.getAccountList();
      }
    });
    
  

    this.temp = this.navParams.get('param');
    console.log("cont===", JSON.stringify(this.temp));

    if (this.temp != null && this.temp != undefined && this.temp != '') {
      this._obj.beneficiaryID = this.temp.phone;
      this._obj.toName = this.temp.name;
    }

    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });
    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    })

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;
    });

    this.storage.get('font').then((font) => {
      if (font == "zg") {
        this.keyboardfont = 'zg';
      } else {
        this.keyboardfont = 'uni';
      }
    });
  }
  @ViewChild('myInput') myInput;
  ionViewDidLoad(){
    console.log("ionViewDidLoad");
    setTimeout(() => {
      this.myInput.setFocus();
    }, 500);
  }

  goNext() {  
    this.eventlog.fbevent('Login_Success',{ userID: this._obj.userID,Message:'Wallet Transfer'})   
    this.btnflag = true;
    if (this.util.checkInputIsEmpty(this._obj.amount) || this.util.checkNumberOrLetter(this._obj.amount) ||
      this.util.checkAmountIsZero(this._obj.amount) || this.util.checkAmountIsLowerThanZero(this._obj.amount) ||
      this.util.checkStartZero(this._obj.amount) || this.util.checkPlusSign(this._obj.amount)) {
      this.errormsg1 = this.textData[16];
      this.btnflag = false;
    }

   else {
      this._obj.field1 = "1";
      this._obj.wType = this.type.name;
      let flag = false;
      if (this._obj.beneficiaryID != null && this._obj.beneficiaryID != '' && this._obj.beneficiaryID != undefined) {

      }
      else if (this.temp != null && this.temp != undefined && this.temp != '') {
        this._obj.beneficiaryID = this.temp.phone;       
        this._obj.toName = this.temp.name;
      }
      let institution = this.util.setInstitution();
      this._obj.frominstituteCode = institution.fromInstitutionCode;
      this._obj.toInstitutionCode = institution.toInstitutionCode;
      this._obj.toInstitutionName = institution.toInstitutionName;
      this.normalizePhone = this.util.normalizePhone(this._obj.beneficiaryID);     
      if (this.normalizePhone.flag == true) {
        flag = true;
        this._obj.beneficiaryID = this.normalizePhone.phone;
        if (flag) {               
            let tempdata: any;
            this.slimLoader.start(() => { });
            let userID1 = this._obj.userID;
            let param = { userID: userID1, sessionID: this._obj.sessionID, type: '12', merchantID: '' };
            this.http.post(this.ipaddress + '/service002/readMessageSetting', param).map(res => res.json()).subscribe(data => {
              if (data.code == "0000") {
                this.btnflag = false;
                tempdata=data;
                this.rkey=tempdata.rKey
                if(this.rkey=="true"){
                  this.flag1=1;
                }
                console.log("tempdata is chk password : " + JSON.stringify(tempdata));
                this.slimLoader.complete();
                this.eventlog.fbevent('transfer_confrim_success',{ userID: this._obj.userID,Message:'Transfer Confrim'});
                this.navCtrl.push(TransferConfirm, {
                  data: this._obj, 
                  tosKey: this.temp.syskey,            
                  messageParam:param ,
                  psw:this.flag1
                })     
                        
              }             
              else {
                this.eventlog.fbevent('transfer_confrim_fail',{ userID: this._obj.userID,Message:'Transfer Confrim'});
                this.showAlert('Warning!', data.desc);
                this.slimLoader.complete();
                  
              }        
            },
              error => {
                this.showAlert('Warning!', this.getError(error));
                this.slimLoader.complete();
            });     
        }
      }
   }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  typeChange(s) {
    this.type = s;
    this.type.name = s.name;
    this.type.code = s.code;
  }

  setAccNumber(accNum) {
    for (let i = 0; i < this.accountList.length; i++) {
      if (accNum == this.accountList[i].value) {
        this.accNumber = this.accountList[i].accNumber;
      }
    }
  }

  getAccountList() {
    this.slimLoader.start(() => { });
    let userID1 = this._obj.userID;//this._obj.userID.replace(/\+/g, '');

    let parameter = { userID: userID1, sessionID: this._obj.sessionID };

    this.http.post(this.ipaddress + '/service001/getAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result != null && result.code != null && result.code == "0000") {
        let tempArray = [];

        if (!Array.isArray(result.accountList)) {
          tempArray.push(result.accountList);
          result.accountList = tempArray;
        }

        this.accountList = result.accountList;
        this.accNumber = this.accountList[0].accNumber;

        this.slimLoader.complete();
      } else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      }
    },
      error => {
        /* ionic App error
         ............001) url link worng, not found method (404)
         ........... 002) server not response (500)
         ............003) cross fillter not open (403)
         ............004) server stop (-1)
         ............005) app lost connection (0)
         */
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }

        let msg = "Can't connect right now. [" + code + "]";

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      });
  }

  instituteChange(s) {
    this._obj.toInstitutionName = s.toInstitutionName;
    this._obj.toInstitutionCode = s.toInstitutionCode;
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }

    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

    }
  }

  onChange(s, i) {
    if (i == "09") {
      this._obj.beneficiaryID = "+959";
    }
    else if (i == "959") {
      this._obj.beneficiaryID = "+959";
    }
  }

  ionViewWillEnter() {
    if (this.temp.phone != undefined || this.temp != '' || this.temp != null) {
      this._obj.beneficiaryID = this.temp.phone;
      ////console.log("my data is" + this._obj.beneficiaryID);
      this._obj.toName = this.temp.name;
      ////console.log("my data is" + this._obj.toName);
    }
  }

  showAlert(titleText, subTitleText) {
    if (this.alertPresented == undefined) {
      let alert = this.alertCtrl.create({
        title: titleText,
        subTitle: subTitleText,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.alertPresented = undefined;
            }
          }
        ],
        enableBackdropDismiss: false
      });

      this.alertPresented = alert.present();
      setTimeout(() => alert.dismiss(), 2000 * 60);
    }
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);
    this.isLoading = false;
    ////console.log("Oops!");
  }
}
