import { NavController, ModalController, PopoverController, NavParams, Content, ToastController, Platform, Events, AlertController, LoadingController, ActionSheetController, App } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Component, ViewChild } from '@angular/core';
import 'rxjs/add/operator/map';
import { File } from '@ionic-native/file';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { DomSanitizer } from '@angular/platform-browser';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { VideodetailPage } from '../videodetail/videodetail';
import { ViewPhotoMessagePage } from '../view-photo-message/view-photo-message';
import { FbdetailPage } from '../fbdetail/fbdetail';
import { FunctProvider } from '../../providers/funct/funct';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Network } from '@ionic-native/network';
import { CommentPage } from '../comment/comment';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ChangefontProvider } from '../../providers/changefont/changefont';
import { Changefont } from '../changefont/changeFont';
//import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { CreatePostPage } from '../create-post/create-post';
import { EditImagesPage } from '../edit-images/edit-images';
import { PostLikePersonPage } from '../post-like-person/post-like-person';
import { LikepersonPage } from '../likeperson/likeperson';

declare var window;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ChangelanguageProvider, ChangefontProvider, CreatePopoverPage]
})

export class HomePage {

  @ViewChild(Content) content: Content;
  rootNavCtrl: NavController;
  home: string = "home";
  popover: any;
  items: any[];
  data: any = [];
  url: string = '';
  img: any = '';
  isLoading: any;
  registerData: any = { syskey: '', t1: '' };
  menuList: any = [];
  menu: any;
  parameter: any;
  nores: any;
  photoLink: any;
  videoImgLink:any;
  tabs: any = '';
  start: any = 0;
  end: any = 0;
  vantagedata: any = [];
  db: any;
  font: any;
  textMyan: any = ["သတင်းများ", "ကြိုက်တယ်", "မှတ်ချက်", "ဝေမျှမည်", "ဒေတာ မရှိပါ။", "အကြောင်းအရာများ",
    "ဗီဒီယို", "တွေ့ဆုံမေးမြန်းခြင်း", "ကြည့်ရန်"
    //, "ခု"
  ];
  textEng: any = ["News", "Like", "Comment", "Share", "No result found", "Article",
    "Videos", "Interview", "View"
    //, "Count"
  ];
  textData: any = [];
  allData: any = [];
  getdeep: any;
  count: any = 0;
  docImg: any;
  docData: any;
  photoName: any;
  public scannedText: any = {};
  public buttonText: string;
  public loading: boolean;
  latitude: any;
  longitude: any;
  location: any;
  imglink: any;
  ipaddress: any;
  pageStatus: any;
  alertPopup: any;
  ticketSummary: any = [];
  total: any;
  open: any = '';
  closed: any = '';
  allSummary: any;
  loadingIcon: any;

  profile: any;
  photo: string = '';
  profileImage: any;

  userText: any;  

  constructor(
    public navCtrl: NavController, 
    public modalCtrl: ModalController,
    public navParams: NavParams, 
    public popoverCtrl: PopoverController,
    public storage: Storage, 
    public toastCtrl: ToastController,
    public http: Http, 
    public funct: FunctProvider,
    public events: Events, 
    public appCtrl: App,
    public platform: Platform, public alertCtrl: AlertController,
    public changeLanguage: ChangelanguageProvider,
    public sanitizer: DomSanitizer, 
    public sqlite: SQLite,
    private file: File,
    public createPopover: CreatePopoverPage, 
    public global: GlobalProvider,
    public loadingCtrl: LoadingController, 
    public menuCtrl: MenuController,
    public network: Network, 
    public actionSheetCtrl: ActionSheetController,
    public changeFont: ChangefontProvider, 
    public changefont1: Changefont,
    //private transfer: Transfer,
    private transfer: FileTransfer
  ) {
    //this.events.publish('user:loggedin');    
    //this.menuCtrl.swipeEnable(true);
    this.photoLink = this.global.digitalMedia + "upload/smallImage/contentImage/";
    this.videoImgLink = this.global.digitalMedia + "upload/smallImage/videoImage/";
    this.storage.get('localPhotoAndLink').then((photo) => {
      //console.log('localPhotoAndLink in app.component constructor get:', photo);
      this.photo = photo;
      //this.checkLocalPhotoExists(this.imglnk, this.photo);

      if (this.photo != '' && this.photo != undefined && this.photo != null && this.photo.includes("/")) {
        this.profile = this.photo;
        //console.log('file cache directory and photo(e.g.xxx.jpg):', this.profile);
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
        //console.log('Unknown.png or user-icon.png or blank string:', this.profile);
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
          //console.log('server link and photo(e.g.xxx.jpg):', this.profile);
        });
      }
    });
    this.events.subscribe('localPhotoAndLink', photo => {
      //console.log('localPhotoAndLink in app.component constructor subscribe:', photo);
      this.photo = photo;
      //this.checkLocalPhotoExists(this.imglnk, this.photo);

      if (this.photo != '' && this.photo != undefined && this.photo != null && this.photo.includes("/")) {
        this.profile = this.photo;
        //console.log('file cache directory and photo(e.g.xxx.jpg):', this.profile);
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
        //console.log('Unknown.png or user-icon.png or blank string:', this.profile);
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
          //console.log('server link and photo(e.g.xxx.jpg):', this.profile);
        });
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    this.ipaddress = this.global.ipaddressCMS;

    this.rootNavCtrl = navParams.get('rootNavCtrl');
    //this.photoLink = this.global.digitalMedia + "upload/image";

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      //console.log("success create or open database");
      this.db = db;
      this.db.executeSql('CREATE TABLE IF NOT EXISTS WalletHome(allData TEXT)', {})
        .then(() => console.log('Executed SQL'))
        .catch(e => console.log(e));
      this.storage.get('appData').then((data) => {
        //console.log("registerData = " + JSON.stringify(this.registerData));
        this.registerData = data;
        this.selectData();
      });
    }).catch(e => console.log(e));   
  }

 changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont1.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }  

  getList(start, infiniteScroll) {
    this.end = this.end + 10;
    let parm = {
      start: start,
      end: this.end,
      userPhone: this.registerData.t1,
      // domain: this.global.domainCMS
      usersk: this.registerData.usersyskey
    }

    //console.log("home page == " + JSON.stringify(parm));
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + '/serviceConfiguration/getMenuOrderList?usersk=' + this.registerData.usersyskey + '&regionCode=' + this.global.regionCode, parm).map(res => res.json()).subscribe(data => {
        //console.log("response getMenuOrderList = ", JSON.stringify(data));

        //console.log("data.menu_arr[0].menu_arr.length == " + data.menu_arr[0].menu_arr.length);
        if (data.menu_arr.length > 0) {
          for (let index = 0; index < data.menu_arr.length; index++) {
            if (data.menu_arr[index].menu_arr.length > 0) {
              this.nores = 1;
              if (this.end == 10) {
                this.menuList = data.menu_arr;
                for (let i = 0; i < this.menuList.length; i++) {
                  //console.log("type one brfore==" + JSON.stringify(this.menuList[i].menu_arr));
                  for (let j = 0; j < this.menuList[i].menu_arr.length; j++) {
                    this.menuList[i].menu_arr[j].modifiedDate = this.funct.getTransformDate(this.menuList[i].menu_arr[j].modifiedDate);
                    //console.log("one obj ====== " + JSON.stringify(this.menuList[i].menu_arr[j]));
                    if (this.menuList[i].menu_arr[j].n6 != 1)
                      this.menuList[i].menu_arr[j].showLike = false;
                    else
                      this.menuList[i].menu_arr[j].showLike = true;
                    if (this.menuList[i].menu_arr[j].n7 != 1)
                      this.menuList[i].menu_arr[j].showContent = false;
                    else
                      this.menuList[i].menu_arr[j].showContent = true;
                    if (this.menuList[i].menu_arr[j].n2 != 0) {
                      //this.menuList[i].menu_arr[j].likeCount = this.funct.getChangeCount(this.menuList[i].menu_arr[j].n2);
                      this.menuList[i].menu_arr[j].likeCount = this.menuList[i].menu_arr[j].n2;
                    }
                    if (this.menuList[i].menu_arr[j].n3 != 0) {
                      //this.menuList[i].menu_arr[j].commentCount = this.funct.getChangeCount(this.menuList[i].menu_arr[j].n3);
                      this.menuList[i].menu_arr[j].commentCount = this.menuList[i].menu_arr[j].n3;
                    }
                    if (this.menuList[i].menu_arr[j].t2.replace(/<\/?[^>]+(>|$)/g, "").length > 200) {
                      this.menuList[i].menu_arr[j].showread = true;
                    }
                    else
                      this.menuList[i].menu_arr[j].showread = false;

                    if (this.menuList[i].menu_arr[j].n10 == 1) {
                      if (this.menuList[i].menu_arr[j].t8 != '') {
                        if (this.menuList[i].menu_arr[j].uploadedPhoto.length > 0) {
                          this.menuList[i].menu_arr[j].videoLink = this.menuList[i].menu_arr[j].uploadedPhoto[0].t7;
                          //this.menuList[i].menu_arr[j].videoLink = this.photoLink + this.menuList[i].menu_arr[j].uploadedPhoto[0].t7;
                        }
                        else {
                          let temp = this.menuList[i].menu_arr[j].t8;     // for video link
                          let str1 = temp.search("external/");
                          let str2 = temp.search(".sd");
                          let res = temp.substring(str1 + 9, str2);
                          this.menuList[i].menu_arr[j].videoLink = "https://i.vimeocdn.com/video/" + res + "_295x166.jpg";
                          //console.log("data.data[i].videoLink == " + this.menuList[i].menu_arr[j].t8.videoLink);
                        }
                        this.menuList[i].menu_arr[j].t8 = this.sanitizer.bypassSecurityTrustResourceUrl(this.menuList[i].menu_arr[j].t8);

                      }
                    }
                    else if (this.menuList[i].menu_arr[j].n10 == 2) {
                      this.menuList[i].menu_arr[j].videoLink = this.menuList[i].menu_arr[j].uploadedPhoto[0].t7;
                      //this.menuList[i].menu_arr[j].videoLink = this.photoLink + this.menuList[i].menu_arr[j].uploadedPhoto[0].t7;
                      this.menuList[i].menu_arr[j].t8 = this.sanitizer.bypassSecurityTrustResourceUrl(this.menuList[i].menu_arr[j].t8 + "?&autoplay=1");
                      //console.log("data.data[i].t8 for youtube == " + this.menuList[i].menu_arr[j].t8);
                    }
                    else {
                      if (this.menuList[i].menu_arr[j].uploadedPhoto.length > 0) {
                        if (this.menuList[i].menu_arr[j].uploadedPhoto[0].t2 != '') {
                          this.menuList[i].menu_arr[j].videoLink = this.menuList[i].menu_arr[j].uploadedPhoto[0].t7;
                          //this.menuList[i].menu_arr[j].videoLink = this.photoLink + this.menuList[i].menu_arr[j].uploadedPhoto[0].t7;
                          this.menuList[i].menu_arr[j].videoStatus = true;
                        }
                        else {
                          this.menuList[i].menu_arr[j].videoStatus = false;
                        }
                      }
                    }
                    if (this.menuList[i].menu_arr[j].t2.indexOf("<img ") > -1) {//atn
                      this.menuList[i].menu_arr[j].t2 = this.menuList[i].menu_arr[j].t2.replace(/<img /g, "<i ");
                      //console.log("replace img == "+this.menuList[i].menu_arr[j].t2);
                      this.menuList[i].menu_arr[j].t2 = this.menuList[i].menu_arr[j].t2.replace(/ \/>/g, "></i>");
                      //console.log("replace <i/> == "+this.menuList[i].menu_arr[j].t2);
                    }
                    /*  this.changeLanguage.changelanguageText(this.font, this.menuList[i].menu_arr[j].t1).then((data) => {
                      this.menuList[i].menu_arr[j].t1 = data;
                    });
                    this.changeLanguage.changelanguageText(this.font, this.menuList[i].menu_arr[j].t2).then((data) => {
                      this.menuList[i].menu_arr[j].t2 = data;
                    });  */


                  }
                }
                //console.log("menu list == " + JSON.stringify(this.menuList));
                this.allData = [];

                for (let d = 0; d < this.menuList.length; d++) {
                  //console.log("menu_arr == " + JSON.stringify(this.menuList[d].menu_arr));
                  for (let h = 0; h < this.menuList[d].menu_arr.length; h++) {
                    this.allData.push(this.menuList[d].menu_arr[h]);
                  }
                }

                if (this.allData.length > 0) {
                  this.deleteData();
                }
              }
              else if (this.end > 10) {
                let tempData = [];
                tempData = data.menu_arr;
                for (let i = 0; i < tempData.length; i++) {
                  //console.log("type one brfore==" + JSON.stringify(this.menuList[i].menu_arr));
                  for (let j = 0; j < tempData[i].menu_arr.length; j++) {
                    tempData[i].menu_arr[j].modifiedDate = this.funct.getTransformDate(tempData[i].menu_arr[j].modifiedDate);
                    //console.log("n3 ====== "+ this.menuList[i].menu_arr[j].n3);
                    if (tempData[i].menu_arr[j].n6 != 1)
                      tempData[i].menu_arr[j].showLike = false;
                    else
                      tempData[i].menu_arr[j].showLike = true;
                    if (tempData[i].menu_arr[j].n7 != 1)
                      tempData[i].menu_arr[j].showContent = false;
                    else
                      tempData[i].menu_arr[j].showContent = true;
                    if (tempData[i].menu_arr[j].n2 != 0) {
                      //tempData[i].menu_arr[j].likeCount = this.funct.getChangeCount(tempData[i].menu_arr[j].n2);
                      tempData[i].menu_arr[j].likeCount = tempData[i].menu_arr[j].n2;
                    }
                    if (tempData[i].menu_arr[j].n3 != 0) {
                      //tempData[i].menu_arr[j].commentCount = this.funct.getChangeCount(tempData[i].menu_arr[j].n3);
                      tempData[i].menu_arr[j].commentCount = tempData[i].menu_arr[j].n3;
                    }

                    if (tempData[i].menu_arr[j].t2.replace(/<\/?[^>]+(>|$)/g, "").length > 200) {
                      tempData[i].menu_arr[j].showread = true;
                    }
                    else
                      tempData[i].menu_arr[j].showread = false;

                    if (tempData[i].menu_arr[j].n10 == 1) {

                      if (tempData[i].menu_arr[j].t8 != '') {
                        if (tempData[i].menu_arr[j].uploadedPhoto.length > 0) {
                          tempData[i].menu_arr[j].videoLink = tempData[i].menu_arr[j].uploadedPhoto[0].t7;
                          //tempData[i].menu_arr[j].videoLink = this.photoLink + tempData[i].menu_arr[j].uploadedPhoto[0].t7;
                        }
                        else {
                          let temp = tempData[i].menu_arr[j].t8;     // for video link
                          let str1 = temp.search("external/");
                          let str2 = temp.search(".sd");
                          let res = temp.substring(str1 + 9, str2);
                          tempData[i].menu_arr[j].videoLink = "https://i.vimeocdn.com/video/" + res + "_295x166.jpg";
                          //.log("data.data[i].videoLink == " + tempData[i].menu_arr[j].t8.videoLink);
                        }

                        tempData[i].menu_arr[j].t8 = this.sanitizer.bypassSecurityTrustResourceUrl(tempData[i].menu_arr[j].t8);
                      }
                    }
                    else if (tempData[i].menu_arr[j].n10 == 2) {
                      tempData[i].menu_arr[j].videoLink = tempData[i].menu_arr[j].uploadedPhoto[0].t7;
                      //tempData[i].menu_arr[j].videoLink = this.photoLink + tempData[i].menu_arr[j].uploadedPhoto[0].t7;
                      tempData[i].menu_arr[j].t8 = this.sanitizer.bypassSecurityTrustResourceUrl(tempData[i].menu_arr[j].t8 + "?&autoplay=1");
                     //console.log("data.data[i].t8 for youtube == " + tempData[i].menu_arr[j].t8);
                    }
                    else {
                      if (tempData[i].menu_arr[j].uploadedPhoto.length > 0) {
                        if (tempData[i].menu_arr[j].uploadedPhoto[0].t2 != '') {
                          tempData[i].menu_arr[j].videoLink = tempData[i].menu_arr[j].uploadedPhoto[0].t7;
                          //tempData[i].menu_arr[j].videoLink = this.photoLink + tempData[i].menu_arr[j].uploadedPhoto[0].t7;
                          tempData[i].menu_arr[j].videoStatus = true;
                        }
                        else {
                          tempData[i].menu_arr[j].videoStatus = false;
                        }
                      }
                    }
                    if (tempData[i].menu_arr[j].t2.indexOf("<img ") > -1) {
                      tempData[i].menu_arr[j].t2 = tempData[i].menu_arr[j].t2.replace(/<img /g, "<i ");
                      tempData[i].menu_arr[j].t2 = tempData[i].menu_arr[j].t2.replace(/ \/>/g, "></i>");
                    }
                    /* this.changeLanguage.changelanguageText(this.font, tempData[i].menu_arr[j].t1).then((data) => {
                      tempData[i].menu_arr[j].t1 = data;
                    });
                    this.changeLanguage.changelanguageText(this.font, tempData[i].menu_arr[j].t2).then((data) => {
                      tempData[i].menu_arr[j].t2 = data;
                    });  */


                    this.menuList[i].menu_arr.push(tempData[i].menu_arr[j]);
                    console.log(JSON.stringify("video link>>"+ this.menuList[i].menu_arr));
                  }
                }
                //console.log("type one after=" + JSON.stringify(this.menuList));
                this.isLoading = false;
              }

              //this.allData = this.menuList;

              if (infiniteScroll != '') {
                infiniteScroll.complete();
              }
            }
            else {
              //console.log("this.end == " + this.end);
              if (this.end > 10) {
                this.end = this.start - 1;
                if (infiniteScroll != '') {
                  infiniteScroll.complete();
                }
              }
              if (this.menuList.length > 0) {
                this.nores = 1;
              }
              else {
                this.isLoading = false;
                this.nores = 0;
              }
            }
          }

          //console.log("menuList lenght == " + this.menuList.length);
          //console.log("alldata length == " + this.allData.length);

          /**/
        }
        else {
          if (this.end > 10) {
            this.end = this.start - 1;
            if (infiniteScroll != '') {
              infiniteScroll.complete();
            }
          }
          if (this.menuList.length > 0) {
            this.nores = 1;
          }
          else {
            this.isLoading = false;
            this.nores = 0;
          }

          //console.log("data.menu_arr is 0.");
        }

        this.isLoading = false;
        //console.log("isLoading == " + this.isLoading);
        //console.log("this.menuList.length " + this.menuList[0].menu_arr.length);
        //console.log("this.allData == "+JSON.stringify(this.allData));


      }, error => {
        //console.log("signin error=" + error.status);
        if (this.end > 10) {
          this.end = this.start - 1;
          if (infiniteScroll != '') {
            infiniteScroll.complete();
          }
        }
        if (this.menuList.length > 0) {
          this.nores = 1;
        }
        else {
          this.isLoading = false;
          this.nores = 0;
        }
        this.isLoading = false;
        this.getError(error);
      });
    }
  }

  insertData() {
    //console.log("home all data length = ", this.allData.length);
    let c;
    if (this.allData.length > 5)
      c = 5;
    else
      c = this.allData.length;
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      for (let i = 0; i < c; i++) {
        db.executeSql("INSERT INTO WalletHome(allData) VALUES (?)", [JSON.stringify(this.allData[i])]).then((data) => {
          //console.log("Insert data successfully", data);
        }, (error) => {
          //console.error("Unable to insert data", error);
        });
      }
    }).catch(e => console.log(e));
  }

  deleteData() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      //console.log("success create or open database");
      db.executeSql("DELETE FROM WalletHome", []).then((data) => {
        //console.log("Delete data WalletHome successfully", data);
        this.insertData();
        //this.allData =[];
      }, (error) => {
        //console.error("Unable to delete data", error);
      });
    }).catch(e => console.log(e));
  }

  offlineContent() {
    this.menuList = [];

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      //console.log("success create or open database");

      db.executeSql("SELECT * FROM WalletHome", []).then((data) => {
        //console.log("length == " + data.rows.length);
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            //console.log("data.rows.item(i).allData == " + data.rows.item(i).allData);
            this.vantagedata.push(JSON.parse(data.rows.item(i).allData));
          }
          //console.log("data rows:::" + JSON.stringify(this.vantagedata));
          //console.log("vantagedata length == " + this.vantagedata.length);
          this.menuList.push({ "menu_arr": this.vantagedata });
          //console.log("menuList = " + JSON.stringify(this.menuList));
          this.nores = 1;
          this.vantagedata = [];
          this.storage.get('appData').then((data) => {
            //console.log("registerData = " + JSON.stringify(this.registerData));
            this.registerData = data;
            this.start = 0;
            this.end = 0;
            this.getList(this.start, '');
            //console.log("registerData = " + JSON.stringify(this.registerData));
          });

        }
        else {
          this.storage.get('appData').then((data) => {
            //console.log("registerData = " + JSON.stringify(this.registerData));
           // console.log("no offline data");
            this.registerData = data;
            this.start = 0;
            this.end = 0;
            this.menuList = [];
            this.getList(this.start, '');
           // console.log("registerData = " + JSON.stringify(this.registerData));
          });
        }
      }, (error) => {

        this.storage.get('appData').then((data) => {
          this.registerData = data;
          this.start = 0;
          this.end = 0;
          this.menuList = [];
          this.getList(this.start, '');
         // console.log("registerData = " + JSON.stringify(this.registerData));
        });
        //console.error("Unable to select data", error);
      });
    }).catch(e => console.log(e));
  }


  doInfinite(infiniteScroll) {
    //console.log('Begin async operation');
    return new Promise((resolve) => {
      setTimeout(() => {
        this.start = this.end + 1;
        if (!this.isLoading)
          this.getList(this.start, infiniteScroll);
        //console.log('Async operation has ended');
        //infiniteScroll.complete();
      }, 900);
    })
  }

  singlePhoto(i) {
    console.log("viewImage == " + JSON.stringify(i));
    let localNavCtrl: boolean = false;
    if (localNavCtrl) {
      this.navCtrl.push(ViewPhotoMessagePage, {
        data: i,
        contentImg: "singlePhoto"
      });
    } else {
      this.navCtrl.push(ViewPhotoMessagePage, {
        data: i,
        contentImg: "singlePhoto"
      });
    }
  }

  viewImage(i) {
    //console.log("viewImage == " + JSON.stringify(i));
    let localNavCtrl: boolean = false;
    if (localNavCtrl) {
      this.navCtrl.push(FbdetailPage, {
        detailData: i
      });
    } else {
      this.navCtrl.push(FbdetailPage, {
        detailData: i
      });
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll) this.content.scrollToBottom(0);
    }, 400)
  }

  continue(i) {
    this.comment(i, 'detail');
  }


  comment(data, title) {
    if (this.nores != 0) {

      let localNavCtrl: boolean = false;
      if (localNavCtrl) {
       // console.log("hello")
       console.log("data aaa == " + JSON.stringify(data));
        this.navCtrl.push(CommentPage, {
          data: data,
          title: title
        });
      } else {
       // console.log("hi")
        this.navCtrl.push(CommentPage, {
          data: data,
          title: title
        });
      }
    }
    else {
      let toast = this.toastCtrl.create({
        message: "Please check internet connection!",
        duration: 5000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  /* share(i) {
    console.log("this is share url == " + JSON.stringify(i));
    let sahareImg = this.global.imglink + "upload/image/mit_logo.png";
    if (i.uploadedPhoto.length > 0) {
      sahareImg = this.photoLink + i.uploadedPhoto[0].t7;
    }
    let title = this.changeFont.UnitoZg(i.t1);
    const Branch = window['Branch'];
    //  this.url = "https://b2b101.app.link/R87NzKABHL";

    var propertiesObj = {
      canonicalIdentifier: 'content/123',
      canonicalUrl: 'https://example.com/content/123',
      title: title,
      contentDescription: '' + Date.now(),
      contentImageUrl: sahareImg,
      price: 12.12,
      currency: 'GBD',
      contentIndexingMode: 'private',
      contentMetadata: {
        custom: 'data',
        testing: i.syskey,
        this_is: true
      }
    }

    // create a branchUniversalObj variable to reference with other Branch methods
    var branchUniversalObj = null
    Branch.createBranchUniversalObject(propertiesObj).then(function (res) {
      branchUniversalObj = res
      //alert('Response1: ' + JSON.stringify(res))
      console.log('Response1: ' + JSON.stringify(res))
      // optional fields
      var analytics = {
        channel: 'facebook',
        feature: 'onboarding',
        campaign: 'content 123 launch',
        stage: 'new user',
        tags: ['one', 'two', 'three']
      }

      // optional fields
      var properties1 = {
        $desktop_url: 'http://www.example.com/desktop',
        $android_url: 'http://www.example.com/android',
        $ios_url: 'http://www.example.com/ios',
        $ipad_url: 'http://www.example.com/ipad',
        $deeplink_path: 'content/123',
        $match_duration: 2000,
        custom_string: i.syskey,
        custom_integer: Date.now(),
        custom_boolean: true
      }

      branchUniversalObj.generateShortUrl(analytics, properties1).then(function (res) {
        //alert('Response2: ' + JSON.stringify(res.url));
        console.log('Response2: ' + JSON.stringify(res.url));
        // optional fields
        var analytics = {
          channel: 'facebook',
          feature: 'onboarding',
          campaign: 'content 123 launch',
          stage: 'new user',
          tags: ['one', 'two', 'three']
        }

        // optional fields
        var properties = {
          $desktop_url: 'http://www.example.com/desktop',
          custom_string: i.syskey,
          custom_integer: Date.now(),
          custom_boolean: true
        }

        var message = 'Check out this link'

        // optional listeners (must be called before showShareSheet)
        branchUniversalObj.onShareSheetLaunched(function (res) {
          // android only
          console.log(res)
        })
        branchUniversalObj.onShareSheetDismissed(function (res) {
          console.log(res)
        })
        branchUniversalObj.onLinkShareResponse(function (res) {
          console.log(res)
        })
        branchUniversalObj.onChannelSelected(function (res) {
          // android only
          console.log(res)
        })

        // share sheet
        branchUniversalObj.showShareSheet(analytics, properties, message)
      }).catch(function (err) {
        console.error('Error2: ' + JSON.stringify(err))
      })
    }).catch(function (err) {
      console.error('Error1: ' + JSON.stringify(err))
    })
  } */

  share(i) {
    let appStore = "";

    if (this.global.regionCode == '13000000') {
      //Yangon App (13000000)
      appStore = 'https://play.google.com/store/apps/details?id=dc.digitalyangon';
    }
    if (this.global.regionCode == '10000000') {
      //Mandalay App (10000000)
      appStore = 'https://play.google.com/store/apps/details?id=dc.digitalmandalay';
    }
    if (this.global.regionCode == '00000000') {
      //All App (00000000)
      appStore = 'https://play.google.com/store/apps/details?id=wallet.NSBmawalletdev';
    }

    //console.log("this is share url == " + JSON.stringify(i));

    let sahareImg = this.photoLink + "/dc/dc_logo.png";
    if (i.uploadedPhoto.length > 0) {
      if (i.uploadedPhoto[0].t7 != '')
        sahareImg = i.uploadedPhoto[0].t7;
      else
        sahareImg = this.photoLink + "/dc/imgErr.png";
    }
    let title = this.changeFont.UnitoZg(i.t1);
    const Branch = window['Branch'];
    //  this.url = "https://b2b101.app.link/R87NzKABHL";

    var propertiesObj = {
      canonicalIdentifier: 'content/123',
      canonicalUrl: 'https://example.com/content/123',
      title: title,
      contentDescription: '' + Date.now(),
      contentImageUrl: sahareImg,
      price: 12.12,
      currency: 'GBD',
      contentIndexingMode: 'private',
      contentMetadata: {
        custom: 'data',
        testing: i.syskey,
        this_is: true
      }
    }

    // create a branchUniversalObj variable to reference with other Branch methods
    var branchUniversalObj = null
    Branch.createBranchUniversalObject(propertiesObj).then(function (res) {
      branchUniversalObj = res
      //alert('Response1: ' + JSON.stringify(res))
      //console.log('Response1: ' + JSON.stringify(res))
      // optional fields
      var analytics = {
        channel: 'facebook',
        feature: 'onboarding',
        campaign: 'content 123 launch',
        stage: 'new user',
        tags: ['one', 'two', 'three']
      }

      // optional fields
      var properties1 = {
        $desktop_url: appStore,
        $android_url: 'http://www.example.com/android',
        $ios_url: 'http://www.example.com/ios',
        $ipad_url: 'http://www.example.com/ipad',
        $deeplink_path: 'content/123',
        $match_duration: 2000,
        custom_string: i.syskey,
        custom_integer: Date.now(),
        custom_boolean: true
      }

      branchUniversalObj.generateShortUrl(analytics, properties1).then(function (res) {
        //alert('Response2: ' + JSON.stringify(res.url));
        //console.log('Response2: ' + JSON.stringify(res.url));
        // optional fields
        var analytics = {
          channel: 'facebook',
          feature: 'onboarding',
          campaign: 'content 123 launch',
          stage: 'new user',
          tags: ['one', 'two', 'three']
        }

        // optional fields
        var properties = {
          $desktop_url: appStore,
          custom_string: i.syskey,
          custom_integer: Date.now(),
          custom_boolean: true
        }

        var message = 'Check out this link'

        // optional listeners (must be called before showShareSheet)
        branchUniversalObj.onShareSheetLaunched(function (res) {
          // android only
          //console.log(res)
        })
        branchUniversalObj.onShareSheetDismissed(function (res) {
         // console.log(res)
        })
        branchUniversalObj.onLinkShareResponse(function (res) {
          //console.log(res)
        })
        branchUniversalObj.onChannelSelected(function (res) {
          // android only
          //console.log(res)
        })

        // share sheet
        branchUniversalObj.showShareSheet(analytics, properties, message)
      }).catch(function (err) {
        //console.error('Error2: ' + JSON.stringify(err))
      })
    }).catch(function (err) {
      //console.error('Error1: ' + JSON.stringify(err))
    })
  }

  clickLike(data, j, k) {
    //console.log("data=" + JSON.stringify(data));
    this.menuList[j].menu_arr[k].likeflag = true;
    if (!data.showLike) {
      this.menuList[j].menu_arr[k].showLike = true;
      this.menuList[j].menu_arr[k].n2 = this.menuList[j].menu_arr[k].n2 + 1;
      //this.menuList[j].menu_arr[k].likeCount = this.funct.getChangeCount(this.menuList[j].menu_arr[k].n2);
      this.menuList[j].menu_arr[k].likeCount = this.menuList[j].menu_arr[k].n2;
      this.getLike(data, j, k);
    }
    else {
      this.menuList[j].menu_arr[k].showLike = false;
      this.menuList[j].menu_arr[k].n2 = this.menuList[j].menu_arr[k].n2 - 1;
      //this.menuList[j].menu_arr[k].likeCount = this.funct.getChangeCount(this.menuList[j].menu_arr[k].n2);
      this.menuList[j].menu_arr[k].likeCount = this.menuList[j].menu_arr[k].n2;
      this.getUnlike(data, j, k);
    }
  }
  
  getLike(data, j, k) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    //console.log("request clickLike = ", JSON.stringify(parameter));
    if (this.global.netWork == 'connected') {
      this.http.get(this.ipaddress + '/serviceArticle/clickLikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
        this.menuList[j].menu_arr[k].likeflag = false;
        if (data.state) {
          this.menuList[j].menu_arr[k].showLike = true;
        }
        else {
          this.menuList[j].menu_arr[k].showLike = false;
          this.menuList[j].menu_arr[k].n2 = this.menuList[j].menu_arr[k].n2 - 1;
        }
      }, error => {
        //console.log("signin error=" + error.status);
        this.getError(error);
      });
    }
  }

  getUnlike(data, j, k) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    //console.log("request clickLUnlike = ", JSON.stringify(parameter));
    if (this.global.netWork == 'connected') {
      this.http.get(this.ipaddress + '/serviceArticle/clickUnlikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
        this.menuList[j].menu_arr[k].likeflag = false;
        if (data.state) {
          this.menuList[j].menu_arr[k].showLike = false;
        }
        else {
          this.menuList[j].menu_arr[k].showLike = true;
          this.menuList[j].menu_arr[k].n2 = this.menuList[j].menu_arr[k].n2 + 1;
        }
      }, error => {
        //console.log("signin error=" + error.status);
        this.getError(error);
      });
    }
  }

  clickBookMark(data, j, k) {
    //console.log("data=" + JSON.stringify(data));
    if (!data.showContent) {
      this.menuList[j].menu_arr[k].showContent = true;
      this.saveContent(data, j, k);
    }
    else {
      this.menuList[j].menu_arr[k].showContent = false;
      this.unsaveContent(data);
    }
  }

  saveContent(data, j, k) {
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 1
    }
    //console.log("request saveContent parameter= ", JSON.stringify(parameter));
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + '/serviceContent/saveContent', parameter).map(res => res.json()).subscribe(data => {
        //console.log("response saveContent = ", JSON.stringify(data));
        // this.isLoading = false;
      }, error => {
        //console.log("signin error=" + error.status);
        this.getError(error);
      });
    }
  }

  unsaveContent(data) {
    //console.log("request unsaveContent = ", JSON.stringify(data));
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 0
    }
    //console.log("request unsaveContent parameter = ", JSON.stringify(parameter));
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + '/serviceContent/unsaveContent', parameter).map(res => res.json()).subscribe(data => {
        //console.log("response unsaveContent = ", JSON.stringify(data));

        //this.isLoading = false;
      }, error => {
        console.log("signin error=" + error.status);
        this.getError(error);
      });
    }
  }


  goVideoDetail(url) {

    if (this.nores != 0) {
      let localNavCtrl: boolean = false;
      //console.log("url == " + url + " ////  localNavCtrl == " + localNavCtrl);
      if (localNavCtrl) {
        this.navCtrl.push(VideodetailPage, {
          url: url
        });
      } else {
        this.navCtrl.push(VideodetailPage, {
          url: url
        });
      }
    }

  }
  ionViewWillEnter() {
    this.menuCtrl.swipeEnable(true);
  }

  ionViewCanEnter(){
    this.menuCtrl.swipeEnable(true);
  }

  doRefresh(refresher) {
    //console.log('Begin async operation', refresher);
    setTimeout(() => {
      //console.log('Async operation has ended');
      this.start = 0;
      this.end = 0;
      if (!this.isLoading)
        this.getList(this.start, refresher);
      refresher.complete();
    }, 2000);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //showCloseButton: true,
      dismissOnPageChange: true,
      //closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    //console.log("Oops!");
  }

  isURL(str) {
    let pattern = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    //console.log("pattern test = " + pattern.test(str));

    if (!pattern.test(str)) {
      return false;
    } else {
      return true;
    }
  }

  showMap() {
    window.open('geo://?q=', '_system');  // android
    // window.open('http://maps.apple.com/?q=','_system') //ios
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      //console.log("Active Page=" + this.navCtrl.getActive().name);
      this.platform.exitApp();
    });
  }

  download(photoAndServerLink, photo) {
    //const fileTransfer: TransferObject = this.transfer.create();
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(photoAndServerLink, this.file.cacheDirectory + photo).then((entry) => {
      //console.log('photo download success: ' + entry.toURL());
      let abc = entry.toURL();
      this.storage.set('localPhotoAndLink', abc);
      this.events.publish('localPhotoAndLink', abc);
    }, (error) => {
      //console.log('photo download error: ' + JSON.stringify(error));
    });
  }

  goCreatePage(sysKeyParam) {
    if (sysKeyParam == '')
      sysKeyParam = '0';

    this.storage.get('appData').then((data) => {
      this.registerData = data;
      this.start = 0;
      this.end = 0;
      this.menuList = [];
      this.getList(this.start, '');
      //console.log("registerData = " + JSON.stringify(this.registerData));

      this.navCtrl.push(CreatePostPage, {
        data: this.registerData,
        postSyskey: sysKeyParam
      });
    });
  }

  goPhotoPickPage() {
    this.navCtrl.push(EditImagesPage, {
      data: this.registerData
    });
  }

  makeAction(sysKeyParam) {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Edit Post',
          icon: !this.platform.is('ios') ? 'ios-create' : null,
          handler: () => {
            this.goCreatePage(sysKeyParam);
          }
        },
        {
          text: 'Delete Post',
          icon: !this.platform.is('ios') ? 'md-trash' : null,
          handler: () => {
            this.deletePost(sysKeyParam);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
           // console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  deletePost(sysKeyParam) {
    this.isLoading = true;

    let params = {
      'syskey': sysKeyParam,
      'type': 'user post',
    }

    this.http.post(this.ipaddress + '/serviceQuestionAdm/deleteContent', params).map(res => res.json()).subscribe(result => {
      //console.log("response deleteContent=" + JSON.stringify(result));

      if (result.state) {
        let toast = this.toastCtrl.create({
          message: "Deleted successfully!",
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);

        this.start = 0;
        this.end = 0;
        this.getList(this.start, null);
      } else {
        let toast = this.toastCtrl.create({
          message: "Deleted failed!",
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
      }

      this.isLoading = false;
    }, error => {
      //console.log("signin error=" + error.status);
      this.isLoading = false;
      this.getError(error);
    });
  }

  selectData() {
    this.isLoading = true;

    this.offlineContent();
  }

  getLikePerson(i){
    this.navCtrl.push(PostLikePersonPage,{
    data:i.syskey
    })
    }

}
