import { Component } from '@angular/core';
import { MenuController, ModalController, NavController, NavParams, LoadingController, PopoverController, ToastController, AlertController, Events, ActionSheetController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { DatePipe } from '@angular/common';
import { PaymentDetailsPage } from '../payment-details/payment-details';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { PaymentFailPage } from '../payment-fail/payment-fail';
import { Login } from '../login/login';
import { PaymentPasswordPage } from '../payment-password/payment-password';
import { TransferPasswordPage } from '../transfer-password/transfer-password'
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
@Component({
  selector: 'page-payment-confirm',
  templateUrl: 'payment-confirm.html',
  providers: [Changefont, CreatePopoverPage]
})
export class PaymentConfirm {
  textEng: any = ["Confirmation", "Name", "Balance", "Payee", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.",
    "Please insert name field!", "Cancel", "Confirm", "Reference", "Type"];
  textMyan: any = ["အတည်ပြုခြင်း", "အမည်", "လက်ကျန်ငွေ", "ငွေလွှဲသူ", "အော်ပရေတာ", "ငွေပမာဏ", "မှတ်ချက်",
    "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!",
    "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်", "အကြောင်းအရာ", "အမျိုးအစား"];
  textData: string[] = [];

  address: string = '';
  lat: string = '';
  long: string = '';
  userID: string = '';
  balance: string = '';
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: string = '';
  remark: string = '';
  ipaddress: string;
  submitted = false;
  isenabled: boolean = false;
  ischecked: boolean = false;
  font: string = '';
  public loading;
  lan: any;
  regdata: any;
  storageToken:any;
  token:any;
  popover: any;
  password:any;
  buttonColor:string='';
  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "reference": "", "toAcc": "",
    "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2",
    "sKey": "", tosKey: "", fromName: "", toName: "", "wType": "",
    "field1": "", "field2": "", accountNo: ""
  }
  passData: any;
  resData: any;
  errormsg: any;
  errormsg1: any;
  errormsg3: any;
  messNrc: any;
  tosKey: any;
  passPassword: any;
  param: any;
  ammount: any;
  userData: any;
  remark_flag: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public menuCtrl: MenuController,
    public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,public platform: Platform, public sqlite: SQLite,
    public changefont: Changefont,private all: AllserviceProvider,
    public global: GlobalProvider, public events: Events,
    public util: UtilProvider, public alertCtrl: AlertController,public popoverCtrl: PopoverController,
    public appCtrl: App, public createPopover: CreatePopoverPage,
    private eventlog:EventLoggerProvider) {
    this._obj = this.navParams.get('data');
    console.log("payment data is : " + JSON.stringify(this._obj));
    this.tosKey = this.navParams.get('tosKey');
    this.ammount = this.util.formatAmount(this._obj.amount);
    this.param = this.navParams.get("messageParam");
    this.passPassword = this.navParams.get("psw");
    console.log("toskey >>" + JSON.stringify(this.tosKey));
    console.log("param >>" + JSON.stringify(this.param));
    console.log("password >>" + JSON.stringify(this.passPassword));
    if (this._obj.remark != '' && this._obj.remark != undefined) {
      this.remark_flag = true;
    }
    //this.param = this.navParams.get("messageParam");
    //this.tosKey = this.navParams.get('tosKey');
    this.ammount = this.util.formatAmount(this._obj.amount);
    //this.passPassword = this.navParams.get("psw");
    console.log("param : " + JSON.stringify(this.param));
    console.log("tokey : " + JSON.stringify(this.tosKey));
    console.log("password is : " + JSON.stringify(this.passPassword));
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });

    this.storage.get('scanlocation').then((scanlocation) => {
      ////console.log('Scan location is', scanlocation);
      if (scanlocation !== undefined && scanlocation !== "") {
        this.address = scanlocation;
      }
    });
    this.storage.get('lat').then((lat) => {
      ////console.log('lat is', lat);
      if (lat !== undefined && lat !== "") {
        this.lat = lat;
      }
    });
    this.storage.get('long').then((long) => {
      ////console.log('long ', long);
      if (long !== undefined && long !== "") {
        this.long = long;
      }
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    }) //...
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("state data=" + JSON.stringify(this.textData));
    });
    this.events.subscribe('userData', userData => {
      if (userData != null) {
        this.userData = userData;
        //console.log("user data=" + JSON.stringify(this.userData));
      }
    });
    this.storage.get('firebaseToken').then((firebaseToken) => {
      this.storageToken = firebaseToken;
      console.log('Storage Login Token: '+this.storageToken);
    });
  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }

    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  checkPsw() {
    let temp: any;
    if (this.passPassword == 'true') {
      this.navCtrl.push(PaymentPasswordPage, {
        messageParam: this.param,
        tosKey: this.tosKey,
        data: this._obj,
      })
    } else {
     // this.goPost();
    }
  }

  // goPost() {
  //   let beneID = this._obj.beneficiaryID;
  //   if (beneID.startsWith("+")) {
  //     this.goQR();
  //   }
  //   else {
  //     this.goManual();
  //   }
  // }
  goManual() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
    // let benePhone = this.util.removePlus(this._obj.beneficiaryID);
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    let param = {
      "userID": payeePhone, sessionID: this._obj.sessionID,
      "payeeID": payeePhone, "beneficiaryID": this._obj.accountNo, "fromInstitutionCode": this._obj.frominstituteCode, "toInstitutionCode": this._obj.toInstitutionCode,
      "toAcc": this._obj.toAcc, "reference": this._obj.reference, amount: amount, "bankCharges": this._obj.bankCharges, "commissionCharges": this._obj.commissionCharges, "remark": this._obj.remark, "transferType": "1",
      "sKey": this._obj.sKey,
      "tosKey": this._obj.tosKey,
      "fromName": this._obj.fromName,
      "toName": this._obj.toName,
      "wType": this._obj.wType,
      "field1": "", "field2": ""
    };
    console.log("request for payment" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service001/`payment`', param).map(res => res.json()).subscribe(data => {
      
      let resdata = {
        "bankRefNo": data.bankRefNo,
        "code": data.code,
        "desc": data.desc,
        "field1": "",
        "field2": "",
        "transDate": data.transDate
      };
      console.log("response for payment in ad=" + JSON.stringify(data));
      let code = data.code;
      if (code == '0000') {
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName
        });
        this.loading.dismiss();
      }
      else {
        this.resData = [];
        this.navCtrl.push(PaymentFailPage, {
          desc: data.desc,
          data: this._obj
        })

        this.loading.dismiss();
      }
    },
      error => {
        ////console.log("error=" + error.status);
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }
  goPost() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
    let benePhone = this._obj.beneficiaryID;//this.util.removePlus(this._obj.beneficiaryID);
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs();
    let param = {
      token: this._obj.sessionID,
      senderCode: this._obj.userID,
      receiverCode:  this._obj.beneficiaryID,
      fromName:this._obj.fromName,
      toName: this._obj.toName,
      amount: amount, 
      prevBalance: this._obj.balance, 
      narrative:this._obj.remark,
      "password": this.all.getEncryptText(iv, salt, dm, this.password),
      "iv": iv, "dm": dm, "salt": salt,
      //apkType:"mWallet"
      };
    this.http.post(this.ipaddress + '/payment/goWalletTransfer', param).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
         };
      let code = data.code;
      if (code == '0000') {
        this.eventlog.fbevent('transfer_success',{ userID: this._obj.userID,Message:data.code});
        // if(this.storageToken!=null){
        //   this.token=this.storageToken;
        // }
        // else{
        //   this.token="";
        // }
        this.resData = data;
       
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName
        });

           this.loading.dismiss();
       
      } else if (data.code == "0012") {
        this.eventlog.fbevent('Transfer ',{ Code: data.code,Description:data.desc});
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.messageDesc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                // this.storage.remove('phonenumber');
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.eventlog.fbevent('transfer_fail',{ userID: this._obj.userID,Message:data.code});
        this.resData = [];
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
       
        toast.present(toast);
        this.loading.dismiss();
        
      }
    }
    ,
      error => {
        ////console.log("error=" + error.status);
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    );
  }


  goQR() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
    // let benePhone = this.util.removePlus(this._obj.beneficiaryID);
    let benePhone = this._obj.beneficiaryID;
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs();
      let param = {
        token: this._obj.sessionID,
        senderCode: this._obj.userID,
        receiverCode:  this._obj.beneficiaryID,
        fromName:this._obj.fromName,
        toName: this._obj.toName,
        amount: amount, 
        prevBalance: this._obj.balance,
        "password": this.all.getEncryptText(iv, salt, dm, this.password),
        "iv": iv, "dm": dm, "salt": salt,
        "appType":"wallet"
      };
      this.http.post(this.ipaddress + '/payment/goWalletTransfer', param).map(res => res.json()).subscribe(data => {
        let resdata = {
          "bankRefNo": data.bankRefNumber,
          "code": data.code,
          "desc": data.desc,
          "transDate": data.transactionDate
           };
        let code = data.code;
        if (code == '0000') {
          this.eventlog.fbevent('transfer_success',{ userID: this._obj.userID,Message:data.code});
          // if(this.storageToken!=null){
          //   this.token=this.storageToken;
          // }
          // else{
          //   this.token="";
          // }
          this.resData = data;
          this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
            data: resdata,
            data1: this._obj.amount,
            data2: this._obj.toName
          });
          this.loading.dismiss();
        }
        else if (data.code == "0016") {
          this.eventlog.fbevent('transfer_fail',{ userID: this._obj.userID,Message:data.code});
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  // this.storage.remove('phonenumber');
                  // this.firebase.logEvent('agent_transfer_fail', { Receiver:this._obj.beneficiaryID,ErrorMessage:"agent transfer fail",Code:data.code})
                  // .then((res: any) => { console.log(res); })
                  // .catch((error: any) => console.error(error));
                  this.storage.remove('username');
                  this.storage.remove('userData');
                  this.storage.remove('firstlogin');
                  this.appCtrl.getRootNav().setRoot(Login);
                }
              }
            ]
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          this.eventlog.fbevent('transfer_fail',{ userID: this._obj.userID,Message:data.code});
          this.resData = [];
          // this.firebase.logEvent('agent_transfer_fail', { Receiver:this._obj.beneficiaryID,ErrorMessage:"agent transfer fail",Code:data.code})
          // .then((res: any) => { console.log(res); })
          // .catch((error: any) => console.error(error));
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
         
          toast.present(toast);
          this.loading.dismiss();
          
        }
      },
      error => {
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }
  presentPopoverContact(ev) {
   // this.buttonColor = '#D8D8D8';
    this.popover = this.popoverCtrl.create(TransferPasswordPage, { data: ev }, { cssClass: 'contact-popover' });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != undefined && data != null && data != '') {
       // this.buttonColor='';
        this.password=data;
        console.log('Confrim Password Log: '+this.password);
        this.goQR();
      }
      // else{
      //   this.buttonColor='';
      // }
    });
}
goConfrim() {
  this.loading = this.loadingCtrl.create({
    content: "Processing",
    dismissOnPageChange: true
    // duration: 3000
  });
  this.loading.present();
  let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
  // let benePhone = this.util.removePlus(this._obj.beneficiaryID);
  let benePhone = this._obj.beneficiaryID;
  let amount = this.util.formatToDouble(this._obj.amount);
  let institution = this.util.setInstitution();
  this._obj.frominstituteCode = institution.fromInstitutionCode;
  this._obj.toInstitutionCode = institution.toInstitutionCode;
  let iv = this.all.getIvs();
  let dm = this.all.getIvs();
  let salt = this.all.getIvs();
    let param = {
      token: this._obj.sessionID,
      senderCode: this._obj.userID,
      receiverCode:  this._obj.beneficiaryID,
      fromName:this._obj.fromName,
      toName: this._obj.toName,
      amount: amount, 
      prevBalance: this._obj.balance,
      "password": this.all.getEncryptText(iv, salt, dm, this.password),
      "iv": iv, "dm": dm, "salt": salt,
      "appType":"wallet"
    };
    this.http.post(this.ipaddress + '/payment/goWalletTransfer', param).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
         };
      let code = data.code;
      if (code == '0000') {
        this.eventlog.fbevent('transfer_success',{ userID: this._obj.userID,Message:data.code});
        // if(this.storageToken!=null){
        //   this.token=this.storageToken;
        // }
        // else{
        //   this.token="";
        // }
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName
        });
        this.loading.dismiss();
      }
      else if (data.code == "0016") {
        this.eventlog.fbevent('transfer_fail',{ userID: this._obj.userID,Message:data.code});
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.resData = [];
        this.eventlog.fbevent('transfer_fail',{ userID: this._obj.userID,Message:data.code});
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
       
        toast.present(toast);
        this.loading.dismiss();
        
      }
    },
    error => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
      this.loading.dismiss();
    });
}


}
