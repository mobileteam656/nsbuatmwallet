import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Chat } from '../chat/chat';
import { FunctProvider } from '../../providers/funct/funct';
import { GlobalProvider } from '../../providers/global/global';
import { Platform, NavController, NavParams, PopoverController, Events, LoadingController, ToastController, ActionSheetController } from 'ionic-angular';
import { ContactPage } from '../contact/contact';//contact
import { App } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { File } from '@ionic-native/file';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { UtilProvider } from '../../providers/util/util';
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
  providers: [CreatePopoverPage],
})
export class MessagePage {

  rootNavCtrl: NavController;
  chatList: any = [];
  isLoading: any;
  userData: any;
  nores: any;
  start: any = 0;
  end: any = 0;
  ipaddress: string;
  userDataChat: any;
  popover: any;
  tabBarElement: any;
  channelData: any = [];
  //  db: any;
  profile: any;
  photo: any;
  docData: any;
  from: any;
  description: string = ''; 
  photoName: any;
  latitude: any;
  longitude: any;
  location: any;
  buttonText: any;
  loading: boolean;
  scannedText: any;
  profileImageLink: any;

  from_Chat: any = { fromName: '' };
  description_Chat: any = { description: '' };

  loadingIcon: any;
  db: any;
  constructor(
    public platform: Platform,
    public popoverCtrl: PopoverController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http, 
    public toastCtrl: ToastController,
    public global: GlobalProvider, 
    private file: File,
    public sqlite: SQLite, 
    public funct: FunctProvider,
    public events: Events, 
    public storage: Storage,
    public createPopover: CreatePopoverPage, 
    public loadingCtrl: LoadingController,
    public appCtrl: App,
    public actionSheetCtrl: ActionSheetController,
    public menuCtrl: MenuController,
    public util: UtilProvider,
  ) {    
    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.profileImageLink = profileImage;
      } else {
        this.profileImageLink = this.global.digitalMediaProfile;
        console.log("my profile image is here : " + JSON.stringify(this.profileImageLink));
      }
    }); 
    this.storage.get('ipaddress3').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log('Chatting Service is: '+this.ipaddress);
      } else {
        this.ipaddress = this.global.ipaddress3;
      }
    });
    this.rootNavCtrl = navParams.get('rootNavCtrl');
    this.docData = this.navParams.get('data');
    this.from = this.navParams.get('from');
    this.description = this.navParams.get('description');

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => { 
      this.db = db; 
      this.db.executeSql("CREATE TABLE IF NOT EXISTS channel (id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT,messagecontent TEXT)", {}).then((data) => {       
        this.selectData();
      }, (error) => {    
      });
    }).catch(e => console.log(e));
    console.log(JSON.stringify("Message Data"  +this.chatList));
  }

  ionViewCanEnter() {
    this.menuCtrl.swipeEnable(true);
    this.storage.get("phonenumber").then((data) => {      
      this.userData = data;
      this.start = 0;
      this.end = 0;
      this.storage.get('ipaddress3').then((ip) => {
        if (ip !== undefined && ip !== "" && ip != null) {
          this.ipaddress = ip;
        } else {
          this.ipaddress = this.global.ipaddress3;
        }
        console.log("Message Service is: "+this.ipaddress);
      });
    });
  }

  insertData() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {  
      for (let i = 0; i < this.chatList.length; i++) {
        db.executeSql("INSERT INTO channel(messagecontent) VALUES (?)", [JSON.stringify(this.chatList[i])]).then((data) => {        
        }, (error) => {        
        });
      }     
    }).catch(e => console.log(e));
  }

  deleteData() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => { 
      db.executeSql("DELETE FROM channel", []).then((data) => {      
        this.insertData();
      }, (error) => {       
      });
    }).catch(e => console.log(e));
  }

  selectData() {  
    this.chatList = [];
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => { 
      db.executeSql("SELECT * FROM channel ", []).then((data) => {    
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {           
            this.channelData.push(JSON.parse(data.rows.item(i).messagecontent));        
          }
          this.nores = 1;
        }        
      });
    }).catch(e => console.log(e));
  }

  getchatUserList(start) {   
    this.end = this.end + 10;
    let msgparams = {
      start: start,
      end: this.end
    }   
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + "serviceChat/searchChatList1?syskey=" + this.userDataChat.sKey + "&role=" + "&domain=" + this.global.domainChat + "&appId=" + this.global.appIDChat, msgparams).map(res => res.json()).subscribe(result => {
        console.log("response chatList =>" + JSON.stringify(result));
        if (result.data == undefined || result.data == null || result.data == '') {
          this.deleteData();
          this.nores = 0;
        }
        if (result.code == "0012") {
          this.util.SessiontimeoutAlert(result.desc);
        } 
        else {
          let tempArray = [];
          if (!Array.isArray(result.data)) {
            tempArray.push(result.data);
            result.data = tempArray;
          }
          for (let i = 0; i < result.data.length; i++) {
            if (this.global.regionCode == '13000000') {
              if (!(result.data[i].t1.includes("YCDC"))) {
                result.data.splice(i, 1);
                i--;
              }
            }
            if (this.global.regionCode == '10000000') {
              //Mandalay App (10000000)
              if (!(result.data[i].t1.includes("MCDC"))) {
                result.data.splice(i, 1);
                i--;
              }
            }
            if (this.global.regionCode == '00000000') {
              //All App (00000000)
              if (result.data[i].t1 != "general@dc.com") {
                result.data.splice(i, 1);
                i--;
              }
            }
          }
          if (result.data.length > 0) {
            this.chatList = result.data;           
            this.getGroupData();
            this.deleteData();
            this.nores = 1;         
          }
        }
        this.isLoading = false;
      }, error => {       
        this.getError(error);
      });
    }
  }

  viewChat(d) {
    this.from_Chat.fromName = 'MessagePage';
    this.description_Chat.description = '';    
    this.navCtrl.push(Chat, {
      data: d,
      from: this.from_Chat,
      sKeyDB: this.userDataChat.sKey,
      description: this.description_Chat
    });
  }

  doRefresh(refresher) {  
    this.start = this.start + 11;
    this.getchatUserList(this.start);
    setTimeout(() => {   
      refresher.complete();
    }, 2000);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  chatadd() {
    this.navCtrl.push(ContactPage);
  }

  getGroupData() {
    let monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    for (let i = 0; i < this.chatList.length; i++) {
      let modifiedDateStr = this.chatList[i].modifiedDate;
      let monthStr = modifiedDateStr.substring(4, 6);
      let monthInt = parseInt(monthStr);
      let month = monthNames[monthInt];
      let dateStr = modifiedDateStr.substring(6);
      let date = parseInt(dateStr);
      this.chatList[i].modifiedDate = date + " " + month;     
    }  
    let sortedContacts = this.chatList.sort(function (a, b) {
      if (a.t16 < b.t16) return 1;
      if (a.t16 > b.t16) return -1;
      return 0;
    });
    let group_to_values = sortedContacts.reduce(function (obj, item) {
      obj[item.t16] = obj[item.t16] || [];
      obj[item.t16].push(item);
      return obj;
    }, {});
    let groups = Object.keys(group_to_values).map(function (key) {   
      return { t16: key, data: group_to_values[key] };
    });
    this.chatList = groups;   
  }

  ionViewWillEnter() {
    this.menuCtrl.swipeEnable(true);
    this.storage.get('userData').then((userData) => {  
      if (userData !== undefined && userData !== "") {
        this.userDataChat = userData;
        this.start = this.start + 11;
        this.getchatUserList(this.start);
      }
    });
  }

  getError(error) {   
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',   
      dismissOnPageChange: true,  
    });
    toast.present(toast);
    this.isLoading = false;  
  }
}