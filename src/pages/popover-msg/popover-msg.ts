import { Component } from '@angular/core';
import { NavParams, ViewController, Events } from "ionic-angular";
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-popover-msg',
  templateUrl: 'popover-msg.html',
  providers: [Changefont]
})
export class PopoverMsgPage {

  //popoverItemListTemp: any = [];
  popoverItemList: any[] = [{ name: '', key: 1 }, { name: '', key: 2 }, { name: '', key: 3 }, { name: '', key: 4 }];

  textEng: any = [{ name: 'Copy', key: 1 }, { name: 'Delete', key: 2 }, { name: 'View Image', key: 3 }, { name: 'Save Image', key: 4 }];
  textMyan: any = [{ name: 'ကော်ပီကူးမည်', key: 1 }, { name: 'ဖျက်မည်', key: 2 }, { name: 'ကြည့်မည်', key: 3 }, { name: 'သိမ်းမည်', key: 3 }];
  font: string;

  passData: any;
  group: any;
  show: string = "no";

  constructor(public viewCtrl: ViewController, public changefont: Changefont,
    public events: Events, public storage: Storage,
    public navParams: NavParams) {
    this.show = "no";
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      ////console.log('Your language is', font);
      this.changelanguage(font);
    });

    this.passData = this.navParams.get("data");
    this.group = this.navParams.get("group");
    ////console.log("popoverPage=" + this.passData)
  }

  changelanguage(lan) {
    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j < this.textMyan.length; j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      this.popoverItemList = this.textMyan;
      for (let j = 0; j < this.textMyan.length; j++) {
        this.popoverItemList[j].name = this.changefont.UnitoZg(this.textMyan[j].name);
      }
    }
    else {
      this.font = '';
      for (let j = 0; j < this.textEng.length; j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
    }

    for (let i = 0; i < this.popoverItemList.length; i++) {
      if (this.group == 1) {
        if (this.popoverItemList[i].key == 2) {
          this.popoverItemList.splice(i, 1);
          i--;
        }
      }

      if (this.passData.t5 == "1054" || this.passData.t5 == "1058" ||
        this.passData.t5 == "1059" || this.passData.t5 == "1060" ||
        this.passData.t5 == "1061" || this.passData.t5 == "1062") {
        if (this.popoverItemList[i].key == 2) {
          this.popoverItemList.splice(i, 1);
          i--;
        }
      }

      if (this.passData.t3 == '') {
        if (this.popoverItemList[i].key == 3) {
          this.popoverItemList.splice(i, 1);
          i--;
        }
        if (this.popoverItemList[i].key == 4) {
          this.popoverItemList.splice(i, 1);
          i--;
        }
      }

      if (this.passData.t2 == '') {
        if (this.popoverItemList[i].key == 1) {
          this.popoverItemList.splice(i, 1);
          i--;
        }
      }

      this.show = "yes";
    }
  }

  ionViewDidLoad() {
  }

  getAccess(s) {
    this.viewCtrl.dismiss(s);
  }

}
