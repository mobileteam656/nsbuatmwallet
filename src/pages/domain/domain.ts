import { Component } from '@angular/core';
import { NavController, Platform, NavParams, PopoverController, LoadingController, ToastController, Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { DatePipe } from '@angular/common';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { FunctProvider } from '../../providers/funct/funct';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-domain',
  templateUrl: 'domain.html',
  providers: [Changefont]
})

export class Domain {

  isLoading: any;
  textEng: any = ["Domain", "Key", "OK"];
  textMyan: any = ["Domain", "Key", "OK"];
  font: any = '';
  textDatas: string[] = [];
  popover: any;
  ip: any;
  key: any;
  settings: any;
  domain: any;
  errormessagetext: any;
  imglink: any;
  profileImage: any;
  linkStatus: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public popoverCtrl: PopoverController, public loadingCtrl: LoadingController, 
    public http: Http, public toastCtrl: ToastController, 
    public sqlite: SQLite, public datePipe: DatePipe,
    public funt: FunctProvider, public storage: Storage, 
    public changefont: Changefont, public events: Events, 
    public platform: Platform, public global: GlobalProvider) {
    this.linkStatus = this.navParams.get("data");

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("state data=" + JSON.stringify(this.textDatas));
    });

    this.storage.get('settings').then((result) => {
      this.settings = result;

      if (this.settings === "domain") {
        this.storage.get('ip').then((ip) => {
          if (ip !== undefined && ip !== "" && ip !== null) {
            this.ip = ip;
            ////console.log("Your ip is", this.ip);
          }
        });

        this.storage.get('domain').then((domain) => {
          if (domain !== undefined && domain !== "" && domain !== null) {
            this.domain = domain;
            ////console.log("Your domain is", this.domain);
          }
        });
      }
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textDatas[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textDatas[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textDatas[i] = this.textMyan[i];
      }

    }
  }

  savedomain() {
    if (this.domain == '') {
      this.errormessagetext = "Please enter domain."
    } else {
      if (this.domain == 'DM001' || this.domain == 'Dm001' || this.domain == 'dM001' || this.domain == 'dm001') {
        this.errormessagetext = "";
        
        // this.ip = this.global.ipaddressCloud;
        // this.imglink = this.global.digitalMediaCloud;
        // this.profileImage = this.global.digitalMediaProfileCloud;
        
        this.storage.set('ip', this.ip);
        this.storage.set('imglink', this.imglink);
        this.storage.set('profileImage', this.profileImage);

        this.events.publish('ip', this.ip);
        this.events.publish('ip', this.ip);
        this.events.publish('ip', this.ip);

        this.storage.set('settings', 'domain');

        let toast = this.toastCtrl.create({
          message: 'Updated successfully.',
          duration: 5000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);

        //others
        this.storage.set('domain', this.domain);

        this.storage.remove('linkStatus');
        this.storage.set('linkStatus', this.linkStatus);
      } else {
        this.errormessagetext = "Invalid domain. Please try again."
      }
    }
  }
}
