import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { Changefont } from '../changefont/changeFont';
import { DatePipe } from '@angular/common';
import { GlobalProvider } from '../../providers/global/global';
import { TabsPage } from '../tabs/tabs';
import { CreatePopoverPage } from '../create-popover/create-popover';

@Component({
  selector: 'top-up-details',
  templateUrl: 'top-up-details.html',
  providers: [CreatePopoverPage]
})
export class TopUpDetails {

  textEng: any = ["Transaction Result", "Top up successfully.", "Top up fail.",
    "Transaction Date", "Close"];
  textMyan: any = ["ရလဒ်", "လုပ်ဆောင်မှု အောင်မြင်ပါသည်", "လုပ်ဆောင်မှု မအောင်မြင်ပါ",
    "လုပ်ဆောင်ခဲ့သည့်ရက်", "ပိတ်မည်"];
  textData: string[] = [];
  today: string = '';
  param: any;
  title: string = '';

  amount: string = '';
  ipaddress: string;
  font: string = '';
  lan: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public datePipe: DatePipe,
    public changefont: Changefont, public global: GlobalProvider,
    public events: Events, public createPopover: CreatePopoverPage) {
    let date = new Date();
    this.today = this.datePipe.transform(date, 'dd-MM-yyyy');

    this.param = this.navParams.get('parameters');

    this.storage.get('ipaddressWallet').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressWallet;
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
      if (this.param.messageCode == '0000') {
        this.title = this.textData[1];
      } else {
        this.title = this.textData[2];
      }
    })

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      if (this.param.messageCode == '0000') {
        this.title = this.textData[1];
      } else {
        this.title = this.textData[2];
      }
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  goBack() {
    let params = { tabIndex: 3, tabTitle: 'Wallet' };
    this.navCtrl.setRoot(TabsPage, params);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
