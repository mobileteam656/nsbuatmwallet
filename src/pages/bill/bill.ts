import { Component } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';

import { GlobalProvider } from '../../providers/global/global';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { PaymentConfirm } from '../payment-confirm/payment-confirm';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { AllserviceProvider } from '../../providers/allservice/allservice';
@Component({
  selector: 'page-bill',
  templateUrl: 'bill.html',
  providers: [CreatePopoverPage],
})
//Payment Request ( off us ): Payee ( label ) , Balance ( lable ), Beneficiary( Textbox ), Institution Code( Textbox ), Amount( Textbox ), Remark( Textbox )
export class BillPage {
  textEng: any = ["Bills", "Name", "Balance", "Payee", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Pay",
    "Mobile Wallet", "Type", "Payment To", "Reference", "Date", "Please enter amount!", "Please calculate commission!"];
  textMyan: any = ["ငွေပေးသွင်းရန်", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်/ စာရင်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ", "မှတ်ချက်", "မှားယွင်းနေပါသည်", "Does not match! Please try again.", 
  "Please insert name field!", "ပေးသွင်းမည်", "Mobile Wallet", "အမျိုးအစား", "Payment To", "အမှတ်စဉ်", "နေ့စွဲ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "Please calculate commission!"];
  textData: string[] = [];
  errormsg2:string = '';
  type: any = { name: "mWallet", code: "mWallet" };
  institute: any = { name: "", code: "" };
  address: string = '';
  lat: string = '';
  long: string = '';
  userID: string = '';
  balance: string = '';
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: string = '';
  remark: string = '';
  ipaddress: string = '';
  submitted = false;
  isenabled: boolean = false;
  ischecked: boolean = false;
  font: string = '';
  public loading;
  lan: any;
  popover: any;
  regdata: any;
  selectedTitle: string;
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: "", tosKey: "", datetime: "", accountNo: ""
  }
  passData: any;
  resData: any;
  errormsg: any;
  errormsg1: any;
  errormsg3: any;
  messNrc: any;
  temp: any;
  normalizePhone: any;
  readOnly: any;
  merchant: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public menuCtrl: MenuController,
    public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public sqlite: SQLite,
    public changefont: Changefont, 
    public events: Events, public alertCtrl: AlertController,
    public popoverCtrl: PopoverController, public platform: Platform,
    public util: UtilProvider, public global: GlobalProvider,
    public viewCtrl: ViewController, public appCtrl: App,
    public createPopover: CreatePopoverPage
  ) {
    this.readOnly = 0;
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
     // //console.log("Payment userData data=" + JSON.stringify(this._obj));
      if (userData != null || userData != '') {
        this.balance = userData.balance;
      }
    });
    ////console.log("this.global.appCode>>" + this.global.appCode);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    this.institute.name = this._obj.toInstitutionName;
    this.institute.code = this._obj.toInstitutionCode;
    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
      ////console.log("Payment userData ID=" + JSON.stringify(this._obj.userID));
    });
    this.storage.get('ipaddressWallet').then((ip) => {
     // //console.log('Your ip is', ip);
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
       // //console.log("Your ip is", this.ipaddress);
      }
      else {
        this.ipaddress = global.ipaddressWallet;
       // //console.log("Your ip main is", this.ipaddress);
      }
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    }) //...
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("state data=" + JSON.stringify(this.textData));
    });
    this.merchant = this.navParams.get('param');
    ////console.log("Merchant Data is : " + JSON.stringify(this.merchant))
    if (this.merchant != null && this.merchant != undefined && this.merchant != '') {
      this._obj.accountNo = this.merchant.accountNo;
     // //console.log("Merchant Account is : " + JSON.stringify(this._obj.accountNo))
      this._obj.beneficiaryID = this.merchant.merchantID;
      ////console.log("Merchant ID is : " + JSON.stringify(this._obj.beneficiaryID))
      this._obj.toName = this.merchant.merchantName;
      ////console.log("Merchant Name is : " + JSON.stringify(this._obj.toName))
    }
    this.temp = this.navParams.get('data');
    if (this.temp != null && this.temp != undefined && this.temp != '') {
      this._obj.beneficiaryID = this.temp[0];
      this._obj.toInstitutionCode = this.temp[1];
      this._obj.toName = this.temp[2];
      this._obj.amount = this.temp[3];
      this._obj.tosKey = this.temp[6];
      if (this._obj.amount != "" && this._obj.amount != "0" && this._obj.amount != "0.0" && this._obj.amount != "0.00") {
        this.readOnly = 1;
      }
      this._obj.remark = this.temp[4];
      this._obj.datetime = this.temp[5];
      ////console.log("this._obj include data from receipt page>>" + JSON.stringify(this._obj));
    }

    this.storage.get('username').then((username) => {
      ////console.log('Your name is', username);
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });
    this.events.subscribe('username', username => {
      ////console.log('Your user name is', username);
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    })
  }

  ionViewWillEnter() {

    if (this.merchant.merchantID != undefined || this.merchant != '' || this.merchant != null) {
      this._obj.beneficiaryID = this.merchant.merchantID;
      this._obj.toName = this.merchant.merchantName;
    }
  }

  goNext() {
    if (this.temp == null || this.temp == "" || this.temp == undefined) {
      this.goManual();
    }
    else {
      this.goQR();
    }

  }

  goManual() {
    //  let payeePhone = this.util.removePlus(this._obj.userID);
    // let benePhone = this.util.removePlus(this._obj.beneficiaryID);
    this._obj.field1 = "2";
    this._obj.wType = this.type.name;
    let flag = false;
    if (this._obj.beneficiaryID != null && this._obj.beneficiaryID != '' && this._obj.beneficiaryID != undefined) {
      ////console.log("this._obj.beneficiaryID>>" + this._obj.beneficiaryID);
    }
    else if (this.merchant != null && this.merchant != undefined && this.merchant != '') {
      this._obj.accountNo = this.merchant.accountNo;
      //console.log("Go Manual account no:" + JSON.stringify(this._obj.accountNo));
      this._obj.toName = this.merchant.merchantName;
      // //console.log("Go Manual to name:" + JSON.stringify(this._obj.accountNo));
      this._obj.beneficiaryID = this.merchant.merchantID;
      ////console.log("Go Manual account no:" + JSON.stringify(this._obj.accountNo));
    }
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    //this.tosKey = this.temp[6];
    ////console.log("this.temp>>" + this.temp);
    ////console.log("this._obj >> " + JSON.stringify(this._obj));
    this.navCtrl.push(PaymentConfirm, {
      data: this._obj,
    })
  }

  goQR() {
    ////console.log("my amount is : " + this._obj.amount);
    this._obj.field1 = "2";
    this._obj.wType = this.type.name;
    let flag = false;
    if (this.temp != null && this.temp != undefined && this.temp != '') {
      this._obj.beneficiaryID = this.temp[0];
      this._obj.toName = this.temp[2];
      if (this.temp[3] != "" && this.temp[3] != null && this.temp[3] != "0" && this.temp[3] != "0.00" && this.temp[3] != undefined) {
        this._obj.amount = this.temp[3];
      }
      if (this.temp[4] != "" && this.temp[4] != null && this.temp[4] != undefined) {
        this._obj.remark = this.temp[4];
      }
      this._obj.tosKey = this.temp[6];
    }

    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    //this.tosKey = this.temp[6];
    ////console.log("this.temp>>" + this.temp);
    ////console.log("this._obj >> " + JSON.stringify(this._obj));
    /* //console.log("this._obj.toInstitutionName >> " + this._obj.toInstitutionName)
    //console.log("_obj.toInstitutionName >> " + this._obj.toInstitutionName) */
    this.normalizePhone = this.util.normalizePhone(this._obj.beneficiaryID);
    if (this.normalizePhone.flag == true) {
      flag = true;
     // //console.log("normalizePhone.phone>>" + this.normalizePhone.phone);
      this._obj.beneficiaryID = this.normalizePhone.phone;
      ////console.log("this._obj.beneficiaryID>>" + this._obj.beneficiaryID);
      if (flag) {
        this.navCtrl.push(PaymentConfirm, {
          data: this._obj,
        })

      }
    } else {
      flag = false;
      this.errormsg = "Invalid. Please try again!";
      let toast = this.toastCtrl.create({
        message: this.errormsg,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }


  ionViewDidLoad() {
    ////console.log('ionViewDidLoad PaymentOther');
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }


  typeChange(s) {
    this.type = s;
    this.type.name = s.name;
    this.type.code = s.code;
    ////console.log(JSON.stringify(s));

  }

  onChange(s, i) {
    if (i == "09") {
      this._obj.beneficiaryID = "+959";
    }
    else if (i == "959") {
      this._obj.beneficiaryID = "+959";
    }
  }

  instituteChange(s) {
    this._obj.toInstitutionName = s.toInstitutionName;
    this._obj.toInstitutionCode = s.toInstitutionCode;
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }

    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

    }
  }  

}
