import { Component } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';

import { GlobalProvider } from '../../providers/global/global';
import { DatePipe } from '@angular/common';
import { UtilProvider } from '../../providers/util/util';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AccountAdd } from '../account-add/account-add';
import { AccountInfo } from '../account-info/account-info';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
declare var window;

@Component({
  selector: 'page-account-list',
  templateUrl: 'account-list.html',
  providers: [CreatePopoverPage],
})

export class AccountList {
  textEng: any = ["Account List", "Account Number", "Account Type"];
  textMyan: any = ["အကောင့်စာရင်း", "အကောင့်နံပါတ်", "အကောင့်အမျိုးအစား"];
  textData: string[] = [];

  accountList: any;
  tempData: any;

  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: ""
  }

  font: string = '';
  ipaddress: string = '';
  //temp: any;

  popover: any;

  accountTypeList: any;
  //accountType: string = '';
  //accountTypeCaption: string = '';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public changefont: Changefont,
    public events: Events,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public util: UtilProvider,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    private slimLoader: SlimLoadingBarService,
    public appCtrl: App,
    public createPopover: CreatePopoverPage, ) {

    this.storage.get('userData').then((userData) => {
      this._obj = userData;

      this.storage.get('ipaddressWallet').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
          this.getAccType();
        } else {
          this.ipaddress = this.global.ipaddressWallet;
          this.getAccType();
        }
      });

      // if(userData != null || userData != ''){
      //   this.balance = userData.balance;
      // }
    });

    // let institution = this.util.setInstitution(); 
    // this._obj.frominstituteCode = institution.fromInstitutionCode;
    // this._obj.toInstitutionCode = institution.toInstitutionCode;
    // this._obj.toInstitutionName = institution.toInstitutionName;
    // this.institute.name=this._obj.toInstitutionName;
    // this.institute.code= this._obj.toInstitutionCode;

    // this.storage.get('phonenumber').then((phonenumber) => {
    //   this._obj.userID = phonenumber;
    // });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    // this.temp = this.navParams.get('data');
    // if (this.temp != null && this.temp != undefined && this.temp != '') {
    //   this._obj.beneficiaryID = this.temp[0];
    //   this._obj.toInstitutionCode = this.temp[1];
    //   this._obj.toName = this.temp[2];
    // }

    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });

    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    })
  }

  accountInfo(account) {
    this.navCtrl.push(AccountInfo, {
      param: account
    });
  }

  getAccType() {
    this.slimLoader.start(() => { });

    let userID1 = this._obj.userID;//this._obj.userID.replace(/\+/g, '');

    let parameter = { userID: userID1, sessionID: this._obj.sessionID };

    this.http.post(this.ipaddress + '/service001/getAccountType', parameter).map(res => res.json()).subscribe(result => {
      if (result != null && result.code != null && result.code == "0000") {
        let tempArray = [];

        if (!Array.isArray(result.accountTypeList)) {
          tempArray.push(result.accountTypeList);
          result.accountTypeList = tempArray;
        }

        this.accountTypeList = result.accountTypeList;

        this.getAccountList();

        this.slimLoader.complete();
      } else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      }
    },
      error => {
        /* ionic App error
         ............001) url link worng, not found method (404)
         ........... 002) server not response (500)
         ............003) cross fillter not open (403)
         ............004) server stop (-1)
         ............005) app lost connection (0)
         */
        let code;

        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }

        let msg = "Can't connect right now. [" + code + "]";

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      });
  }

  getAccountList() {
    this.slimLoader.start(() => { });

    let userID1 = this._obj.userID;//this._obj.userID.replace(/\+/g, '');

    let parameter = { userID: userID1, sessionID: this._obj.sessionID };

    this.http.post(this.ipaddress + '/service001/getAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result != null && result.code != null && result.code == "0000") {
        let tempArray = [];

        if (!Array.isArray(result.accountList)) {
          tempArray.push(result.accountList);
          result.accountList = tempArray;
        }

        this.accountList = result.accountList;

        for (var i = 0; i < this.accountList.length; i++) {
          for (var j = 0; j < this.accountTypeList.length; j++) {
            if (this.accountList[i].accountType == this.accountTypeList[j].value) {
              this.accountList[i].accountType = this.accountTypeList[j].caption;
            }
          }
        }

        this.tempData = this.accountList;

        this.slimLoader.complete();
      } else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      }
    },
      error => {
        /* ionic App error
         ............001) url link worng, not found method (404)
         ........... 002) server not response (500)
         ............003) cross fillter not open (403)
         ............004) server stop (-1)
         ............005) app lost connection (0)
         */
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }

        let msg = "Can't connect right now. [" + code + "]";

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      });
  }

  getAccounts(ev: any) {
    this.accountList = this.tempData;

    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.accountList = this.accountList.filter((item) => {
        return (item.accNumber.indexOf(val) > -1) || (item.accountType.indexOf(val) > -1);
      });
    }
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";

      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";

      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";

      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  addAccount() {
    this.navCtrl.push(AccountAdd, {})
  }
}
