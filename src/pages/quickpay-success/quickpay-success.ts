import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { WalletPage } from '../wallet/wallet';
import { UtilProvider } from '../../providers/util/util';

@Component({
  selector: 'page-quickpay-success',
  templateUrl: 'quickpay-success.html',
})
export class QuickpaySuccessPage {

  textMyan: any = [ "Bill Payment Details", "Payment Code(Ref No)", "Company/Customer Name", "ငွေပမာဏ","Cancel" , "Submit","Select Account","Wave Account No","Name","ငွေပမာဏ","NRC","Phone No","ငွေပေးချေမှု အောင်မြင်ပါသည်","ပိတ်သည်","Narrative"];
  textEng: any = [ "Bill Payment Details", "Payment Code(Ref No)", "Company/Customer Name", "Amount","Cancel" , "Submit","Select Account","Wave Account No","Name","Amount","NRC","Phone No","Transfer Successful.","Close","Narrative"];  
  showFont: string[] = [];
  billerid = "";

  //data: any[] = [];
  obj: any[] = [];
  amount: any;
  data = {
    refNo: "", name: "", amount: "", account: "", waveAcc: "", nrc: "", phoneNo:"", description:"", waveNo:"", oneNo:""
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage, public util: UtilProvider) {

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
    this.billerid = this.navParams.get("billerid");
     this.data = this.navParams.get("data");
     this.obj = this.navParams.get("obj");
     //this.amount = this.util.formatToDouble(this.data.amount);
     this.amount = this.util.formatAmount(this.data.amount);
     console.log("data==" , JSON.stringify(this.obj));
    // this.name = this.navParams.get("name");
    // this.amount = this.navParams.get("amount");
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickpayPage');
  }
 
  goClose(){
    this.navCtrl.setRoot(WalletPage);
  }
}

