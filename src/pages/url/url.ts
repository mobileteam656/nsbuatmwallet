import { Component } from '@angular/core';
import { NavController, Platform, NavParams, PopoverController, LoadingController, ToastController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { DatePipe } from '@angular/common';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';

@Component({
  selector: 'page-url',
  templateUrl: 'url.html',
  providers: [Changefont]
})

export class URL {

  textEng: any = ["URL", "Key", "OK"];
  textMyan: any = ["URL", "Key", "OK"];
  font: any = '';
  textDatas: string[] = [];
  popover: any;
  ip: any;
  key: any;
  errormessagetext: string;
  flag: string;
  imglink: any;
  profileImage: any;
  linkStatus: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public popoverCtrl: PopoverController, public loadingCtrl: LoadingController, 
    public http: Http, public toastCtrl: ToastController, 
    public sqlite: SQLite, public datePipe: DatePipe,
    public storage: Storage, 
    public changefont: Changefont, public events: Events, 
    public platform: Platform) {
    this.linkStatus = this.navParams.get("data");
    
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("state data=" + JSON.stringify(this.textDatas));
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip !== null) {
        this.ip = ip;
        ////console.log("Your ip is", this.ip);
      }
    });
    this.events.subscribe('ip', ip => {
      if (ip !== undefined && ip !== "" && ip !== null) {
        this.ip = ip;
        ////console.log("Your ip is", this.ip);
      }
    });

    this.storage.get('key').then((key) => {
      if (key !== undefined && key !== "" && key !== null) {
        this.key = key;
        ////console.log("Your key is", this.key);
      }
    });
    this.events.subscribe('key', key => {
      if (key !== undefined && key !== "" && key !== null) {
        this.key = key;
        ////console.log("Your key is", this.key);
      }
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textDatas[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textDatas[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textDatas[i] = this.textMyan[i];
      }
    }
  }

  saveurl() {
    if (this.ip == '') {
      this.errormessagetext = "Invalid! Please try again."
    }
    else if (this.key == '') {
      this.errormessagetext = "Invalid! Please try again."
    }
    else {
      this.errormessagetext = '';

      ////console.log("URL is>>" + this.ip);
      this.events.publish('ip', this.ip);

      let path = this.ip.substr(this.ip.indexOf('/') + 2);
      path = path.substr(0, path.indexOf('/'));
      this.imglink = "http://" + path + "/DigitalMedia/";
      this.profileImage = "http://" + path + "/DigitalMedia/upload/image/userProfile/";
      ////console.log("Path =>" + path);
      this.storage.get('ip').then((result) => {
        if (result == null || result == '') {
          this.storage.set('ip', this.ip);
          this.storage.set('key', this.key);
        }
        else {
          this.storage.remove('ip');
          this.storage.remove('key');
          this.storage.set('ip', this.ip);
          this.storage.set('key', this.key);
          this.storage.set('imglink', this.imglink);
          this.storage.set('profileImage', this.profileImage);
          this.events.publish('ip', this.ip);
          this.events.publish('key', this.key);
        }
        let toast = this.toastCtrl.create({
          message: 'Updated successfully.',
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          //  closeButtonText: this.showFont[10]
        });
        toast.present(toast);

        this.storage.remove('linkStatus');
        this.storage.set('linkStatus', this.linkStatus);
      });
    }
  }

}
