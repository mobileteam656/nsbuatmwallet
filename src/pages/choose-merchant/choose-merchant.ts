import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { App, Events, NavController, NavParams, Platform, PopoverController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { MerchantPaymentPage } from '../merchant-payment/merchant-payment';
import { QrUtilityPage } from '../qr-utility/qr-utility';
import { QuickpayPage } from '../quickpay/quickpay';
import { SkynetTopupPage } from '../skynet-topup/skynet-topup';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
@Component({
  selector: 'page-choose-merchant',
  templateUrl: 'choose-merchant.html',
  providers: [Changefont, CreatePopoverPage]
})
export class ChooseMerchantPage {

  MerchantList: any = [];
  nores: any;
  isLoading: any;
  _obj = {
    userID: "", sessionID: "", paymentType: ""
  }
  userData: any;
  ipaddress: string = '';
  popover: any;
  public loading;
  textEng: any = [
    "Payment Bill"
  ];
  textMyan: any = [
    "ငွေပေးချေမှု"
  ];
  textData: any = [];
  constructor(public navCtrl: NavController, public events: Events,
    public appCtrl: App, public platform: Platform,
    public popoverCtrl: PopoverController, public global: GlobalProvider,
    public toastCtrl: ToastController, public navParams: NavParams,
    public http: Http, public util: UtilProvider,private eventlog:EventLoggerProvider,
    public storage: Storage, public createPopover: CreatePopoverPage) {
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      if (userData != null || userData != '') {
        //  this.balance = userData.balance;
      }
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
      });
      this.isLoading = true;
      // this.storage.get('ipaddressWallet').then((ip) => {
      //   if (ip !== undefined && ip != null && ip !== "") {
      //     this.ipaddress = ip;
      //     this.getPayee();
      //   }
      //   else {
      //     this.ipaddress = this.global.ipaddressWallet;
      //     this.getPayee();
      //   }
      // });
      this.storage.get('ipaddress').then((ip) => {
        if (ip !== undefined && ip !== "" && ip != null) {
          this.ipaddress = ip;
          this.getPayee();

        } else {
          this.ipaddress = this.global.ipaddress;
          this.getPayee();
        }
      });
      this.events.subscribe('ipaddress', ip => {
        this.ipaddress = ip;
      });
      // this.getPayee();
    });
  }

  getPayee() {
    let userid = this._obj.userID;//this.util.removePlus(this._obj.userID);
    let param = {
      userID: userid,
      sessionID: this._obj.sessionID,
      paymentType: this._obj.paymentType
    }
    this.http.post(this.ipaddress + "/service001/getPayee", param).map(res => res.json()).subscribe(result => {
      // if (result.merchantList == undefined || result.merchantList == null || result.merchantList == '') {
      //   this.nores = 0;
      // }

       if (result.code == "0012") {
        this.util.SessiontimeoutAlert(result.desc);
      } else {
        let tempArray = [];

        if (!Array.isArray(result.merchantList)) {
          tempArray.push(result.merchantList);
          result.merchantList = tempArray;
        }
        if (result.merchantList.length > 0) {
          this.nores = 1;
          this.MerchantList = result.merchantList;
        }
      }
      this.isLoading = false;

    }, error => {
      this.nores = 0;
      this.isLoading = false;
      this.getError(error);
    });
    // }
  }

  chooseMerchant(merchant) { 
    this.eventlog.fbevent('merchant_page',{ Message:'MerchantPage'});  
    if (merchant.merchantID === 'M00019') {
      this.navCtrl.push(QrUtilityPage, {
        param: merchant
      });
    } else if (merchant.merchantID === 'M00012'){
      this.eventlog.fbevent('skynet_page',{ Message:'SkynetPage'});
      this.navCtrl.push(SkynetTopupPage, {
        param: merchant
      });
    }else {
      this.eventlog.fbevent('merchant_page',{ Message:'MerchantPage'});
      this.navCtrl.push(MerchantPaymentPage, {
        param: merchant
      });
    }
  }

  // chooseMerchant(merchant) {
  //   if(merchant.merchantID == 'M000001'){
  //     this.navCtrl.push(QrUtilityPage, {
  //       param: merchant
  //     });
  //   } else {
  //     this.navCtrl.push(QuickpayPage, {
  //       //param: merchant
  //     });
  //   } 
  // }

  getError(error) {
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.loading = false;
    // this.loading.dismiss();
    ////console.log("Oops!");
  }

  /*doRefresh(refresher) {
   
    this.getPayee();
    setTimeout(() => {
     
      refresher.complete();
    }, 2000);
  }*/

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  goNext() {
    this.navCtrl.push(QuickpayPage);
  }
}
