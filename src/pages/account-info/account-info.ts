import { Component } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';

import { GlobalProvider } from '../../providers/global/global';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { UtilProvider } from '../../providers/util/util';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
declare var window;

@Component({
  selector: 'page-account-info',
  templateUrl: 'account-info.html',
  providers: [CreatePopoverPage],
})
export class AccountInfo {

  textEng: any = ["Account Info", "Serial Number", "Account Number", "Account Type"];
  textMyan: any = ["အကောင့် အချက်အလက်", "နံပါတ်", "အကောင့်နံပါတ်", "အကောင့်အမျိုးအစား"];
  textData: string[] = [];

  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: ""
  }

  serialNo: string = '';
  accNumber: string = '';
  accountType: string = '';

  font: string = '';
  ipaddress: string = '';

  popover: any;
  temp: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public changefont: Changefont,
    public events: Events,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public util: UtilProvider,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    private slimLoader: SlimLoadingBarService,
    public appCtrl: App,
    public createPopover: CreatePopoverPage, ) {

    this.temp = this.navParams.get('param');
    this.serialNo = this.temp.serialNo;
    this.accNumber = this.temp.accNumber;
    this.accountType = this.temp.accountType;

    this.storage.get('userData').then((userData) => {
      this._obj = userData;

      this.storage.get('ipaddressWallet').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
        } else {
          this.ipaddress = this.global.ipaddressWallet;
        }
      });

      // if(userData != null || userData != ''){
      //   this.balance = userData.balance;
      // }
    });

    // let institution = this.util.setInstitution(); 
    // this._obj.frominstituteCode = institution.fromInstitutionCode;
    // this._obj.toInstitutionCode = institution.toInstitutionCode;
    // this._obj.toInstitutionName = institution.toInstitutionName;
    // this.institute.name=this._obj.toInstitutionName;
    // this.institute.code= this._obj.toInstitutionCode;

    // this.storage.get('phonenumber').then((phonenumber) => {
    //   this._obj.userID = phonenumber;
    // });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    // this.temp = this.navParams.get('data');
    // if (this.temp != null && this.temp != undefined && this.temp != '') {
    //   this._obj.beneficiaryID = this.temp[0];
    //   this._obj.toInstitutionCode = this.temp[1];
    //   this._obj.toName = this.temp[2];
    // }

    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });

    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    })
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";

      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";

      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";

      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
