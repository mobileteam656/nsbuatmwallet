import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ToastController, PopoverController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';

import { GlobalProvider } from '../../providers/global/global';
import { FunctProvider } from '../../providers/funct/funct';
import { Language } from '../language/language';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { UtilProvider } from '../../providers/util/util';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-contact-info',
  templateUrl: 'contact-info.html',
  providers: [Changefont, CreatePopoverPage]
})
export class ContactInfoPage {

  textEng: any = ["Profile", "Mobile Number/Email", "Name", "ADD CONTACT"];
  textMyan: any = ["ကိုယ်ရေးမှတ်တမ်း", "ဖုန်းနံပါတ်/အီးမေး", "နာမည်", "ADD CONTACT"];
  textData: string[] = [];
  font: string = '';
  _obj: any;
  ipaddress: string = '';
  contactInfo: any;
  contactList: any;
  isLoading: boolean;
  profileImageLink: any;

  constructor(public navParams: NavParams, public funct: FunctProvider,
    public platform: Platform, public storage: Storage,
    public http: Http, public toastCtrl: ToastController,
    public changefont: Changefont, public events: Events,
    public global: GlobalProvider, public popoverCtrl: PopoverController,
    public createPopover: CreatePopoverPage, public util: UtilProvider,
    public navCtrl: NavController, ) {
    this.contactInfo = this.navParams.get('data');

    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.profileImageLink = profileImage;
      } else {
        this.profileImageLink = this.global.digitalMediaProfile;
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      this.storage.get('ipaddressChat').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
        } else {
          this.ipaddress = this.global.ipaddressChat;
        }

        if (this.contactInfo.type != "admin") {
          this.getContacts();
        }
      });
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  getContacts() {
    let phone = this.contactInfo.phone;

    let param = {
      userID: this._obj.userID, sessionID: ''
    }

    this.http.post(this.ipaddress + "/chatservice/getContact", param).map(res => res.json()).subscribe(
      result => {
        ////console.log("response getContact =" + JSON.stringify(result));

        if (result.dataList != undefined && result.dataList != null && result.dataList != '') {
          let tempArray = [];

          if (!Array.isArray(result.dataList)) {
            tempArray.push(result.dataList);
            result.dataList = tempArray;
          }
          
          if (result.dataList.length > 0) {
            this.contactList = result.dataList;

            for (let i = 0; i < this.contactList.length; i++) {
              if (this.contactList[i].phone == phone) {
                this.contactInfo.image = this.contactList[i].t18;
              }
            }
          }
        }
        ////console.log("response list =" + JSON.stringify(this.contactList));
      }, error => {
        ////console.log("signin error=" + error.status);
        this.getError(error);
      });
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    ////console.log("Oops!");
  }

}