import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, Platform, PopoverController, Select, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/Rx';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { UtilityConfirmPage } from '../utility-confirm/utility-confirm';

declare var window;
@Component({
  selector: 'page-qr-utility',
  templateUrl: 'qr-utility.html',
})
export class QrUtilityPage {
  @ViewChild('myselect') select: Select;
  textMyan: any = ["ငွေပေးချေမှု", "အကောင့်နံပါတ်", "အမှတ်စဉ်", "ဘေလ်နံပါတ်", "အမည်", "အခွန် ငွေပမာဏ", "အခွန်ဌာန", "အခွန် အမျိုးအစား", "ကုန်ဆုံးမည့် ရက်စွဲ", "ဒဏ်ကြေး", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်", "ကော်မရှင် ငွေပမာဏ", "စုစုပေါင်း ငွေပမာဏ", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "နောက်ကျသည့် ရက်ပေါင်း"];
  textEng: any = ["Utility Payment", "Account Number", "Reference Number", "Bill ID", "Customer Name", "Bill Amount", "Department Name", "Tax Description", "Due Date", "Penalty Amount", "Narrative", "CANCEL", "SUBMIT", "Bank Commission", "Total Amount", "Account Balance", "Belated Days"];
  showFont: string[] = [];
  textErrorEng: any = ["Please choose Account number.", "Please scan QR", "Please fill amount.", "Insufficient Balance"]
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး QR ဖတ်ပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ"];
  textError: string[] = [];
  merchantType: any = [{ name: 'NPTDC', value: '000000' }];
  popover: any;
  ipaddress: string;
  modal: any;
  hardwareBackBtn: any = true;
  userData: any;
  loading: any;
  city: any = {};
  useraccount: any;
  qrValue: any = [];
  qrData: any;
  billAmount: any;
  idleState = 'Not started.';
  timedOut = false;
  min: any;
  sec; any;
  flag: string;
  fromAccountlist: any;
  accountBal: any = '';
  fromaccMsg: any = '';
  font: any;
  isScan: boolean;
  requireScan: any;
  totalAmount: any;
  comAmount: any;
  copyText: any;
  validText: boolean = false;
  buttonText: any;
  penaltyAmount: any;
  amountBal: any;
  placeHolderText:any;
  alertPresented: any;
  merchant:any;
  constructor(public global: GlobalProvider, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public alertCtrl: AlertController, public events: Events,
    public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public platform: Platform,
    public popoverCtrl: PopoverController, public changefont: Changefont, public changeLanguage: ChangelanguageProvider, private slimLoader: SlimLoadingBarService, public barcodeScanner: BarcodeScanner, public all: AllserviceProvider,
    public network: Network, public util: UtilProvider) {
    this.buttonText = 'QR Scan';
    this.amountBal = "";
    this.city.peocessingCode = '000000';
    this.isScan = false;
    this.merchant = this.navParams.get('param');
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('userData').then((result) => {
      this.userData = result;
      this.storage.get('ipaddressApp').then((result) => {
        if (result == null || result == '') {
            this.ipaddress = this.global.ipaddress;
        }
        else {
            this.ipaddress = result;
        }
    })
    });

  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      this.placeHolderText= "For paste Your QR Text";//""  QR စာသားထည့်ရန်
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
    }
    else {
      this.font = "uni";
      this.placeHolderText= "QR စာသားထည့်ရန်";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
    }
  }

 /*  ionViewDidLoad() {
  } */

  validation() {
    if (this.isScan == false && this.city.refNo == undefined) {
      this.requireScan = this.textError[1];
    }
    else {
      this.fromaccMsg = '';
      this.requireScan = '';
      if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this.formatToDouble(this.city.totalAmount))) {
        this.fromaccMsg = this.textError[3];
      } else {
        this.goConfirm();
      }
    }
  }

  goConfirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      if (true) {
        this.loading = this.loadingCtrl.create({
          dismissOnPageChange: true
        });
        this.loading.present();
        let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '1', merchantID: '' };
        this.http.post(this.ipaddress + '/service002/readMessageSetting', param).map(res => res.json()).subscribe(
        data => {
          if (data.code == "0000") {
            this.loading.dismiss();
            this.navCtrl.push(UtilityConfirmPage, {
              data: this.city,
              otp: data.rKey,
              sKey: data.sKey,
              merchantID: this.merchant.merchantID
            })
          }
          else if (data.code == "0016") {
            this.loading.dismiss();
            this.showAlert('Warning!', data.desc);
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 5000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          }
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.all.getErrorMessage(error),
              duration: 5000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
      }
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
      this.loading.dismiss();
    }
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  scanQR() {
    if (this.validText == false) {
      this.barcodeScanner.scan().then((barcodeData) => {
        this.qrValue = barcodeData.text;
        if (barcodeData.cancelled == false) {
          this.isScan = true;
          this.requireScan = '';
          this.decryptData();
        }
      });
    } else {
      this.qrValue = this.copyText;
      this.decryptData();
    }

  }

  decryptData() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, data: this.qrValue };
      this.http.post(this.ipaddress + '/service002/decryptData', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.isScan = true;
          this.requireScan = '';
          this.loading.dismiss();
          this.qrData = data.data;
          this.city.billId = this.qrData[0].billId;
          this.city.refNo = this.qrData[0].refNo;
          this.city.cusName = this.qrData[0].cusName;
          this.city.amount = this.formatAmount(this.qrData[0].billAmount);
          this.city.ccy = this.qrData[0].ccyCode;
          this.city.deptName = this.qrData[0].deptName;
          this.city.taxDesc = this.qrData[0].taxDesc;
          this.city.t1 = this.qrData[0].t1;
          this.city.t2 = this.qrData[0].t2;
          this.city.dueDate = this.qrData[0].dueDate;
          //this.city.toAcc = this.qrData[0].deptAcc;
          this.city.penaltyAccount = this.qrData[0].penaltyAccount;
          this.city.vendorCode = this.qrData[0].vendorCode;
          //this.city.merchantID = this.merchant.merchantID;
          this.city.penaltyAmount = this.formatAmount(this.qrData[0].penalty);
          this.penaltyAmount = this.city.penaltyAmount + "  " + this.city.ccy;
          this.city.comAmount = this.formatAmount(this.qrData[0].bankCharges);
          this.city.belatedDays = this.qrData[0].belatedDays;
          this.billAmount = this.city.amount + "  " + this.city.ccy;
          this.city.totalAmount = this.formatAmount(this.qrData[0].totalAmount);
          this.comAmount = this.formatAmount(this.qrData[0].bankCharges) + "  " + this.city.ccy;
          this.totalAmount = this.city.totalAmount + " " + this.city.ccy;
        }
        else {
          let msg = '';

          if (data.statusCode == '200') {
            msg = "It is already paid."
          } else if (data.statusCode == '400') {
            msg = "It is not available at a moment."
          } else if (data.statusCode == '406') {
            msg = "Bill Reference No. is wrong."
          } else {
            msg = data.desc;
          }

          this.all.showAlert('Warning!', msg);
          this.isScan = false;
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
          this.isScan = false;
        });
  }

  presentAlert(title, desc) {
    let alert = this.alertCtrl.create({
      title: title,
      message: desc,
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });
    alert.present();
  }

  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  checkNetwork() {

    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }SS

  changeAcc(s, account) {
    if (s == 1) {
      this.fromaccMsg = '';
    }
    for (let i = 0; i < this.fromAccountlist.length; i++) {
      if (account == this.fromAccountlist[i].depositAcc) {
        this.amountBal = this.fromAccountlist[i].avlBal;
        this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
      }
    }
  }

  /* ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  } */

  inputChange() {
    if (this.copyText != undefined && this.copyText != '') {
      this.validText = true;
      this.buttonText = 'Check';
    } else {
      this.buttonText = 'QR Scan';
      this.validText = false;
    }
  }

  showAlert(titleText, subTitleText) {
    if (this.alertPresented == undefined) {
      let alert = this.alertCtrl.create({
        title: titleText,
        subTitle: subTitleText,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.alertPresented = undefined;
            }
          }
        ],
        enableBackdropDismiss: false
      });

      this.alertPresented = alert.present();
      setTimeout(() => alert.dismiss(), 2000 * 60);
    }
  }
}
