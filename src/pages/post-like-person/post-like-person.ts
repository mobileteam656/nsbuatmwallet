import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { FunctProvider } from '../../providers/funct/funct';
import { Config } from 'ionic-angular/config/config';

@Component({
  selector: 'page-post-like-person',
  templateUrl: 'post-like-person.html',
})
export class PostLikePersonPage {
  
  passData:any;
  photoLink:any;
  font : any;
  //registerData:any;
  personData:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global: GlobalProvider,
              public funct:FunctProvider,public storage:Storage,public toastCtrl: ToastController,
             public platform:Platform) {                
    //this.photoLink = this.global.digitalMediaProfile ;    
    this.storage.get('language').then((font) => {
      if(font != "zg")
          this.font = 'uni';
        else
          this.font = font;
    });
    this.passData = this.navParams.get("data");    
    this.storage.get('appData').then((data) => {
      //this.registerData = data;
      if(this.passData !=null && this.passData !=undefined && this.passData !=''){
        this.getData(this.passData);
      }      
    }); 
    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.photoLink = profileImage;
      } else {
        this.photoLink = this.global.digitalMediaProfile;
      }
    });    
    this.backButtonExit();
  }

  /* getData(skey){
      let parameter = {
        n1: this.registerData.syskey,
        n2: skey,
        sessionKey: this.registerData.sessionKey   
     }      
      this.http.post(this.global.ipaddressCMS + '/serviceArticle/getArticleLikePerson', parameter).map(res => res.json()).subscribe(data => {
       
          if( data.state && data.data.length > 0){
            for(let i =0;i<data.data.length;i++){
              this.personData.push({
                name:data.data[i].t3,
                img:data.data[i].t16
              })
            }
          }
         
        },
          error => {
          this.getError(error,117);
        });
  }  */
   getData(skey) {
    console.log("request Like Count:" +JSON.stringify(skey));
    this.http.get(this.global.ipaddressCMS + '/serviceArticle/getArticleLikePerson?key=' + skey).map(res => res.json()).subscribe(
      data => {
        console.log("response Like Count:" +JSON.stringify(data.data.length));
        if (data.state && data.data.length > 0) {
          for (let i = 0; i < data.data.length; i++) {
            this.personData.push({
              name: data.data[i].username,
              img: data.data[i].t16
            })
          }
        }
      },
      error => { },
      () => { }
    );
  } 

  backButtonExit(){
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    });
  }

  getError(error,status){   
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg ='';
    if(code == '005'){
      msg = "Please check internet connection!";
    }
    else{
      msg = "Can't connect right now. [" + code + ' - ' + status + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);    
  }

  ionViewDidLoad() {
        
  }

}
