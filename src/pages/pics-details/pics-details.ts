import { Component, ViewChild, ElementRef } from '@angular/core';
import { Events, Content, IonicPage, NavController, NavParams, ActionSheetController, Platform, LoadingController, ToastController, TextInput } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { File } from '@ionic-native/file';
//import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, CameraPosition, MarkerOptions, Marker } from '@ionic-native/google-maps';
import { GoogleMaps } from '../../providers/google-maps';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard';
import { MessageChooseChannelsPage } from '../message-choose-channels/message-choose-channels';
declare var google;

@Component({
  selector: 'page-pics-details',
  templateUrl: 'pics-details.html',
})
export class PicsDetailsPage {

  textEng: any = ["Details", "Write description..."];
  textMyan: any = ["အသေးစိတ်", "စာရိုက်ပါ"];
  textData: string[] = [];
  showFont: any;
  keyboardfont: any;
  language: any;

  //map: GoogleMap;
  imageData: any = '';
  place: any;
  createDate: any = '';
  latLng: any;
  lat: any;
  lng: any;
  markers = [];
  loading: any;
  description: string = '';

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: TextInput;
  @ViewChild('map') mapElement: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    private file: File,
    public toastCtrl: ToastController,
    public events: Events,
    public storage: Storage,
    public changefont: Changefont,
    public map: GoogleMaps
  ) {
    this.showFont = "uni";

    this.imageData = this.navParams.get('detailData');
    this.createDate = this.imageData.CreateDate + ' ' + this.imageData.CreateTime;

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

      if (this.keyboardfont == 'zg') {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      }
    }
  }

  onFocus() {
    this.content.resize();
  }

  ionViewDidLoad() {
    //Number()
    this.imageData = this.navParams.get('detailData');
    this.lat = parseFloat(this.imageData.latitude);
    this.lng = parseFloat(this.imageData.longitude);
    this.loadMap();
    ////console.log('ionViewDidLoad PicsDetailsPage');
  }

  addMarker() {
    window.open('geo://?q=' + this.imageData.latitude + ',' + this.imageData.longitude + '(' + this.imageData.location + ')', '_system');
  }

  sendPicture(data) {
   // //console.log("data=" + data);
    this.navCtrl.push(MessageChooseChannelsPage, {
      data: data,
      from: 'PicsDetailsPage',
      description: this.description
    });
  }

  loadMap() {
    var point = { lat: this.lat, lng: this.lng };
    let divMap = (<HTMLInputElement>document.getElementById('map'));
    this.map = new google.maps.Map(divMap, {
      center: point,
      zoom: 15,
      disableDefaultUI: true,
      draggable: false,
      zoomControl: true
    });

    this.createMapMarker(point);
  }

  createMapMarker(place: any): void {
    var marker = new google.maps.Marker({
      map: this.map,
      position: place
    });
    this.markers.push(marker);
  }

  saveGallery(photo) {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Save to Gallery',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.goGallery(photo);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  goGallery(photo) {
    ////console.log("dowload image == " + JSON.stringify(photo));
    this.loading = this.loadingCtrl.create({
      content: 'Saving...',
    });
    this.loading.present();

    ////console.log("this.file.externalRootDirectory == " + this.file.externalRootDirectory);
    this.file.checkDir(this.file.externalRootDirectory + 'DCIM/', 'DC').then((data) => {
      ////console.log('Directory exists');
      this.copyFileToLocalDir(photo);

    }, (error) => {
      ////console.log('Directory doesnt exist');
      this.file.createDir(this.file.externalRootDirectory + 'DCIM/', 'DC', false).then((res) => {
       // //console.log("file create success");
        this.copyFileToLocalDir(photo);
      }, (error) => {
        //console.log("file create error");
      });
    });
  }

  copyFileToLocalDir(photo) {
    ////console.log("namePaht == " + namePath + "   //// currentNmae == " + currentName + "   ////  newFileName == " + newFileName);
   // //console.log("this.file.datadirectory == " + this.file.externalRootDirectory);
    let correctPath = photo.PhotoPath.substr(0, photo.PhotoPath.lastIndexOf('/') + 1);
    this.file.copyFile(correctPath, photo.name, this.file.externalRootDirectory + 'DCIM/DC', photo.name).then(success => {
      this.presentToast("Saving completed");
      this.loading.dismissAll();
    }, error => {
      this.loading.dismissAll();
      alert('Error while storing file.' + JSON.stringify(error));
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      dismissOnPageChange: true,
      position: 'top'
    });
    toast.present();
  }

}
