import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController, LoadingController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { TabsPage } from '../tabs/tabs';
import { Device } from '@ionic-native/device';
import { Platform } from 'ionic-angular/platform/platform';
import { AllserviceProvider } from '../../providers/allservice/allservice';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
  providers: [Changefont]
})
export class SignUpPage {

  textEng: any = ["Sign Up", "Phone", "Name", "Password", "Confirm Password", "Email Address", "Shop", "Region", "Date of Birth", "Gender", "Register", "Terms and Conditions", "Female", "Male", "Password is required.", "Name is required", "Birth date is required", "Gender is required", "NRC", "Invalid phone number.", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Invalid email address", "Invalid NRC format", "Address", "Institution Code", "Institution Code is required"];
  textMyan: any = ["စာရင်းသွင်းခြင်း", "ဖုန်းနံပါတ်", "အမည်", "လျှို့ဝှက်နံပါတ်", "အတည်ပြုလျှို့ဝှက်နံပါတ်", "အီးမေးလိပ်စာ", ",ေစ ်းဆိုင္", "ဒေသ", "မွေးရက်", "ကျား/ မ", "စာရင်းသွင်းမည်", "စည်းကမ်းနှင့်သတ်မှတ်ချက်များ", "မိန်းမ", "ယောက်ျား", "လျှို့ဝှက်နံပါတ်လိုအပ်ပါသည်", " အမည်လိုအပ်ပါသည်", "မွေးရက်နေ့စွဲလိုအပ်ပါသည်", "ကျား, မလိုအပ်ပါသည်", "မှတ်ပုံတင်", "ဖုန်းနံပါတ်မှားနေပါသည်။", "မှားနေပါသည်။ ထပ်ကြိုးစားပါ!", "လျှို့ဝှက်နံပါတ်မကိုက်ညီ! ထပ်ကြိုးစားပါ။", "အားလုံးဖြည့်စွက်ပါ", "အီးမေး လိပ်စာမှားနေပါသည်", "မှတ်ပုံတင် ပုံစံ မှားနေပါသည်", "Address"];
  textData: string[] = [];
  keyboardfont: any;
  language: any;
  showFont: any;

  ipaddress: string;
  ipaddressCMS: string;
  ipaddressWallet: string;

  userdata: any = {}; dataOne: any;
  public loading;

  phone: string = '';
  name: string = '';
  password: string = '';
  passwordConfirm: string = '';

  /* start all user registration data acmy */
  hautosys: any;
  registerData = {
    syskey: "", phone: "", username: "", realname: "", stateCaption: "",
    photo: "", fathername: "", nrc: "", id: "", passbook: "",
    address: "", dob: "", t1: "", t2: "", t3: "",
    t4: "", t5: "", state: "", status: "", n1: "",
    n2: "", n3: "", n4: "", n5: "", city: ""
  };
  stateList: any = [];
  tempData: any;
  msgForUser: any;
  pdata:any={};
  uuid:string ="";
  /* end all user registration data acmy */

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public changefont: Changefont,
    public events: Events,
    public util: UtilProvider,
    public alertCtrl: AlertController,
    public global: GlobalProvider,
    private device: Device,
    public platform: Platform,
    private all: AllserviceProvider
  ) {
    this.showFont = "uni";
    this.menuCtrl.swipeEnable(false);

    this.userdata = navParams.get("data");
    this.dataOne = navParams.get("dataOne")
    ////console.log("sign up=", JSON.stringify(this.userdata));

    this.phone = this.userdata;
   //  this.name = this.userdata.name;
    // this.ipaddress = this.global.ipaddress;
    // this.ipaddressWallet = this.global.ipaddressWallet;
    // this.ipaddressCMS = this.global.ipaddressCMS;
   
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress= this.global.ipaddress;
      }
      this.readPasswordPolicy();
    });
    
    this.storage.get('ipaddress3').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddressCMS = ip;
      } else {
        this.ipaddressCMS = this.global.ipaddress3;
      }
    });
    this.events.subscribe('ipaddress3', ip => {
      this.ipaddressCMS = ip;
    });
   
   
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;
    });
	
	this.platform.registerBackButtonAction(() => {
      ////console.log("Active Page=" + this.navCtrl.getActive().name);
      let view = this.navCtrl.getActive().name;

      if (view == "SignUpPage") {
        this.navCtrl.pop();
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  checkUndefinedOrNullOrBlank(param) {
    if (param == undefined || param == null || param == "") {
      return true;
    } else {
      return false;
    }
  }

  checkBlankAfterTrim(param) {
    if (param == "") {
      return true;
    } else {
      return false;
    }
  }

  register() {
    let isValid = true;

    if (this.checkUndefinedOrNullOrBlank(this.phone)) {
      isValid = false;
      this.msgForUser = "Invalid Phone.";
      this.showAlert();
    }

    else if (this.checkUndefinedOrNullOrBlank(this.name)) {
      isValid = false;
      this.msgForUser = "Invalid Name.";
      this.showAlert();
    }

    else if (this.checkUndefinedOrNullOrBlank(this.password)) {
      isValid = false;
      this.msgForUser = "Invalid Password.";
      this.showAlert();
    }

    else if (this.checkUndefinedOrNullOrBlank(this.passwordConfirm)) {
      isValid = false;
      this.msgForUser = "Invalid Confirm Password.";
      this.showAlert();
    }

    if (isValid) {
      this.phone = this.phone.trim();
      this.name = this.name.trim();
      this.password = this.password.trim();
      this.passwordConfirm = this.passwordConfirm.trim();

      if (this.checkBlankAfterTrim(this.phone)) {
        this.msgForUser = "Invalid Phone.";
        this.showAlert();
      } else if (this.checkBlankAfterTrim(this.name)) {
        this.msgForUser = "Invalid Name.";
        this.showAlert();
      } else if (this.checkBlankAfterTrim(this.password)) {
        this.msgForUser = "Invalid Password.";
        this.showAlert();
      } else if (this.checkBlankAfterTrim(this.passwordConfirm)) {
        this.msgForUser = "Invalid Confirm Password.";
        this.showAlert();
      } else if (this.password != this.passwordConfirm) {
        isValid = false;
        this.msgForUser = "Password and Confirm Password should be same.";
        this.showAlert();
      } else {
        this.doRegister();
      }
    }
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: this.msgForUser,
      buttons: [{
        text: 'OK',
        handler: () => {
          ////console.log('OK clicked');
        }
      }]
    });

    alert.present();
  }

  doRegister() {
    if (this.keyboardfont == 'zg') {
      this.name = this.changefont.ZgtoUni(this.name);
    }

    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
    });
    this.loading.present();

    let registerT20 = '';

    this.storage.get('linkStatus').then((result) => {
      if (result == 'domain') {
        registerT20 = '0';
      } else {
        registerT20 = '1';
      }
    });
   
    let params = {
      t1: this.phone,
      t2: this.name,
      t3: this.global.domainChat,
      t4: "",
      t9: this.global.appIDChat,
      t16: "",
      t20: registerT20 // one is testing
      //t20: "0" // zero is live
    };

    ////console.log("request registerUser >> " + JSON.stringify(params));

    this.http.post(this.ipaddressCMS + 'service001/registerUser', params).map(res => res.json()).subscribe(result => {
      ////console.log("response service001 registerUser:" + JSON.stringify(result));
      if(this.device.uuid == null){
        this.uuid = "";
      }else this.uuid = this.device.uuid;
      if (result.syskey > 0) {
        let iv = this.all.getIvs();
        let dm = this.all.getIvs();
        let salt = this.all.getIvs();
        let openAccountParams = {
          userID: this.phone,
          name: this.name,
          sessionID: this.dataOne.sessionID,
          sKey: result.syskey,
          field1: this.uuid,
          region: this.global.region,
          phoneType: this.global.type,
          password: this.password,
          "iv": iv, "dm": dm, "salt": salt
        };
        openAccountParams.password = this.all.getEncryptText(iv, salt, dm, openAccountParams.password)
        this.http.post(this.ipaddress + '/service001/openAccount', openAccountParams).map(res => res.json()).subscribe(data => {
          ////console.log("response openAccount >> " + JSON.stringify(data));

          if (data.messageCode == '0000') {
            let hautosys = { hautosys: data.sKey };
            this.storage.set('hautosys', hautosys);
            this.events.publish('hautosys', hautosys);

            let resData = {
              "name": data.name, "accountNo": data.accountNo,
              "sessionID": data.sessionID, "sKey": data.sKey,
              "userID": this.phone,
              "frominstituteCode": data.institutionCode,
              "institutionName": "",
              "balance": this.util.formatAmount(data.balance),
              "field1": data.field1, "field2": data.field2,
              "region": this.global.region, "t16": "user-icon.png"
            };

            // this.storage.remove('phonenumber');
            this.storage.remove('username');
            this.storage.remove('userData');

            this.storage.set('phonenumber', this.phone);
            this.events.publish('phonenumber', this.phone);
            this.storage.set('username', this.name);
            this.events.publish('username', this.name);
            this.storage.set("userData", resData);
            this.events.publish('userData', resData);
            this.storage.set('firstlogin', true);
            this.events.publish('firstlogin', true);

            let appData = { usersyskey: data.sKey, t1: this.phone, t3: data.name, syskey: data.sKey, t16: data.t16 };
            this.storage.set('appData', appData);
            this.events.publish('appData', appData);

            this.storage.set('localPhotoAndLink', data.t16);
            this.events.publish('localPhotoAndLink', data.t16);

            this.hautosys = data.sKey;
          //  this.getStateList();
          //  this.getRegister();

            let params = { tabIndex: 0, tabTitle: "Home" };

            this.navCtrl.setRoot(TabsPage, params).catch((err: any) => {
              ////console.log(`Didn't set nav root: ${err}`);
            });

            this.msgForUser = "You have successfully registered.";
          } else {
            this.msgForUser = data.messageDesc;
          }

          this.loading.dismiss();
          this.showAlert();
        }, error => {
          this.msgForUser = "Your registration fails.";

          this.loading.dismiss();
          this.showAlert();

          this.getError(error);
        });
      } else {
        this.msgForUser = "Your registration fails.";

        this.loading.dismiss();
        this.showAlert();
      }
    }, error => {
      this.msgForUser = "Your registration fails.";

      this.loading.dismiss();
      this.showAlert();

      this.getError(error);
    });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }

    let msg = '';

    if (code == '005') {
      msg = "Please check internet connection!";
    } else {
      msg = "Can't connect right now. [" + code + "]";
    }

    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });

    //toast.present(toast);
  }
  readPasswordPolicy(){
    let param = {
      "userID": this.userdata,
      "sessionID": this.dataOne.sessionID,
      "pswminlength":"",
      "pswmaxlength":"",
      "spchar":"",
      "upchar":"",
      "lowerchar":"",
      "pswno":"",
      "msgCode" : "",
      "msgDesc" : ""
    };
    
    this.http.post(this.ipaddress + '/service001/readPswPolicy', param).map(res => res.json()).subscribe(response => {
      
      if (response.msgCode == "0000") {
        this.pdata=response;
      }
    },
      error => {
        this.getError(error);
      });
  }
}