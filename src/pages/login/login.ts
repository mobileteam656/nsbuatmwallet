import { Component, HostListener, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController, NavParams, LoadingController, PopoverController, ToastController, AlertController, Events, ActionSheetController } from 'ionic-angular';
import { SQLite } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { Changefont } from '../changefont/changeFont';
import 'rxjs/add/operator/map';
import { TabsPage } from '../tabs/tabs';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Device } from '@ionic-native/device';
import { SignUpPage } from '../sign-up/sign-up';
import { Keyboard } from '@ionic-native/keyboard';
import { FCM } from '@ionic-native/fcm';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
import { DefaultPopOver } from '../default-pop-over/default-pop-over';
import { Language } from '../language/language';
import { IpchangePage } from '../ipchange/ipchange';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';

declare var require: any
declare var FacebookAccountKit: any;

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [Changefont, CreatePopoverPage]
})
export class Login {
  @ViewChild('otpCode') otpCode;

  textMyan: any = ["ဖုန်းနံပါတ်", "စာရင်းသွင်းခြင်း", "စာရင်းသွင်းမည်", "အီးမေး လိပ်စာ", "ကျေးဇူးပြုပြီး ဖုန်းနံပါတ် ရိုက်ထည့်ပါ။", "ဘာသာစကားများ",
    "အင်္ဂလိပ်", "ယူနီကုဒ်", "ဇော်ဂျီ", "ပယ်ဖျက်မည်", "ဖုန်းနံပါတ်",
    "စည်းကမ်းနှင့်သတ်မှတ်ချက်များ ကိုသဘောတူခြင်း","NSB Pay", "Invalid UserId", "အတည်ပြု ကုဒ်", "ကုဒ်ရယူပါ",
    "NSB Pay မှကြိုဆိုပါသည်။", "ဝင်မည်"];
  textEng: any = ["Mobile Number", "Registration", "Continue", "Enter Email Address", "Please Enter Mobile Number.", "Languages",
    "English", "Unicode", "Zawgyi", "Cancel", "Phone",
    "By Signing up you agree to the Terms and Conditions", "NSB Pay", "Invalid UserId", "OTP", "Get Code",
    "Welcome to NSB Pay", "Sign in"];

  textdata: string[] = [];
  font: string = '';

  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  errormsg: string = '';
  btnotp:boolean=false;

  //register: any = {};
  userPhone: string = '';
  public loading;
  popover: any;
  ipaddress: string;
  ipaddressCMS: string;
  normalizePhone: any;
  /* start all user registration data acmy */
  hautosys: any;

  registerData = {
    syskey: "", phone: "", username: "", realname: "", stateCaption: "",
    photo: "", fathername: "", nrc: "", id: "", passbook: "",
    address: "", dob: "", t1: "", t2: "", t3: "",
    t4: "", t5: "", state: "", status: "", n1: "",
    n2: "", n3: "", n4: "", n5: "", city: ""
  };
  stateList: any = [];
  tempData: any;
  /* end all user registration data acmy */

  page: string = '';
  languageData: any;
  dataOne: any = {};
  paramOne: any;
  dataTwo: any;
  msgForUser: string;

  uPhno: boolean = false;
  uOtp: boolean = false;
  otp: any = {};
  isenabled: boolean;
  disableBtn: boolean = true;
  storageToken:any;
  uuid:string ="";
  deviceID:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fcm:FCM,
    public http: Http,
    public loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    public toastCtrl: ToastController,
    public storage: Storage,
    public changefont: Changefont,
    public alertCtrl: AlertController,
    public sqlite: SQLite,
    public events: Events,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    public menuCtrl: MenuController,
    public global: GlobalProvider,
    public util: UtilProvider,
    public createPopover: CreatePopoverPage,
    private device: Device,
    public keyboard: Keyboard,
    private faio: FingerprintAIO,
    private eventlog:EventLoggerProvider,
    private firebaseCrashlytics: FirebaseCrashlytics
   
  ) {
    this.events.publish('user:loggedout');
    //this.menuCtrl.swipeEnable(false);
    this.removeStorage();
    /* this.sim.getSimInfo().then((info) => {
      this.userPhone = info.phoneNumber;
      ////console.log('Sim info: ', JSON.stringify(info))
    }, err => console.log("Sim error " + err)); */
    this.storage.get('phonenumber').then((phone) => {
      if (phone != '' || phone != null)
        this.userPhone = phone;
    });
    if (this.device.uuid != '' && this.device.uuid != undefined) {
      this.deviceID = this.device.uuid;
    }
    this.storage.get('firebaseToken').then((firebaseToken) => {
      this.storageToken = firebaseToken;
      console.log('Storage Login Token: '+this.storageToken);
    });
   
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });
    this.events.subscribe('ipaddress', ip => {
      this.ipaddress = ip;
    });
   


    this.storage.get('ipaddressCMS').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddressCMS = ip;
      } else {
        this.ipaddressCMS = this.global.ipaddressCMS;
      }
    });
    this.events.subscribe('ipaddressCMS', ip => {
      this.ipaddressCMS = ip;
    });

    this.events.subscribe('changelanguage', lan => {
      if (lan == "" || lan == null) {
        this.storage.set('language', "eng");
        lan = "eng";
      }
      this.changelanguage(lan);
    }) //...
    this.storage.get('language').then((lan) => {
      if (lan == "" || lan == null) {
        this.storage.set('language', "eng");
        lan = "eng";
      }
      this.changelanguage(lan);
    });
    // this.login();
  }

  changelanguage(lan) {
    this.languageData = lan;
    ////console.log('language' + this.languageData);
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }
  startLoading() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();

  }

  removeStorage() {
    this.storage.remove('phonenumber');
    this.storage.remove('username');
    this.storage.remove('userData');
    this.storage.remove('firstlogin');
    this.storage.remove('appData');
    this.storage.remove('localPhotoAndLink');
    this.storage.remove("stateList");
    this.storage.remove("uari");
  }

  toastInvalidLogin() {
    let toast = this.toastCtrl.create({
      message: "Connection Error.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }
  
  getError(error) {
    let code;

    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }

    let msg = "Can't connect right now. [" + code + "]";

    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });

    toast.present(toast);
  }

  doLocalSaveAndGoHome(data) {
    this.hautosys = data.sKey;
    let sysKey = { hautosys: this.hautosys };
    this.storage.set('hautosys', sysKey);
    this.events.publish('hautosys', sysKey);
    let resData = {
      "sKey": data.sKey, "userID": this.userPhone,
      "name": data.name, "sessionID": data.sessionID,
      "accountNo": data.accountNo, "balance": this.util.formatAmount(data.balance),
      "frominstituteCode": data.institutionCode, "institutionName": "",
      "field1": data.field1, "field2": data.field2,
      "region": this.global.region, "t16": data.t16
    };

    this.storage.set('phonenumber', this.userPhone);
    this.events.publish('phonenumber', this.userPhone);
    this.storage.set('username', data.name);
    this.events.publish('username', data.name);
    this.storage.set("userData", resData);
    this.events.publish('userData', resData);
    this.storage.set('firstlogin', true);
    this.events.publish('firstlogin', true);
    let appData = { usersyskey: data.sKey, t1: this.userPhone, t3: data.name, syskey: data.sKey, t16: data.t16 };
    this.storage.set('appData', appData);
    this.events.publish('appData', appData);
    this.storage.set('localPhotoAndLink', data.t16);
    this.events.publish('localPhotoAndLink', data.t16);
    this.loading.dismiss();
    this.events.subscribe('changelanguage', lan => {
      if (lan == "" || lan == null) {
        this.storage.set('language', "eng");
        lan = "eng";
      }
      this.changelanguage(lan);
    }) //...

    this.storage.get('language').then((lan) => {
      if (lan == "" || lan == null) {
        this.storage.set('language', "eng");
        lan = "eng";
      }
      this.changelanguage(lan);
    });
    let params = { tabIndex: 0, tabTitle: "Home" };
    this.navCtrl.setRoot(TabsPage, params).catch((err: any) => {
    
    });
  }
  
  showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: msg,
      buttons: [{
        text: 'OK',
        handler: () => {
          ////console.log('OK clicked');
        }
      }]
    });
    alert.present();
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      position: 'bottom',
      showCloseButton: true,
      dismissOnPageChange: true,
      closeButtonText: 'OK'
    });
    toast.present(toast);
  }

  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    var code = event.keyCode || event.which;
    if (code === 13) {
      event.preventDefault();
      if (this.uPhno == true) {
        this.otpCode.setFocus();
        this.uPhno = false;
        this.uOtp = true;
      } else if (this.uOtp == true) {
        //this.keyboard.close();
        this.uOtp = false;
        if (this.otp.length == 6) {
          this.loginV2();
        } else {
          this.showAlert("Please click get code first");
        }
      }
    }
  }
  onChange(i, a) {
    if (a == 1) {
      this.uPhno = true;
      this.uOtp = false;
    } else {
      this.uPhno = false;
      this.uOtp = true;
      this.otp=i;
      this.disableBtn=false;
      if(this.otp.length==6 && this.btnotp==true){
        this.disableBtn=false;
      }
      else{
        this.disableBtn=true;
      }
    }
  }

  ionViewDidLoad() {
    //this.keyboard.disableScroll(false);
  }


  getLoginOTP() {
    if (this.userPhone != undefined && this.userPhone != null && this.userPhone != "" && this.userPhone.trim() != "") {
      this.normalizePhone = this.util.normalizePhone(this.userPhone);
      if (this.normalizePhone.flag == true) {
        this.errormsg = "";
        this.userPhone = this.normalizePhone.phone;
        if(this.device.uuid == null){
          this.uuid = "";
        }else this.uuid = this.device.uuid;
        // start get otp
        this.startLoading();
        let paramOTP = {
          userID: this.userPhone,
          type: "11",
          deviceID:this.uuid
        };
        this.http.post(this.ipaddress + '/service001/getLoginOTP', paramOTP).map(res => res.json()).subscribe(dataTwo => {
          this.loading.dismiss();
          console.log(dataTwo.code);
          if (dataTwo.code == "0000") {
            this.otp = "";
            this.btnotp=true;
            this.storage.set("phonenumber", this.userPhone);
            this.dataTwo = dataTwo;
            //let msg = dataTwo.desc;
            let toast = this.toastCtrl.create({
              message: 'OTP received',
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
        
            toast.present(toast);
            this.eventlog.fbevent('OTP_Success', { userID: this.userPhone,Message:dataTwo.desc });//use eventprovider
          } else if (dataTwo.code == "0014"){
                this.btnotp=false;
                let msg =dataTwo.desc;
                let toast = this.toastCtrl.create({
                  message: msg,
                  duration: 3000,
                  position: 'bottom',
                  //  showCloseButton: true,
                  dismissOnPageChange: true,
                  // closeButtonText: 'OK'
                });
        
            toast.present(toast);
            this.eventlog.fbevent('OTP_Fail', { userID: this.userPhone,Message:dataTwo.desc });
            this.removeStorage();

          }
          else {
            this.btnotp=false;
            //let msg = dataTwo.desc;
            let toast = this.toastCtrl.create({
              message: 'Invalid Your Wallet Phone Number',
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
        
            toast.present(toast);
            this.eventlog.fbevent('OTP_Fail', { userID: this.userPhone,Message:dataTwo.desc });
            this.removeStorage();
          }
        }, error => {
          const crashlytics = this.firebaseCrashlytics.initialise();
          crashlytics.logException('my caught exception');
          this.loading.dismiss();
          this.removeStorage();
          this.toastInvalidLogin();
        });
      } else {
        this.errormsg = "Invalid Mobile Number.";
      }
    } else {
      this.userPhone = "";
      this.errormsg = this.textdata[4];
    }
  }

  loginV2() {
    this.removeStorage();
    console.log('User Information'+ this.userPhone);
    if (this.otp != undefined && this.otp != null && this.otp != "" && this.otp.length == 6 ) {//&& this.dataTwo != undefined
      this.errormsg = "";
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      if(this.device.uuid == null){
        this.uuid = "";
      }else this.uuid = this.device.uuid;
      this.loading.present();
      let checkOTPParams = {
        userID: this.userPhone,
        sessionID: this.dataTwo.sessionID,
        rKey: this.dataTwo.rKey,
        otpCode: this.otp,
        fcmToken:this.storageToken,
        deviceID:this.uuid
      };
      this.http.post(this.ipaddress + '/service001/login', checkOTPParams).map(res => res.json()).subscribe(
        dataResponse => {
          this.loading.dismiss();
          if (dataResponse.messageCode == '0000') {
            this.doLocalSaveAndGoHome(dataResponse);
            this.eventlog.fbevent('Login_Success',{ userID: this.userPhone,Message:dataResponse.messageDesc });
           
          }
          else if (dataResponse.messageCode == '0009') {
            this.navCtrl.setRoot(SignUpPage, {
              data: this.userPhone,
              dataOne: dataResponse
            });
          }
          else {
            this.eventlog.fbevent('Login_Fail',{ userID: this.userPhone,Message:dataResponse.messageDesc });
            this.showAlert(dataResponse.messageDesc);
          }
        }, error => {
          this.loading.dismiss();
          
          this.msgForUser = "Fails.";
          // this.showAlert(this.msgForUser);
          this.getError(error);
        }
      );
    } else {
      this.showAlert("Invalid OTP Code");
     
    }
  }
  fingerPrint() {
    this.platform.ready()
      .then(() => this.faio.isAvailable())
      .then(result => {
        if (result === "finger" || result === "face") {
          this.faio.show({
            clientId: 'Fingerprint-Demo',
            clientSecret: 'password',
            disableBackup: true, // Only Android
          })
            .then((result: any) => {
              this.loading = this.loadingCtrl.create({
                content: "Processing",
                dismissOnPageChange: true
                // duration: 3000
              });
              this.loading.present();
              console.log("Test fingerprint 2 :" + JSON.stringify(result));
              console.log("FingerPrint deviceID :" + this.deviceID);
              let param = {
                deviceID: this.deviceID
              }
              this.http.post(this.ipaddress + '/service001/checkDeviceIdV2', param).map(res => res.json()).subscribe(
                dataResponse => {
                  this.loading.dismiss();
                  if (dataResponse.messageCode == '0000') {
                  this.userPhone=dataResponse.userId;
                  this.storage.set("phonenumber",this.userPhone);
                   console.log('FingerPrint UserID: '+dataResponse.userId)
                    console.log('Finger Print Data'+JSON.stringify(dataResponse));
                    this.doLocalSaveAndGoHome(dataResponse);
                    this.eventlog.fbevent('Login_Success',{ userID: this.userPhone,Message:dataResponse.messageDesc});
                   }
                  
                  else if (dataResponse.messageCode == '0018') {
                    this.showAlert(dataResponse.messageDesc);
                  }
                  else {
                    this.eventlog.fbevent('Login_Success',{ userID: this.userPhone,Message:dataResponse.messageDesc});
                    this.showAlert(dataResponse.messageDesc);
                  }
                }, error => {
                  this.loading.dismiss();
                  
                  this.msgForUser = "Fails.";
                  // this.showAlert(this.msgForUser);
                  this.getError(error);
                }
              );
            })
            .catch((error: any) => {
              console.log('err: ', error);
            });
        }

        else {
          //Fingerprint or Face Auth is not available //Face
          let toast = this.toastCtrl.create({
            message: "Fingerprint Authentication is not available on this device!",
            duration: 3000,
            position: 'bottom',
            // showCloseButton: true,
            dismissOnPageChange: true,
            //  closeButtonText: 'OK'
          });
          toast.present(toast);
        }
      })
  }
  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(DefaultPopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data == "0") {
        this.navCtrl.push(IpchangePage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      /* else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      } */ 
      else if (data == "3") {
        window.open('https://play.google.com/store/apps/details?id=wallet.NSBmawallet', '_system');
      }

    });
  }
  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(Language, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);


    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 0 };
        temp.data = data;
        temp.status = 0;
        this.events.publish('changelanguage', temp);
      }


    });
  }
 }
