import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Events, LoadingController, ToastController, AlertController, ViewController } from 'ionic-angular';
import { PopOverListPage } from '../pop-over-list/pop-over-list';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';

import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { Http } from '@angular/http';
import { App } from 'ionic-angular';
import { TransferDetails } from '../transfer-details/transfer-details';
import { TransferFailPage } from '../transfer-fail/transfer-fail';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-transfer-password',
  templateUrl: 'transfer-password.html',
  providers: [Changefont, CreatePopoverPage]
})
export class TransferPasswordPage {
  popover: any;
  textEng: any = ['Enter Password', "Enter valid password", "Password", "Confirm", "Invalid Password"];
  textMyan: any = ['လျှို့ဝှက်နံပါတ်ကိုရိုက်ထည့်ပါ', "မှန်ကန်သောလျှို့ဝှက်နံပါတ်ကိုရိုက်ထည့်ပါ", "လျှို့ဝှက်နံပါတ်", "အတည်ပြုပါ"];
  textdata: any = [];

  font: string = '';

  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  errormsg: string = '';
  ipaddress: string;
  ipaddressCMS: string;
  public alertPresented: any;
  isLoading: any;
  loading: any;
  resData: any;
  password: any;
  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "reference": "", "toAcc": "",
    "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2",
    "sKey": "", fromName: "", toName: "", "wType": "",
    "field1": "", "field2": ""
  }
  tosKey: any;
  msgparam: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController,
    public global: GlobalProvider, public util: UtilProvider, public events: Events, public toastCtrl: ToastController,
    public createPopover: CreatePopoverPage, public storage: Storage, public changefont: Changefont, public loadingCtrl: LoadingController,
    private slimLoader: SlimLoadingBarService, public alertCtrl: AlertController, public http: Http, private all: AllserviceProvider,
    private firebase: FirebaseAnalytics,public appCtrl: App,public viewCtrl: ViewController) {

    this.tosKey = this.navParams.get('tosKey');
    this.msgparam = this.navParams.get("messageParam");
    this._obj = this.navParams.get('data');
    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });
    this.events.subscribe('ip', ip => {
      this.ipaddress = ip;
    });

    this.storage.get('ipaddressCMS').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddressCMS = ip;
      } else {
        this.ipaddressCMS = this.global.ipaddressCMS;
      }
    });
    this.events.subscribe('ipaddressCMS', ip => {
      this.ipaddressCMS = ip;
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      this.language = this.englanguage;
      for (let i = 0; i < this.textEng.length; i++)
        this.textdata[i] = this.textEng[i];
    } else if (font == "zg") {
      this.font = "zg";
      this.language = this.changefont.UnitoZg(this.myanlanguage);
      for (let i = 0; i < this.textMyan.length; i++)
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
    } else {
      this.font = "uni";
      this.language = this.myanlanguage;
      for (let i = 0; i < this.textMyan.length; i++)
        this.textdata[i] = this.textMyan[i];
    }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopOverListPage, {}, { cssClass: 'login-popover' });
    this.popover.present({
      ev: ev
    });

    this.popover.onWillDismiss(data => {
      if (data.key == 0) {
        this.createPopover.presentPopoverCloud(ev);
      } else if (data.key == 1) {
        this.createPopover.presentPopoverLanguage(ev);
      } else if (data.key == 3) {
        this.createPopover.presentPopoverFont(ev);
      }
    });
  }
  
  getconfirm() {
    if (this.password == undefined || this.password == null || this.password == "") {
      // this.showAlert('Warning!', this.textdata[0]);
      let toast = this.toastCtrl.create({
        message: "Please enter password",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
     
      toast.present(toast);
      this.loading.dismiss();
    }
    else{
      this.viewCtrl.dismiss(this.password);

    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  showAlert(titleText, subTitleText) {
    if (this.alertPresented == undefined) {
      let alert = this.alertCtrl.create({
        title: titleText,
        subTitle: subTitleText,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.alertPresented = undefined;
            }
          }
        ],
        enableBackdropDismiss: false
      });

      this.alertPresented = alert.present();
      setTimeout(() => alert.dismiss(), 2000 * 60);
    }
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);
    this.isLoading = false;
  }

  goPost() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let amount = this.util.formatToDouble(this._obj.amount);
    //let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
    //let benePhone = this._obj.beneficiaryID;//this.util.removePlus(this._obj.beneficiaryID);    
    //let institution = this.util.setInstitution();
    //this._obj.frominstituteCode = institution.fromInstitutionCode;
    //this._obj.toInstitutionCode = institution.toInstitutionCode;
    
    let param = {
    token: this._obj.sessionID,
    senderCode: this._obj.userID,
    receiverCode:  this._obj.beneficiaryID,
    fromName:this._obj.fromName,
    toName: this._obj.toName,
    amount: amount, 
    prevBalance: this._obj.balance,  

     /*  userID: payeePhone, 
      sessionID: this._obj.sessionID, 
      amount: amount, 
      receiveID:benePhone,
      fromName:this._obj.fromName,
      toName: this._obj.toName, */
    };
    this.http.post(this.ipaddress + '/payment/goWalletTransfer', param).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
         };
      let code = data.code;
      if (code == '0000') {
        this.resData = data;
        let transfermsg = "Transferred " + amount + " Ks from " + this._obj.fromName + " (" + this._obj.userID + ") to " + this._obj.toName
          + " (" + this._obj.userID + ") with Trans No  : " + this.resData.bankRefNo + ".";
        let transfermsgParam = {
          t2: transfermsg,
          t3: '', //photo
          t5: this._obj.sKey, //from (reg sykey)
          t6: this.tosKey, // to
          t9: this._obj.sKey, // reg sykey
          t11: '',
          n1: 0,
          t12: "ios",
          t15: "",
          isGroup: 0,
          n20: 0,
          groupName: ""
        };
        this.firebase.logEvent('agent_transfer_success', { Receiver:this._obj.beneficiaryID,Sender:this._obj.userID,Message:data.desc,Code:data.code})
        .then((res: any) => { console.log(res); })
        .catch((error: any) => console.error(error));
              this.appCtrl.getRootNav().setRoot(TransferDetails, {
                data: resdata,
                data1: this._obj.amount,
                data2: this._obj.toName
              });
           this.loading.dismiss();
      }
      else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                // this.storage.remove('phonenumber');
                this.firebase.logEvent('agent_transfer_fail', { Receiver:this._obj.beneficiaryID,ErrorMessage:"agent transfer fail",Code:data.code})
                .then((res: any) => { console.log(res); })
                .catch((error: any) => console.error(error));
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.resData = [];
        this.firebase.logEvent('agent_transfer_fail', { Receiver:this._obj.beneficiaryID,ErrorMessage:"agent transfer fail",Code:data.code})
        .then((res: any) => { console.log(res); })
        .catch((error: any) => console.error(error));
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
       
        toast.present(toast);
        this.loading.dismiss();
        
      }
    }
    ,
      error => {
        ////console.log("error=" + error.status);
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }

      
      
    );
  }

}
