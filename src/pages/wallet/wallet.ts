import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner, BarcodeScannerOptions } from "@ionic-native/barcode-scanner";
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { SQLite } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { ActionSheetController, AlertController, App, Events, LoadingController, MenuController, NavController, NavParams, Platform, PopoverController, ToastController, ViewController } from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { AccountJoinPage } from '../account-join/account-join';
import { AccountList } from '../account-list/account-list';
import { AccountTransaction } from '../account-transaction/account-transaction';
import { Changefont } from '../changefont/changeFont';
import { ChooseContactPage } from '../choose-contact/choose-contact';
import { ChooseMerchantPage } from '../choose-merchant/choose-merchant';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { LocationPage } from '../location/location';
import { Login } from '../login/login';
import { NotificationPage } from '../notification/notification';
import { PaymentTypePage } from '../payment-type/payment-type';
import { Payment } from '../payment/payment';
import { QrPayment } from '../qr-payment/qr-payment';
import { QrReceipt } from '../qr-receipt/qr-receipt';
import { QuickpayPage } from '../quickpay/quickpay';
import { TabsPage } from '../tabs/tabs';
import { TopUp } from '../top-up/top-up';
import { SkynetTopupPage } from '../skynet-topup/skynet-topup';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
import { MobileLoginPage } from '../mobile-login/mobile-login';

@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
  providers: [CreatePopoverPage]
})
export class WalletPage {
  letterObj = {
    to: '',
    from: '',
    text: ''
  }

  textMyan: any = [
    "NSB Pay", "ငွေလွှဲရန်", "အခြားသို့ ငွေလွှဲရန်",
    "စာရင်း", "QR", "လက်ကျန်ငွေ",
    "အမျိုးအစား", 'အချက်အလက်သစ်များရယူရန်အောက်သို့ဆွဲပါ', 'အချက်အလက်သစ်များရယူနေသည်',
    "ငွေပေးသွင်းရန်", "ငွေလက်ခံရန်", "မင်္ဂလာပါ",
    "ချက်တင်", "NFC", "အကောင့်အကျဉ်းချုပ်",
    "လက်ကျန်ငွေ", "မှတ်ပုံတင်", "ငွေတောင်းခံ",
    "ဥပဒ", "Forms", "ဘဏ်အကောင့်", 
    "ငွေသား", "ဘေလ်","ငွေဖြည့်ရန်","ငွေပေးချေ","ငွေလက်ခံ","ေါ(လ်)လက်",
    " လက်ကျန်ငွေ ","ကဒ်အသစ်ထည့်ရန်","ကျပ်","လုပ်ဆောင်မှု မှတ်တမ်း","လုပ်ဆောင်မှုမှတ်တမ်း","အားလုံးကြည့်ရန်",
    "အချက်အလက်မရှိပါ","ကိုယ်စားလှယ်","အချက်အလက်မရှိပါ","Skynet Payment","ငွေသွင်း", "ငွေထုတ်"]; //uin
  textEng: any = [
    "NSB Pay", "Transfer", "Transfer to Others",
    "Transactions", "QR", "Balance",
    "Type", 'Pull to refresh', 'Refreshing',
    "Payment", "Receive", "Hello",
    "Chat", "NFC", "Account Summary",
    "Balance", "Registration", "Utility Payment",
    "Law", "Forms", "Bank Accounts", 
    "Cash", "Bills","Top Up","Scan","My QR","Wallet",
    " Balance"," Add New Card","MMK","Transactions","Activity","View all",
    "No Activity","Agent","No result found!","Skynet Payment","Cash In","Cash Out"]; //uin
  textData: any = [];
  showFont: any = '';
  advertiseData: any = [];
  confirm: any;
  level: any;
  ipaddress: string;
  popover: any;
  selectedTitle: string;
  activity:string;
  showText: string;
  currency:string;
  carryData: any = {};
  displayArray:any=[];
  displayArray1:any=[];
  count:string;
  tokencount=0;
  shownoti:boolean=false;


  // transactionflag=false;
  // flag=false;
  recentTransDate:string;
  username: string = '';
  resultdata= {
      "accRef": "", "codTxnCurr": "", "nbrAccount": "","state":"","subRef":"",
      "txnAmount": "", "txnDate": "", "txnDateTime": "", "txnTypeDesc": "", "txtReferenceNo": "", 
     
    };
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "",
    frominstituteCode: "", toInstitutionCode: "", toAcc: "", accountNo:"",balance: "", bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "", field1: "", field2: ""
  };
  
  type: any = { name: "mWallet", code: "mWallet" };
  dType: any;

  options: BarcodeScannerOptions;
  photoName: any;
  loadingIcon: any;
  latitude: any;
  longitude: any;
  location: any;
  buttonText: any;
  scannedText: any;
  docData: any;
  isLoading: any;
  public loading;
  qrValue: any = [];
  eye2: string;
  open:boolean=true;
  close:boolean=true;
  transactionflag=false;
  fromDate: any = '';
  toDate: any = '';
  currentpage: any = 1;
  pageSize: any = 10;
  duration: any = 0;
  durationmsg: any;
  passTemp: any = {};
  todayDate: any = '';
  displayrecentTransaction:string;
  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public changefont: Changefont,
    public storage: Storage,
    public events: Events,
    public sqlite: SQLite,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public popoverCtrl: PopoverController,
    public global: GlobalProvider,
    public appCtrl: App,
    public http: Http,
    public toastCtrl: ToastController,
    public util: UtilProvider,
    public createPopover: CreatePopoverPage,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public barcodeScanner: BarcodeScanner,
    private all: AllserviceProvider,
    public datePipe: DatePipe,
    private eventlog:EventLoggerProvider
  ) {
    
    this.events.publish('user:loggedin');
    this.showFont = "uni";
    this.dType = 'digital';
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
      this.getBalance();
      this.getAdLink();
      this.goSearch();
      this.getNotiCount();

    });
    
    // this.storage.get('ipaddress').then((result) => {
    //   this.ipaddress = result;
    //   this.getBalance();
    //   this.getAdLink();
    //   this.goSearch();
    //   this.getNotiCount();
    // });
   
    /*  this.events.subscribe('userData', userData => {
       this._obj = userData;
     }); */
    
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      console.log('Wallet userData: '+ JSON.stringify(this._obj));
      this.refreshPage();
      this.see();
    });
    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
    });
    this.storage.get('username').then((username) => {
      this.username = username;
    });
    this.events.subscribe('username', username => {
      this.username = username;

    })
    this.events.subscribe('userData', userData => {
      if (userData !== undefined && userData !== "") {
        this._obj = userData;
      }
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
    
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  getBalance() {
    // if(this.global.noticount==0){
    //   this.shownoti=false;
    //   this.count='';

    // }
    // else{
    //   this.shownoti=true;
    //   this.count=this.global.noticount.toString();
    // }
    
    if (this.global.netWork == 'connected') {
      let param = {
        "userID": this._obj.userID,
        "sessionID": this._obj.sessionID
      };

      this.http.post(this.ipaddress+'/wallet/getBalanceWL', param).map(res => res.json()).subscribe(data => {
        console.log('IP address is: '+this.ipaddress);
        if (data.messageCode == '0000') {
          this._obj.balance = this.util.formatAmount(data.balance);

          this.storage.get('userData').then((userData) => {
            let userDataTemp = userData;
            userDataTemp.balance = this._obj.balance;
            this.global.amount = this._obj.balance;
            // console.log(this._obj.balance);
            // this. userDataTemp.balance="*****";
            // console.log(userDataTemp.balance);
            // console.log(this._obj.balance);
            this.storage.set("userData", userDataTemp);
            this.events.publish('userData', userDataTemp);
          });
        } else if (data.messageCode == "0012") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.messageDesc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  // this.storage.remove('phonenumber');
                  this.storage.remove('username');
                  this.storage.remove('userData');
                  this.storage.remove('firstlogin');
                  this.appCtrl.getRootNav().setRoot(Login);
                }
              }
            ]
          })
          confirm.present();
          //this.loading.dismiss();
        }
      }, error => {
        this.getError(error);
      });
    }
      
  }

  itemTapped() {
    this.count = "";
    this.removeNotiCount();
    this.navCtrl.push(NotificationPage);
  }

 

  gomenu(s) {
    if (s == 1) {
      console.log('Wallet Home Page: '+this._obj.userID);
      this.eventlog.fbevent('choose_contact_page',{ userID:this._obj.userID,Message:"Transfer"});
      // alert("Transfer choose contact");
      this.navCtrl.push(ChooseContactPage);
    } else if (s == 3) {
      this.eventlog.fbevent('transaction_page',{ userID:this._obj.userID,Message:"Transactions"});
      // alert("View All")
      this.navCtrl.push(AccountTransaction);
    } else if (s == 5) {
      this.navCtrl.push(QrPayment);
    } else if (s == 6) {
      this.eventlog.fbevent('qr_scan_page',{ userID:this._obj.userID,Message:"QR scan"});
      this.navCtrl.push(QrReceipt);
    // }else if (s == 12){
    //   this.navCtrl.push(ScanPage);
    }
     else if (s == 7) {
      this.navCtrl.push(TabsPage);
    } else if (s == 9) {
      this.navCtrl.push(TabsPage);
    } else if (s == 10) {
      this.navCtrl.push(AccountList);
    } else if (s == 11) {
      this.eventlog.fbevent('choose_merchant_page',{ userID:this._obj.userID,Message:"Choose Merchant"});
      this.navCtrl.push(ChooseMerchantPage);
      //this.navCtrl.push(QrUtilityPage);
    }  else if (s == 13) {
      this.eventlog.fbevent('location_page',{ userID:this._obj.userID,Message:"Location"});
      this.navCtrl.push(LocationPage);
    } else if (s == 14){
      this.navCtrl.push(QuickpayPage);
    }
    else if (s == 15){
      this.eventlog.fbevent('payment_page',{ userID:this._obj.userID,Message:"Payment"});
      this.navCtrl.push(PaymentTypePage);
    }/* else if (s == 16){
      this.navCtrl.push(SkynetTopupPage);
    } */

  }
  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  refreshPage() {
    this.storage.get('ipaddress').then((result) => {
      this.ipaddress = result;
    this.getBalance();
    this.goSearch();
    this.getNotiCount();
    });
    //this.getNotification();
  }

  ionViewWillEnter() {
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      this.refreshPage();
    });
    this.menuCtrl.swipeEnable(true);
  }

  typeChange(s) {
    this.type = s;
    this.type.name = s.name;
    this.type.code = s.code;
    ////console.log(JSON.stringify(s));
  }

  doRefresh(refresher) {
    this.refreshPage();

    setTimeout(() => {
      ////console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    // if (code == '005') {
    //   msg = "Please check internet connection!";
    // }
    // else {
    //   msg = "Can't connect right now. [" + code + "]";
    // }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //showCloseButton: true,
      dismissOnPageChange: true,
      //closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    ////console.log("Oops!");
  }
  see(){
    console.log( this._obj.balance);
    this.getBalance();
    this.global.seen = false;
    this.global.unseen = true;
       }
  unseen(){
     this.global.amount="*********";
     this.global.seen = true;
     this.global.unseen = false;
       }

  goReceive(a) {
    this.eventlog.fbevent('qr_receive',{ userID:this._obj.userID,Message:"Qr Receive"});
    if (a ==1) {
      this.navCtrl.push(QrReceipt, {
        type: 'Receive'
      })
    }
    else if (a ==2) {
      this.navCtrl.push(QrReceipt, {
        type: 'Cash In'
      })
    }

  }
      

  goScan() {
    this.buttonText = "Loading...";
    this.loading = true;
    this.options = {
      prompt: "Place the QR code within the scanning frame",
      showTorchButton: true,
      // showAlbumButton:true,
      torchOn: false,
      resultDisplayDuration: 500,
      formats: "QR_CODE,PDF_417",
      orientation: "portrait",
      disableSuccessBeep: true // iOS and Android       

    }
    // this.actionSheet();
    this.barcodeScanner.scan(this.options).then((barcodeData) => {
      this.qrValue = barcodeData.text;
      if (barcodeData.cancelled == false) {
        try {
          this.qrValue = this.all.getDecryptText(this.all.iv, this.all.salt, this.all.dm, barcodeData.text);
          this.qrValue = JSON.parse(this.qrValue)
          this.eventlog.fbevent('qr_payment',{ userID: this._obj.userID,Message:'QR Payment'});
          this.navCtrl.push(Payment, {
            data: this.qrValue
          });
        } catch (e) {
          this.toastInvalidQR();
        }
      }
      // else if (barcodeData.code == "0012") {
      //   this.util.SessiontimeoutAlert(barcodeData.desc);
      // }
      else {
        this.navCtrl.setRoot(WalletPage);
        this.toastCtrl.create({ message: 'Cancelling scan...', duration: 2000, dismissOnPageChange: false }).present();
        this.buttonText = "Scan";
        return true;
      }
    });
  } 

  toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration:2000,
      position: 'bottom',
      dismissOnPageChange: false,
    });
  
    toast.present(toast);
  }

  getAdLink() {
    this.http.get(this.ipaddress+"/service001/getAdLink").map(res => res.json()).subscribe(result => {
      console.log("return data getAdLink =" + JSON.stringify(result));

      if (result.code == '0000') {
        this.eventlog.fbevent('advertise_success',{ userID:this._obj.userID,Message:"Advertisement"});
        let tempArray = [];

        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }

        this.advertiseData = result.dataList;

      } else {
        this.eventlog.fbevent('advertise_fail',{ userID:this._obj.userID,Message:"Advertisement"});
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }

    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      })
  }
  goSearch() {
    var param = {
      userID: this._obj.userID,
      sessionID: this._obj.sessionID,
      customerNo: '',
      durationType: this.duration,
      fromDate: this.datePipe.transform(this.fromDate, 'yyyyMMdd'),
      toDate: this.datePipe.transform(this.toDate, 'yyyyMMdd'),
      totalCount: 0,
      acctNo: this._obj.accountNo,
      currentPage: this.currentpage,
      pageSize: this.pageSize,
      pageCount: 0,
    };
    this.http.post(this.ipaddress+'/service001/getTransactionActivityList', param).map(res => res.json()).subscribe(data => {
     
      if (data.code == '0000') {
        this.eventlog.fbevent('activity_list_success',{ userID:this._obj.userID,Message:"AccountListActivity"});
        if(data.data !=null){
        this.resultdata = data.data[0];
        this.displayArray=this.resultdata.txnTypeDesc.split(',');
        this.currency='MMK';
        if(this.resultdata.accRef=="1"){
          this.activity="Wallet to Wallet Transfer";
        }
        else if(this.resultdata.accRef=="2"){
          this.activity="Merchant Payment Transfer";
        }
        else{
          this.activity="Wallet Topup Transfer";
        }
         this.transactionflag=true;
      
        }
      }
        else{
          this.eventlog.fbevent('activity_list_fail',{ userID:this._obj.userID,Message:"AccountListActivity"});
          this.transactionflag=false;
        }
  
      } ,
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        this.eventlog.fbevent('connection error',{Message:"WalletPage"});
        toast.present(toast);
       // this.loading.dismiss();
      });
     
  }
  getNotiCount(){
    console.log('User Id'+this._obj.userID);
    var param = {
      userid:this._obj.userID,
      sessionID: '',
      customerNo: '',
      totalCount: 0,
      currentPage:'',
      pageSize: '',
      pageCount: 0,
    };
    //msgCode
    this.http.post(this.ipaddress + '/service007/tokenNotiCount', param).map(res => res.json()).subscribe(data => {
      if(data.code == '0000'){
        this.tokencount=data.count;
        if(this.tokencount!=0){
          this.count=this.tokencount.toString();
          this.shownoti=true;
          console.log('NotiCount: '+this.count);
        }

      }
      
      // this.loading.dismiss();
    },
      error => {
       // //console.log("notification error=" + error.status);
        let code;

        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.loading.dismiss();
      });
  }
  removeNotiCount(){
    console.log('User Id'+this._obj.userID);
    var param = {
      userid:this._obj.userID,
      sessionID: '',
      customerNo: '',
      totalCount: 0,
      currentPage:'',
      pageSize: '',
      pageCount: 0,
    };
    //msgCode
    this.http.post(this.ipaddress + '/service007/removeNotiCount', param).map(res => res.json()).subscribe(data => {
      if(data.code == '0000'){
        this.shownoti=false;
        this.tokencount=0;
        }

      
      
      // this.loading.dismiss();
    },
      error => {
       // //console.log("notification error=" + error.status);
        let code;

        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }
  
  topup(){
    this.navCtrl.push(MobileLoginPage);
  }

}
