import { Component } from '@angular/core';
import { MenuController, ModalController, NavController, NavParams, LoadingController, PopoverController, ToastController, AlertController, Events, ActionSheetController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont'; import { DatePipe } from '@angular/common';
import { TransferDetails } from '../transfer-details/transfer-details';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { TransferPasswordPage } from '../transfer-password/transfer-password';
import { Login } from '../login/login';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
@Component({
  selector: 'page-transfer-confirm',
  templateUrl: 'transfer-confirm.html',
  providers: [CreatePopoverPage]
})
export class TransferConfirm {
  textEng: any = ["Confirmation", "Name", "Balance", "Mobile Number", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Cancel",
    "Confirm", "Reference", "Type"];
  textMyan: any = ["အတည်ပြုခြင်း", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
    "မှတ်ချက်", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "ပယ်ဖျက်မည်",
    "လုပ်ဆောင်မည်", "အကြောင်းအရာ", "အမျိုးအစား"];
  textData: string[] = [];
  font: string = '';
  userID: string = '';
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: any;
  remark: string = '';
  ipaddress: string;
  public loading;
  storageToken: any;
  token: any;
  buttonColor: string = '';
  flag:boolean=false;
  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "reference": "", "toAcc": "",
    "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2",
    "sKey": "", fromName: "", toName: "", "wType": "",
    "field1": "", "field2": ""
  }
  resData: any;
  tosKey: any;
  passPassword:any;
  passTemp: any;
  passSKey: any;
  fromPage: any;
  passType: any;
  param: any;
  ammount: any;
  popover: any;
  password: any;
  remark_flag: boolean = false;
  appType: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public menuCtrl: MenuController, public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public sqlite: SQLite, public changefont: Changefont,private eventlog:EventLoggerProvider,
    public global: GlobalProvider, public events: Events, public util: UtilProvider, public platform: Platform,
    public alertCtrl: AlertController, private all: AllserviceProvider, public createPopover: CreatePopoverPage, public appCtrl: App, private firebase: FirebaseAnalytics, public popoverCtrl: PopoverController) {
    this._obj = this.navParams.get('data');
   
    console.log("Transfer Confrim" + JSON.stringify(this._obj));
    if (this._obj.remark != '' && this._obj.remark != undefined) {
      this.remark_flag = true;
    }
    this.tosKey = this.navParams.get('tosKey');
    this.ammount = this.util.formatAmount(this._obj.amount);
    this.param = this.navParams.get("messageParam");
    this.passPassword = this.navParams.get("psw");
    console.log("toskey >>" + JSON.stringify(this.tosKey));
    console.log("param >>" + JSON.stringify(this.param));
    console.log("password >>" + JSON.stringify(this.passPassword));
    // this.storage.get('ipaddressWallet').then((ip) => {
    //   if (ip !== undefined && ip !== "" && ip != null) {
    //     this.ipaddress = ip;
    //   } else {
    //     this.ipaddress = this.global.ipaddressWallet;
    //   }
    // });
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });
   
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('firebaseToken').then((firebaseToken) => {
      this.storageToken = firebaseToken;
      console.log('Storage Login Token: ' + this.storageToken);
    });
  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
  goBack() {
    this.navCtrl.pop();
  }

  // checkPsw() {
  //   let temp: any;
  //   if (this.passPassword == 'true') {
  //     this.navCtrl.push(TransferPasswordPage, {
  //       messageParam: this.param,
  //       tosKey: this.tosKey,
  //       data: this._obj,
  //     })
  //   } else {
  //     this.goPost();
  //   }
  // }
  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }
  presentPopoverContact(ev) {
    this.flag=true;
    //this.buttonColor = '#D8D8D8';
    console.log('Change background color: '+this.buttonColor);
    this.popover = this.popoverCtrl.create(TransferPasswordPage, { data: ev }, { cssClass: 'contact-popover' });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != undefined && data != null && data != '') {
       // this.buttonColor='';
       this.flag=false;
        this.password = data;
        console.log('Confrim Password Log: ' + this.password);
        this.goPost();
      }
      else{
        //this.buttonColor='';
        this.flag=false;
      }
    });
  }

  goPost() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
    let benePhone = this._obj.beneficiaryID;//this.util.removePlus(this._obj.beneficiaryID);
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs();
    let param = {
      token: this._obj.sessionID,
      senderCode: this._obj.userID,
      receiverCode: this._obj.beneficiaryID,
      fromName: this._obj.fromName,
      toName: this._obj.toName,
      amount: amount,
      prevBalance: this._obj.balance,
      narrative: this._obj.remark,
      "password": this.all.getEncryptText(iv, salt, dm, this.password),
      "iv": iv, "dm": dm, "salt": salt,
      "appType": "wallet"
    };
    this.http.post(this.ipaddress + '/payment/goWalletTransfer', param).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
      };
      let code = data.code;
      if (code == '0000') {
        this.eventlog.fbevent('transfer_confrim_success',{ userID: this._obj.userID,Message:data.desc});
        // if (this.storageToken != null) {
        //   this.token = this.storageToken;
        // }
        // else {
        //   this.token = "";
        // }
        this.resData = data;
       
        this.appCtrl.getRootNav().setRoot(TransferDetails, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName
        });

       this.loading.dismiss();

      }
      else if (data.code == "0016") {
        this.eventlog.fbevent('transfer_confrim_fail',{ userID: this._obj.userID,Message:data.desc});
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                // // this.storage.remove('phonenumber');
                // this.firebase.logEvent('agent_transfer_fail', { Receiver:this._obj.beneficiaryID,ErrorMessage:"agent transfer fail",Code:data.code})
                // .then((res: any) => { console.log(res); })
                // .catch((error: any) => console.error(error));
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.resData = [];
        this.eventlog.fbevent('transfer_confrim_fail',{ userID: this._obj.userID,Message:data.desc});
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.loading.dismiss();

      }
    },
      error => {
        ////console.log("error=" + error.status);
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    );
  }
  goConfrim() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
    let benePhone = this._obj.beneficiaryID;//this.util.removePlus(this._obj.beneficiaryID);
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs();
    let param = {
      token: this._obj.sessionID,
      senderCode: this._obj.userID,
      receiverCode: this._obj.beneficiaryID,
      fromName: this._obj.fromName,
      toName: this._obj.toName,
      amount: amount,
      prevBalance: this._obj.balance,
      narrative: this._obj.remark,
      "password": "",
      "iv": iv, "dm": dm, "salt": salt,
      "appType": "wallet"
    };
    this.http.post(this.ipaddress + '/payment/goWalletTransfer', param).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
      };
      let code = data.code;
      if (code == '0000') {
        this.eventlog.fbevent('transfer_confrim_success',{ userID: this._obj.userID,Message:data.desc});
        // if (this.storageToken != null) {
        //   this.token = this.storageToken;
        // }
        // else {
        //   this.token = "";
        // }
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(TransferDetails, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName
        });

        this.loading.dismiss();

      }
      else if (data.code == "0016") {
        this.eventlog.fbevent('transfer_confrim_fail',{ userID: this._obj.userID,Message:data.desc});
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                // // this.storage.remove('phonenumber');
                // this.firebase.logEvent('agent_transfer_fail', { Receiver:this._obj.beneficiaryID,ErrorMessage:"agent transfer fail",Code:data.code})
                // .then((res: any) => { console.log(res); })
                // .catch((error: any) => console.error(error));
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.resData = [];
        this.eventlog.fbevent('transfer_confrim_fail',{ userID: this._obj.userID,Message:data.desc});
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.loading.dismiss();

      }
    },
      error => {
        ////console.log("error=" + error.status);
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    );
  }

}
