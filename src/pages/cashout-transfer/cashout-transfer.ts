import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SQLite } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { AlertController, App, Events, LoadingController, MenuController, ModalController, NavController, NavParams, Platform, PopoverController, ToastController, ViewController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
@Component({
  selector: 'page-cashout-transfer',
  templateUrl: 'cashout-transfer.html',
  providers: [CreatePopoverPage],
})
export class CashoutTransferPage {
  textEng: any = ["Transfer to Agent", "Name", "Balance", "Mobile Number", "Institution", "Amount",
  "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Scan QR",
  "Mobile Wallet", "Type", "Transfer To", "Reference", "Account Number",
  "Invalid Amount."];
textMyan: any = ["ငွေလွှဲရန်", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
  "မှတ်ချက်", "မှားယွင်းနေပါသည်", "Does not match! Please try again.", "Please insert name field!", "ငွေပေးချေ",
  "Mobile Wallet", "အမျိုးအစား", "ငွေလွှဲရန်", "အကြောင်းအရာ", "အကောင့်နံပါတ်",
  "ငွေပမာဏ ရိုက်ထည့်ပါ"];
textData: string[] = [];
type: any = { name: "mWallet", code: "mWallet" };
institute: any = { name: "", code: "" };
address: string = '';
userID: string = '';
balance: string = '';
beneficiaryID: string = '';
toInstitutionCode: string = '';
amount: any;
remark: string = '';
ipaddress: string = '';
font: string = '';
lan: any;
qrValue: any = [];
popover: any;
loading: any;
isChecked: boolean = false;
_obj = {
  userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
  frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
  bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
  fromName: "", toName: "", "wType": "",
  field1: "", field2: ""
}
errormsg1: any;
errormsg2: any;
temp: any;
normalizePhone: any;
//contact : any;
accountList: any;
accNumber: string = '';
contact: any = { 'phone': '', 'name': '' };
keyboardfont: any;
public alertPresented: any;
isLoading: any;
txnType: any;
btnflag: boolean;
phone: any;
constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
  public menuCtrl: MenuController, public modalCtrl: ModalController, public http: Http,
  public datePipe: DatePipe, public loadingCtrl: LoadingController,
  public toastCtrl: ToastController, public sqlite: SQLite,
  public changefont: Changefont,
  public barcodeScanner: BarcodeScanner,
  private all: AllserviceProvider,
  public events: Events, public alertCtrl: AlertController,
  public popoverCtrl: PopoverController, public platform: Platform,
  public util: UtilProvider, public global: GlobalProvider,
  public viewCtrl: ViewController, private slimLoader: SlimLoadingBarService,private firebase: FirebaseAnalytics,
  public appCtrl: App, public createPopover: CreatePopoverPage) {
  this.storage.get('userData').then((userData) => {
    this._obj = userData;
    if (userData != null || userData != '') {
      this.balance = userData.balance;
    }
  });
  //this.checkWallet();

  let institution = this.util.setInstitution();
  this._obj.frominstituteCode = institution.fromInstitutionCode;
  this._obj.toInstitutionCode = institution.toInstitutionCode;
  this._obj.toInstitutionName = institution.toInstitutionName;
  this.institute.name = this._obj.toInstitutionName;
  this.institute.code = this._obj.toInstitutionCode;

  this.storage.get('phonenumber').then((phonenumber) => {
    this._obj.userID = phonenumber;
  });

  this.storage.get('ipaddressWallet').then((ip) => {
    if (ip !== undefined && ip != null && ip !== "") {
      this.ipaddress = ip;
      this.getAccountList();
    } else {
      this.ipaddress = this.global.ipaddressWallet;
      this.getAccountList();
    }
  });

  //this.temp = this.navParams.get('param');
  //this.phone = this.navParams.get('phone');
  //this.checkWallet();
  console.log("phone", JSON.stringify(this.phone));

  this.storage.get('username').then((username) => {
    if (username !== undefined && username !== "") {
      this._obj.fromName = username;
    }
  });
  this.events.subscribe('username', username => {
    if (username !== undefined && username !== "") {
      this._obj.fromName = username;
    }
  })

  this.events.subscribe('changelanguage', lan => {
    this.changelanguage(lan);
  });
  this.storage.get('language').then((font) => {
    this.changelanguage(font);
  });

  this.events.subscribe('changeFont', font => {
    this.keyboardfont = font;
  });

  this.storage.get('font').then((font) => {
    if (font == "zg") {
      this.keyboardfont = 'zg';
    } else {
      this.keyboardfont = 'uni';
    }
  });
}

ionViewDidLoad() {

}

ionViewCanEnter() {

}

checkWallet() {
  let num = /^[0-9-\+]*$/;
  let flag = false;

  if (num.test(this.phone)) {
    if (this.phone.indexOf("+") == 0 && (this.phone.length == 12 || this.phone.length == 13 || this.phone.length == 11)) {
      //this.walletIDMsg = '';
      flag = true;
    }
    else if (this.phone.indexOf("7") == 0 && this.phone.length == 9) {
      //this.walletIDMsg = '';
      this.phone = '+959' + this.phone;
      flag = true;
    }
    else if (this.phone.indexOf("9") == 0 && this.phone.length == 9) {
      //this.walletIDMsg = '';
      this.phone = '+959' + this.phone;
      flag = true;
    }
    else if (this.phone.indexOf("09") == 0 && (this.phone.length == 10 || this.phone.length == 11 || this.phone.length == 9)) {
      //this.walletIDMsg = '';
      this.phone = '+959' + this.phone.substring(2);
      flag = true;
    }
    else if (this.phone.indexOf("+") != 0 && this.phone.indexOf("7") != 0 && this.phone.indexOf("9") != 0 && (this.phone.length == 8 || this.phone.length == 9 || this.phone.length == 7)) {
      //this.walletIDMsg = '';
      this.phone = '+959' + this.phone;
      flag = true;
    }
    else if (this.phone.indexOf("959") == 0 && (this.phone.length == 11 || this.phone.length == 12 || this.phone.length == 10)) {
      //this.walletIDMsg = '';
      this.phone = '+959' + this.phone.substring(3);
      flag = true;
    }
    else {
      flag = false;
      //this.walletIDMsg = this.textError[3];
    }
  }
  console.log("phno==", JSON.stringify(this.phone))
  if (flag) {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    //this.walletIDMsg = '';
    let param = { userID: this._obj.userID, sessionID: this._obj.sessionID, loginID: this.phone };
    console.log("CheckWallet Data in WalletTopup is: " + JSON.stringify(param));

    this.http.post(this.ipaddress + '/chatservice/checkPhoneNo', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        this.isChecked = true;
        this._obj.toName = data.name;
        this.loading.dismiss();
      }
      else if (data.code == "0016") {
        //this.logoutAlert(data.desc);
        this.loading.dismiss();
      }
      else {
        //this.all.showAlert('Warning!', data.desc);
        this.loading.dismiss();
      }
    },
      error => {
        // this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }
}
goScan(){
  this.barcodeScanner.scan().then((barcodeData) => {
    // this.firebase.logEvent('page_click', { userID: this._obj.userID,Message:"Scan"})
    //       .then((res: any) => { console.log(res); })
    //       .catch((error: any) => console.error(error));
    if (barcodeData.cancelled) {
        return false;        
    } 
    try {      
        this.qrValue = this.all.getDecryptText(this.all.iv, this.all.salt, this.all.dm, barcodeData.text);
        this.qrValue = JSON.parse(this.qrValue);
        console.log("QR Value" +this.qrValue);
        /* this.navCtrl.push(CashoutTransferConfirmPage, {
          data: this.qrValue
        }); */
    } catch (e) {
      this.toastInvalidQR();
     
    }
  }, (err) => {
  });
}
toastInvalidQR() {
  let toast = this.toastCtrl.create({
    message: "Invalid QR Type.",
    duration: 3000,
    position: 'bottom',
    dismissOnPageChange: true,
  });

  toast.present(toast);
}


presentPopover(ev) {
  this.createPopover.presentPopover(ev);
}

typeChange(s) {
  this.type = s;
  this.type.name = s.name;
  this.type.code = s.code;
}

setAccNumber(accNum) {
  for (let i = 0; i < this.accountList.length; i++) {
    if (accNum == this.accountList[i].value) {
      this.accNumber = this.accountList[i].accNumber;
    }
  }
}

getAccountList() {
  this.slimLoader.start(() => { });
  let userID1 = this._obj.userID;//this._obj.userID.replace(/\+/g, '');

  let parameter = { userID: userID1, sessionID: this._obj.sessionID };

  this.http.post(this.ipaddress + '/service001/getAccountList', parameter).map(res => res.json()).subscribe(result => {
    if (result != null && result.code != null && result.code == "0000") {
      let tempArray = [];

      if (!Array.isArray(result.accountList)) {
        tempArray.push(result.accountList);
        result.accountList = tempArray;
      }

      this.accountList = result.accountList;
      this.accNumber = this.accountList[0].accNumber;

      this.slimLoader.complete();
    } else {
      let toast = this.toastCtrl.create({
        message: result.desc,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
      this.slimLoader.complete();
    }
  },
    error => {
      /* ionic App error
       ............001) url link worng, not found method (404)
       ........... 002) server not response (500)
       ............003) cross fillter not open (403)
       ............004) server stop (-1)
       ............005) app lost connection (0)
       */
      let code;
      if (error.status == 404) {
        code = '001';
      }
      else if (error.status == 500) {
        code = '002';
      }
      else if (error.status == 403) {
        code = '003';
      }
      else if (error.status == -1) {
        code = '004';
      }
      else if (error.status == 0) {
        code = '005';
      }
      else if (error.status == 502) {
        code = '006';
      }
      else {
        code = '000';
      }

      let msg = "Can't connect right now. [" + code + "]";

      let toast = this.toastCtrl.create({
        message: msg,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
      this.slimLoader.complete();
    });
}

instituteChange(s) {
  this._obj.toInstitutionName = s.toInstitutionName;
  this._obj.toInstitutionCode = s.toInstitutionCode;
}

changelanguage(font) {
  if (font == "eng") {
    this.font = "";
    for (let i = 0; i < this.textEng.length; i++) {
      this.textData[i] = this.textEng[i];
    }
  }
  else if (font == "zg") {
    this.font = "zg";
    for (let i = 0; i < this.textMyan.length; i++) {
      this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
    }

  }
  else {
    this.font = "uni";
    for (let i = 0; i < this.textMyan.length; i++) {
      this.textData[i] = this.textMyan[i];
    }

  }
}

onChange(s, i) {
  if (i == "09") {
    this._obj.beneficiaryID = "+959";
  }
  else if (i == "959") {
    this._obj.beneficiaryID = "+959";
  }
}

ionViewWillEnter() {
  this.phone = this.navParams.get('phone');
  console.log("Agent Transfer " + JSON.stringify(this.phone));
  // this._obj.toName = this.navParams.get('name');
  // console.log("Agent Transfer " + JSON.stringify(this._obj.toName));
}

ionViewDidEnter() {
  this.checkWallet();

}



showAlert(titleText, subTitleText) {
  if (this.alertPresented == undefined) {
    let alert = this.alertCtrl.create({
      title: titleText,
      subTitle: subTitleText,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.alertPresented = undefined;
          }
        }
      ],
      enableBackdropDismiss: false
    });

    this.alertPresented = alert.present();
    setTimeout(() => alert.dismiss(), 2000 * 60);
  }
}

getError(error) {
  /* ionic App error
   ............001) url link worng, not found method (404)
   ........... 002) server not response (500)
   ............003) cross fillter not open (403)
   ............004) server stop (-1)
   ............005) app lost connection (0)
   */
  let code;
  if (error.status == 404) {
    code = '001';
  }
  else if (error.status == 500) {
    code = '002';
  }
  else if (error.status == 403) {
    code = '003';
  }
  else if (error.status == -1) {
    code = '004';
  }
  else if (error.status == 0) {
    code = '005';
  }
  else if (error.status == 502) {
    code = '006';
  }
  else {
    code = '000';
  }
  let msg = "Can't connect right now. [" + code + "]";
  let toast = this.toastCtrl.create({
    message: msg,
    duration: 5000,
    position: 'bottom',
    dismissOnPageChange: true,
  });
  toast.present(toast);
  this.isLoading = false;
  ////console.log("Oops!");
}

}
