import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FunctProvider } from '../../providers/funct/funct';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
/**
 * Generated class for the LikepersonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-likeperson',
  templateUrl: 'likeperson.html',
})
export class LikepersonPage {
  passData: any;
  photoLink: any;
  font: any;
  imglink: any;
  personData:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public funct: FunctProvider, public storage: Storage,public http: Http,
    public global: GlobalProvider
  ) {
    this.storage.get('imglink').then((imglink) => {
      if (imglink != undefined && imglink != null && imglink != '') {
        this.imglink = imglink;
      } else {
        this.imglink = this.global.digitalMedia;
      }
      this.photoLink = this.imglink + "upload/image/userProfile";
      console.log("image link=" + this.photoLink);
    });
    this.passData = this.navParams.get("data");
    console.log("comment person pass data == " + JSON.stringify(this.passData));
    this.passData = this.navParams.get("data"); 
      if(this.passData !=null && this.passData !=undefined && this.passData !=''){
        this.getData(this.passData);
      }  
    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.photoLink = profileImage;
      } else {
        this.photoLink = this.global.digitalMediaProfile;
      }
    }); 
    this.storage.get('language').then((font) => {
      if (font != "zg")
        this.font = 'uni';
      else
        this.font = font;
    });
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad LikepersonPage');
  }
  getData(skey) {
    console.log("request Like Count:" +JSON.stringify(skey));
    this.http.get(this.global.ipaddressCMS + '/serviceArticle/getCommentLikePerson?key=' + skey).map(res => res.json()).subscribe(
      data => {
        console.log("response Like Count:" +JSON.stringify(data.data.length));
        if (data.state && data.data.length > 0) {
          for (let i = 0; i < data.data.length; i++) {
            this.personData.push({
              name: data.data[i].username,
              img: data.data[i].t16
            })
          }
        }
      },
      error => { },
      () => { }
    );
  } 

}
