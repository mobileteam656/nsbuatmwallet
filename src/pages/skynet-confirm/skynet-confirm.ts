import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, App } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
//import { SkynetOtpPage } from '../skynet-otp/skynet-otp';
import { SkynetSuccessPage } from '../skynet-success/skynet-success';
//import { SkynetErrorPage } from '../skynet-error/skynet-error';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
    selector: 'page-skynet-confirm',
    templateUrl: 'skynet-confirm.html',
    providers: [ChangelanguageProvider]
})
export class SkynetConfirmPage {

    textMyan: any = [
        "အတည်ပြုခြင်း", "",
        "ကဒ်နံပါတ်​", "ပက်​ကေ့နာမည်",
        "​ဘောက်ချာ အမျိုးအစား", "ငွေပမာဏ",
        "ဝန်​ဆောင်ခ", "စုစုပေါင်း ငွေပမာဏ",
        "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်",
        "​ဇာတ်ကားနာမည်"
    ];
    textEng: any = [
        "Confirmation", "",
        "Your Card No.", "Package Name",
        "Voucher Type", "Amount",
        "Bank Charges", "Total Amount",
        "CANCEL", "CONFIRM",
        "Movie Name"
    ];
    showFont: any = [];
    font: any = '';

    merchantObj: any = {};
    passOtp: any;
    passSKey: any;
    storageToken:any;
    token:any;

    amount: any;
    commissionAmount: any;
    totalAmount: any;

    ipaddress: string;

    userdata: any;
    loading: any;

    constructor(
        public navCtrl: NavController, public navParams: NavParams,
        public events: Events, public http: Http,
        public loadingCtrl: LoadingController, public changefont: Changefont,
        private storage: Storage, public util: UtilProvider,
        public alertCtrl: AlertController, public all: AllserviceProvider,
        public changeLanguage: ChangelanguageProvider, public global: GlobalProvider,
        private firebaseAnalytics: FirebaseAnalytics,public appCtrl: App
    ) {
        this.merchantObj = this.navParams.get("data");
        //this.passOtp = this.navParams.get("otp");
        //this.passSKey = this.navParams.get('sKey');

        this.amount = this.util.formatAmount(this.merchantObj.amount);
        this.commissionAmount = this.util.formatAmount(this.merchantObj.commissionAmount);
        this.totalAmount = this.util.formatAmount(this.merchantObj.totalAmount);

        this.events.subscribe('changelanguage', lan => {
            this.changelanguage(lan.data)
        });
        
        this.storage.get('language').then((font) => {
            this.changelanguage(font);
        });
        this.storage.get('firebaseToken').then((firebaseToken) => {
            this.storageToken = firebaseToken;
            console.log('Storage Login Token: '+this.storageToken);
          });

        this.storage.get('userData').then((data) => {
            this.userdata = data;

            this.storage.get('ipaddress').then((result) => {
                if (result == null || result == '') {
                    this.ipaddress = this.global.ipaddress;
                }
                else {
                    this.ipaddress = result;
                }
            });
        });
    }

    changelanguage(font) {
        if (font == "eng") {
            this.font = "";
            for (let i = 0; i < this.textEng.length; i++) {
                this.showFont[i] = this.textEng[i];
            }
        }
        else if (font == "zg") {
            this.font = "zg";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
            }
        }
        else {
            this.font = "uni";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.showFont[i] = this.textMyan[i];
            }
        }
    }

    goTransfer() {
        this.loading = this.loadingCtrl.create({
            content: "Processing...",
            dismissOnPageChange: true
        });
        this.loading.present();

        let parameter: any;

        if (this.merchantObj.packagetype != 'ppv') {
            parameter = {
                token: this.userdata.sessionID,
                senderCode: this.userdata.userID,
                merchantID:this.merchantObj.merchantType,
                fromName:this.userdata.name, 
                toName:this.merchantObj.toName,
                currentAmount: this.merchantObj.amount,
                bankCharges: this.merchantObj.commissionAmount,
                amount: this.merchantObj.totalAmount,
                cardNo: this.merchantObj.cardNo,
                packageName: this.merchantObj.packageName,
                voucherType: this.merchantObj.voucherType,
                subscriptionno: this.merchantObj.subscriptionno,
                packagetype: this.merchantObj.packagetype,
            };
        } else {
            parameter = {
                token: this.userdata.sessionID,
                senderCode: this.userdata.userID,
                merchantID: this.merchantObj.merchantType,
                fromName:this.userdata.name, 
                toName:this.merchantObj.toName,
                currentAmount: this.merchantObj.amount,
                bankCharges: this.merchantObj.commissionAmount,
                amount: this.merchantObj.totalAmount,
                cardNo: this.merchantObj.cardNo,
                subscriptionno: this.merchantObj.subscriptionno,
                packagetype: this.merchantObj.packagetype,
                moviename: this.merchantObj.moviename,
                moviecode: this.merchantObj.moviecode,
                startdate: this.merchantObj.startdate,
                enddate: this.merchantObj.enddate,
                usage_service_catalog_identifier__id: this.merchantObj.usage_service_catalog_identifier__id
            };
        }
        this.http.post(this.ipaddress +'/payment/goMerchantPayment', parameter).map(res => res.json()).subscribe(
            data1 => {
                let responseparam: any;
                if (data1.code == "0000") {
                    this.loading.dismiss();
                    if(this.storageToken!=null){
                        this.token=this.storageToken;
                      }
                      else{
                        this.token="";
                      }
                    if (this.merchantObj.packagetype != "ppv") {
                        responseparam = { 
                            token: this.userdata.sessionID,
                            senderCode: this.userdata.userID,                           
                            cardNo: this.merchantObj.cardNo,
                            packageName: this.merchantObj.packageName,
                            voucherType: this.merchantObj.voucherType,
                            amount: this.merchantObj.amount,
                            commissionAmount: this.merchantObj.commissionAmount,
                            totalAmount: this.merchantObj.totalAmount,
                            ccy: this.merchantObj.ccy,
                            bankRefNo: data1.bankRefNumber,
                            transDate: data1.transactionDate,
                            merchantID: this.merchantObj.merchantType,
                            packagetype: this.merchantObj.packagetype,
                            errordesc:"",
                            contactName: this.merchantObj.contactName,
                            receiverCode: this.merchantObj.wlPhone
                        };
                    } else {
                        responseparam = {
                            token: this.userdata.sessionID,
                            senderCode: this.userdata.userID,
                            cardNo: this.merchantObj.cardNo,
                            amount: this.merchantObj.amount,
                            commissionAmount: this.merchantObj.commissionAmount,
                            totalAmount: this.merchantObj.totalAmount,
                            ccy: this.merchantObj.ccy,
                            bankRefNo: data1.bankRefNumber,
                            transDate: data1.transactionDate,
                            merchantID: this.merchantObj.merchantType,
                            moviename: this.merchantObj.moviename,
                            packagetype: this.merchantObj.packagetype,
                            errordesc:"",
                            contactName: this.merchantObj.contactName,
                            receiverCode: this.merchantObj.wlPhone
                        };
                    }
                    this.navCtrl.setRoot(SkynetSuccessPage, {
                        data: responseparam
                    });
                }
                else if (data1.code == "0016") {
                    this.loading.dismiss();
                } else if (data1.code == "0012") {
                    let confirm = this.alertCtrl.create({
                        title: 'Warning!',
                        enableBackdropDismiss: false,
                        message: data1.desc,
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.storage.remove('username');
                                    this.storage.remove('userData');
                                    this.storage.remove('firstlogin');
                                    this.appCtrl.getRootNav().setRoot(Login);
                                }
                            }
                        ]
                    })
                    confirm.present();
                    this.loading.dismiss();

                }else{
                    responseparam = {
                        errordesc: data1.desc,
                    }                        
                    this.navCtrl.setRoot(SkynetSuccessPage, {
                        data: responseparam
                    });
                }
                    /* this.navCtrl.setRoot(SkynetErrorPage, {
                        data: paramSkynetError
                    }); */
            },
            error => {
                this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                this.loading.dismiss();

                let code = error.status;
                let msg = "Can't connect right now. [" + code + "]";

                // this.firebaseAnalytics.logEvent('topupSkynet_userid_response', {
                //     "userid": this.userdata.userID,
                //     "error": msg
                // }).then((res: any) => {
                //     console.log(res);
                // }).catch((error: any) => {
                //     console.log(error);
                // });
            }
        );
    }

    cancel() {
        this.navCtrl.pop();
    }

    logoutAlert(message) {
        let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: message,
            buttons: [{
                text: 'OK',
                handler: () => {
                    this.storage.remove('userData');
                    this.events.publish('login_success', false);
                    this.events.publish('lastTimeLoginSuccess', '');
                    this.navCtrl.setRoot(Login, {});
                    this.navCtrl.popToRoot();
                }
            }],
            cssClass: 'warningAlert',
        })
        confirm.present();
    }

    // removeComma(amount) {
    //     return amount.replace(/[,]/g, '');
    // }

}
