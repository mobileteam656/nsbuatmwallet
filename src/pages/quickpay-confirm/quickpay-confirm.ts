import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { QuickpaySuccessPage } from '../quickpay-success/quickpay-success';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { Login } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
@Component({
  selector: 'page-quickpay-confirm',
  templateUrl: 'quickpay-confirm.html',
})
export class QuickpayConfirmPage {

  textMyan: any = ["Bill Payment Confirm", "Merchant Name", "Customer Reference No", "Company/Customer Name", "ငွေပမာဏ", "ပယ်ဖျက်မည်", "ပေးမည်", "အကောင့်", "Receiver Wave အကောင့်", "အမည်", "ငွေပမာဏ", "မှတ်ပုံတင်", "ဖုန်းနံပါတ်", "ကျေးဇူးပြု၍ အချက်အလက်များ ဖြည့်စွက်ပါ။", "အကြောင်းအရာ","Depositor Name"];
  textEng: any = [ "Bill Payment Confirm", "Merchant Name", "Customer Reference No", "Company/Customer Name", "Amount","Cancel" , "Pay","Account","Receiver Wave Account No","Name","Amount","NRC","Phone No","Please fill this field.","Narrative","Depositor Name","Customer Name","Agent/Booking ID","Customer Phone","Service"];
  showFont: string[] = [];
  billerid = "";

  userData: any;
  errormsg: any;
  refNo: any;
  name: any;
  amount: any;
  selectAccount: any;
  resData: any;
  ipaddress: any;
  tosKey: any;
  balance: string = '';
  phno: any;

  _data = {
    refNo: "", name: "", amount: "", account: "", waveAcc: "", nrc: "", phoneNo:"+959784412809", description:"", waveNo:"+959788945976", oneNo:"+9595175475", bnfphone :"+9595175475", bnfname: "", bnfId:"", bphone:"", bnfservice:""
  }

  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "reference": "", "toAcc": "",
    "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2",
    "sKey": "", fromName: "", toName: "", "wType": "",
    "field1": "", "field2": ""
  }
  _obj1 = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: ""
  }
  public loading;
  constructor(private firebase: FirebaseAnalytics,public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage, public loadingCtrl: LoadingController, public http: Http,public util: UtilProvider,
  public alertCtrl: AlertController, public toastCtrl: ToastController, public appCtrl: App, public global: GlobalProvider,) {

    this.storage.get('userData').then((userData) => {
      this._obj1 = userData;
      console.log("user id" + JSON.stringify(this._obj1));
      // if (userData != null || userData != '') {
      //   this.balance = userData.balance;
      // }
    });

    this.storage.get('ipaddressWallet').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressWallet;
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.billerid = this.navParams.get("billerid");   

  }
  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickpayPage');
  }

  goCancel(){
    this.navCtrl.pop();
  }

  goPay(){
    if(this._data.refNo == "" || this._data.name == "" || this._data.amount == ""){
      this.errormsg = this.showFont[13];
    }else{
      this.errormsg = "";
      this.navCtrl.push(QuickpaySuccessPage,{
      billerid : this.billerid, 
      data: this._data 
      });
    }     
  }

  goPost() {
    if(this.billerid=="ABC Telecomm"){
      this.phno=this._data.phoneNo;
    }else if(this.billerid=="One Stop Mart"){
      this.phno=this._data.oneNo;
    }else if(this.billerid=="Wave Money"){
      this.phno=this._data.waveNo;
    }else if(this.billerid=="BNF Express"){
      this.phno=this._data.bnfphone;
    }
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this._obj1.userID;//this.util.removePlus(this._obj.userID);
    let benePhone = this._obj.beneficiaryID;//this.util.removePlus(this._obj.beneficiaryID);
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    let param = {
      "userID": payeePhone, sessionID: this._obj1.sessionID,
      "payeeID": payeePhone, "beneficiaryID": this.phno, "fromInstitutionCode": this._obj.frominstituteCode, "toInstitutionCode": this._obj.toInstitutionCode,
      "toAcc": this._obj.toAcc, "reference": this._obj.reference, amount: this._data.amount, "bankCharges": this._obj.bankCharges, "commissionCharges": this._obj.commissionCharges, "remark": this._obj.remark, "transferType": "1",
      "sKey": this._obj.sKey,
      "tosKey": this.tosKey,
      "fromName": this._obj.fromName,
      "toName": this._obj.toName,
      "wType": this._obj.wType,
      "field1": "", "field2": ""
    };
    console.log("request for transferOut in ad=" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service001/transferOut', param).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNo,
        "code": data.code,
        "desc": data.desc,
        "field1": "",
        "field2": "",
        "transDate": data.transDate
      };
      console.log("response for transferOut in ad=" + JSON.stringify(data));
      let code = data.code;
      if (code == '0000') {
        this.firebase.logEvent('billpayment', {RefNo:data.bankRefNo,Message:data.desc,Code:code })
        .then((res: any) => { console.log(res); })
        .catch((error: any) => console.error(error));
        this.loading.dismiss();

        this.navCtrl.push(QuickpaySuccessPage,{
          billerid : this.billerid, 
          data: this._data, 
          obj: data
          });
  
        // let confirm = this.alertCtrl.create({
        //   title: 'Successful',
        //   enableBackdropDismiss: false,
        //   message: data.desc,
        //   buttons: [
        //     {
        //       text: 'OK',
        //       handler: () => {
              
        //       }
        //     }
        //   ]
        // })
        // confirm.present();
          
          
      }
      else if (data.code == "0016") {
        
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                // this.storage.remove('phonenumber');
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.firebase.logEvent('billpayment_fail', {RefNo:data.bankRefNo,Message:data.desc,Code:code  })
        .then((res: any) => { console.log(res); })
        .catch((error: any) => console.error(error));
        this.resData = [];
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    },
      error => {
        ////console.log("error=" + error.status);
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    );
  }

}
