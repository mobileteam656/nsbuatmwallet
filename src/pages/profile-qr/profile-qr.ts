import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController, AlertController, Events, Platform, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { BarcodeScanner,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { GlobalProvider } from '../../providers/global/global';

import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-profile-qr',
  templateUrl: 'profile-qr.html',
})
export class ProfileQrPage {

  dataValue: any = [];
  qrValue: any = [];
  popover: any;
  selectedTitle: any;
  userValue: any;

  buttonText: any;
  public loading;
  options: BarcodeScannerOptions;
  scannedText: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public events: Events, public barcodeScanner: BarcodeScanner, public alertCtrl: AlertController,
    public global: GlobalProvider, public viewCtrl: ViewController,
    public platform: Platform, public popoverCtrl: PopoverController,private _barcodeScanner: BarcodeScanner) {
    this.dataValue = this.navParams.get('data');
    this.userValue = this.navParams.get('user');
    ////console.log("request dataValue >> " + JSON.stringify(this.dataValue));
    ////console.log("request userValue >> " + JSON.stringify(this.userValue));
  }

  scanQR() {
    this.buttonText = "Loading..";
    this.loading = true;
    this.options = {
      formats: "QR_CODE",
      "prompt": " ",
    }

    this._barcodeScanner.scan(this.options).then((barcodeData) => {
      if (barcodeData.cancelled) {
        this.buttonText = "Scan";
        this.loading = false;

        return false;
      }
      this.scannedText = barcodeData.text;

      let datacheck_scan = JSON.parse(this.scannedText);
      this.navCtrl.push(ContactPage, {
        data: datacheck_scan,
        fromPage: "QR"
      });

    });

    
  
   
  }

     //this.scannedText = barcodeData.text;
    

      /*if (param == "contact") {
        try {
          let datacheck_scan = JSON.parse(this.scannedText);

          if (datacheck_scan[0] == "contacts") {
            this.navCtrl.push(ContactPage, {
              data: datacheck_scan,
              fromPage: "QR"
            });
          } else {
            //this.toastInvalidQR();
          }
        } catch (e) {
          //this.toastInvalidQR();
        }
      } 
    }, (err) => {
      ////console.log(err);
    });
  }*/
  
  /*toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }*/
}
