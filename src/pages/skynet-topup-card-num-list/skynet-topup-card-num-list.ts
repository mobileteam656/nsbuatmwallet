// import { Component } from '@angular/core';
// import { NavController, NavParams } from 'ionic-angular';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { App, Events, NavController, NavParams, Platform, PopoverController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { MerchantPaymentPage } from '../merchant-payment/merchant-payment';
import { QrUtilityPage } from '../qr-utility/qr-utility';
import { QuickpayPage } from '../quickpay/quickpay';
import { SkynetTopupPage } from '../skynet-topup/skynet-topup';


@Component({
  selector: 'page-skynet-topup-card-num-list',
  templateUrl: 'skynet-topup-card-num-list.html',
  providers: [Changefont, CreatePopoverPage]
})
export class SkynetTopupCardNumListPage {

  cadData: any;
  nores: any;
  isLoading: any;
  _obj = {
    userID: "", sessionID: "", loginID: ""
  }
  userData: any;
  ipaddress: string = '';
  popover: any;
  public loading;
  textEng: any = [
    "Payment Bill",
    "Skynet Card Beneficiary"
  ];
  textMyan: any = [
    "ငွေပေးချေမှု",
    "သိမ်းထားသောကတ်နံပါတ်များ"
  ];
  textData: any = [];
  merchant:any;

  constructor(public navCtrl: NavController, public events: Events,
    public appCtrl: App, public platform: Platform,
    public popoverCtrl: PopoverController, public global: GlobalProvider,
    public toastCtrl: ToastController, public navParams: NavParams,
    public http: Http, public util: UtilProvider,
    public storage: Storage, public createPopover: CreatePopoverPage) {

    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      if (userData != null || userData != '') {
        //  this.balance = userData.balance;
      }
      this.merchant = this.navParams.get('param');
      

      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
      });
      this.isLoading = true;
      this.storage.get('ipaddress').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
          this.getCardNo();
        }
        else {
          this.ipaddress = this.global.ipaddress;
          this.getCardNo();
        }
      });
      
    });
  }

  getCardNo() {
       let param = {
      userID: this._obj.userID,
      sessionID: this._obj.sessionID,
      }
    this.http.post(this.ipaddress + "/payment/getCardNo", param).map(res => res.json()).subscribe(result => {
      if (result.cadData == undefined || result.cadData == null || result.cadData == '') {
        this.nores = 0;
      } else {
        let tempArray = [];

        if (!Array.isArray(result.cadData)) {
          tempArray.push(result.cadData);
          result.cadData = tempArray;
        }
        if (result.cadData.length > 0) {
          this.nores = 1;
          this.cadData = result.cadData;
          for(let i=0;i<this.cadData.length;i++){            
            this.cadData[i].name.replace('Daw ','');
          }
        }
      }
      this.isLoading = false;

    }, error => {
      this.nores = 0;
      this.isLoading = false;
      this.getError(error);
    });
    // }
  }

  chooseCardNo(obj) {
    console.log('My data is,:'+JSON.stringify(obj));

      // this.navCtrl.pop(SkynetTopupPage, {
      //   objparam: obj,
      //   param: this.merchant
      // });
      this.navCtrl.getPrevious().data.thing1 =obj;
      this.navCtrl.pop();
  }


  getError(error) {
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.loading = false;

  }

  


  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  // goNext() {
  //   this.navCtrl.push(QuickpayPage);
  // }
}


