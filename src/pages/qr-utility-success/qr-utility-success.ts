
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { QrUtilityPage } from '../qr-utility/qr-utility'
import { WalletPage } from '../wallet/wallet';
@Component({
  selector: 'page-qr-utility-success',
  templateUrl: 'qr-utility-success.html',
})
export class QrUtilitySuccessPage {

  passTemp: any;
  passTemp1: any;
  passTemp2: any;
  textEng: any = ["From account", "To account", "Amount", "Bank Reference No.",
   "Transaction Date", "Card Expired", 
  "Transaction Approved", "Transfer Type", "Close", "Bill ID", "Reference Number", "Customer Name"];
  textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", 'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်', 'သက်တမ်းကုန်ဆုံးချိန်', 'လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'ငွေလွှဲ အမျိုးအစား', "ပိတ်မည်" , "ဘေလ်နံပါတ်" , "အမှတ်စဉ်", "အမည်" ];
  showFont: any = [];
  font: any = '';
  amount: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public changeLanguage: ChangelanguageProvider) {
    this.passTemp = this.navParams.get("data");
    this.passTemp1 = this.navParams.get("detail");
    this.passTemp2 = this.navParams.get("type");
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
  }

  ionViewDidLoad() {

  }

  getOK() {
    this.navCtrl.setRoot(WalletPage);
  }
  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }
}
