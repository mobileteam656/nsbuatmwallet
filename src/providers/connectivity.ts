import { Injectable } from '@angular/core';
//import { Network } from 'ionic-native';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';

declare var Connection;

@Injectable()
export class Connectivity {

  onDevice: boolean;

  constructor(public platform: Platform,
    public network: Network){
    this.onDevice = this.platform.is('cordova');
  }

  isOnline(): boolean {
   /*  if(this.onDevice && Network.onConnect){
      return Network.onConnect !== Connection.NONE;
    } else {
      return navigator.onLine; 
    } */
    if(this.onDevice && this.network.onConnect){
      return this.network.onConnect !== Connection.NONE;
    } else {
      return navigator.onLine; 
    }
  }

  isOffline(): boolean {
 /*    if(this.onDevice && Network.onConnect){
      return Network.onConnect === Connection.NONE;
    } else {
      return !navigator.onLine;   
    } */
    if(this.onDevice && this.network.onConnect){
      return this.network.onConnect === Connection.NONE;
    } else {
      return !navigator.onLine;   
    }
  }

}