import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class GlobalProvider {

  ipaddressCMS: any;
  ipaddressWallet: any;
  ipaddressChat: any;
  ipaddressTicket: any;

  appIDChat: any;
  appName: any = '';
  domainChat: any;
  domainCMS: any;
  ipaddress: any;
  ipaddress3: string;
  imglink: any;
  digitalMedia: any;
  digitalMediaProfile: any;
  region: string;
  regionCode: string;
  netWork: string = '';
  seen: boolean = true;
  unseen: boolean = false;
  amount: string;
  noticount=0;

  apptype: any = '';
  appCode: any;
  okDollarCode: string;
  waveMoneyCode: string;
  waveName1: string;
  okName2: string;
  nearbyIndex: any;
  DECIMAL_SEPARATOR = ".";
  GROUP_SEPARATOR = ",";
  public phone: any;
  public type: any = "android";
  public version: any = "1.0.12";
  ipaddressApp: string;

  constructor(public network: Network) {
    this.appName = "dc";
    this.appIDChat = "010";
    this.domainChat = "DC001";
    this.domainCMS = "001";
    //All
    this.region = "All";
    this.regionCode = "00000000";
    // //122
    //  this.ipaddressApp = "http://122.248.120.16:8080/AppService/module001";
    //  this.ipaddress = "http://122.248.120.16:8080/WalletService/module001";
    //  this.ipaddressCMS = "http://122.248.120.16:8080/WalletService/module001";
    //  this.ipaddressWallet = "http://122.248.120.16:8080/WalletService/module001";
    //  this.ipaddressChat = "http://122.248.120.16:8080/WalletService/module001";
    //  this.ipaddressTicket = "http://122.248.120.16:8080/WalletService/module001";                                                                                   
    //  this.digitalMedia = "http://122.248.120.16:8080/WalletService/DigitalMedia/";
    //  this.digitalMediaProfile = "http://122.248.120.16:8080/DigitalMedia/upload/image/userProfile/";
    //  this.ipaddress3 = "http://122.248.120.16:8080/chatting/api/v1/";
    //  this.imglink = "http://122.248.120.16:8080/chatting"; 
    //UAT
     this.ipaddressApp = "http://203.81.87.52:8182/AppService/module001";
     this.ipaddress     = "http://203.81.87.52:8182/WalletService/module001";
     this.ipaddressCMS  = "http://203.81.87.52:8182/WalletService/module001";
     this.ipaddressWallet ="http://203.81.87.52:8182/WalletService/module001";
     this.ipaddressTicket ="http://203.81.87.52:8182/WalletService/module001";                                                                                   
     this.digitalMedia = "http://203.81.87.52:8182/DigitalMedia/";
     this.digitalMediaProfile ="http://203.81.87.52:8182/DigitalMedia/upload/image/userProfile/";
     this.ipaddress3 ="http://203.81.87.52:8182/chatting/api/v1/";
     this.imglink = "http://203.81.87.52:8182/chatting";
    
    //cloud link
    //  this.ipaddressApp = "http://52.148.87.124:8080/AppService/module001";
    //  this.ipaddress     = "http://52.148.87.124:8080/WalletService/module001";
    //  this.ipaddressCMS  = "http://52.148.87.124:8080/WalletService/module001";
    //  this.ipaddressWallet = "http://52.148.87.124:8080/WalletService/module001";
    //  this.ipaddressTicket = "http://52.148.87.124:8080/WalletService/module001";                                                                                   
    //  this.digitalMedia = "http://52.148.87.124:8080/DigitalMedia/";
    //  this.digitalMediaProfile = "http://52.148.87.124:8080/DigitalMedia/upload/image/userProfile/";
    //  this.ipaddress3 = "http://52.148.87.124:8080/chatting/api/v1/";
    //  this.imglink = "http://52.148.87.124:8080/chatting";
    
    this.netWork = 'connected';

    this.network.onDisconnect().subscribe(() => {
      this.netWork = 'disconnected';
      console.log('network was disconnected :-(');
    });

    this.network.onConnect().subscribe(() => {
      this.netWork = 'connected';
      console.log('network connected!');
    });

    //the following may be used for mobile banking
    this.appCode = "MA"; // OK
  
  
    this.waveMoneyCode = "000700";
    this.waveName1 = "dc";
    this.okName2 = "ok";
    this.okDollarCode = "000600";
    this.apptype = "001"; // zala   
    this.phone = "01507209";

  }

  format(valString) {
    if (!valString) {
      return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)

  };

  unFormat(val) {
    if (!val) {
      return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g, '');
    if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
    } else {
      return val.replace(/\./g, '');
    }
  };

}







