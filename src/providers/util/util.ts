import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { ActionSheetController, AlertController, App, Events, LoadingController, MenuController, NavController, NavParams, Platform, PopoverController, ToastController, ViewController } from 'ionic-angular';
import { Login } from '../../pages/login/login';
/*
  Generated class for the UtilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
declare var require: any
let num = /^[0-9-\+]*$/;
let flag: boolean;
let phoneData = { phone: "", flag: false }
let normalizePhOld;
let mnum = /^[၀-၉-\+]*$/;
let institutionData = { fromInstitutionName: "", toInstitutionName: "", fromInstitutionCode: "", toInstitutionCode: "" }
@Injectable()
export class UtilProvider {

  constructor(public http: Http, public global: GlobalProvider,public appCtrl: App,public alertCtrl:AlertController,public storage: Storage) {
    //console.log('Hello UtilProvider Provider');
  }

  normalizePhOld(phone) {
    //if (phone != "" && phone != null && phone != undefined) {
    if (num.test(phone)) {
      if (phone.indexOf("+") == 0 && (phone.length == "12" || phone.length == "13" || phone.length == "11")) {
        flag = true;
        normalizePhOld = phone;
      }
      else if (phone.indexOf("7") == 0 && phone.length == "9") {
        normalizePhOld = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("9") == 0 && phone.length == "9") {
        normalizePhOld = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("09") == 0 && (phone.length == 10 || phone.length == 11 || phone.length == 9)) {
        normalizePhOld = '+959' + phone.substring(2);
        flag = true;
      }
      else if (phone.indexOf("7") != 0 && phone.indexOf("9") != 0 && (phone.length == "8" || phone.length == "9" || phone.length == "7")) {
        normalizePhOld = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("959") == 0 && (phone.length == 11 || phone.length == 12 || phone.length == 10)) {
        normalizePhOld = '+959' + phone.substring(3);
        flag = true;
      } else if (phone.length >= 7 && phone.length <= 9) {
        normalizePhOld = '+959' + phone;
        flag = true;
      }
      else {
        normalizePhOld = '';
        flag = false;
      }
    } else {
      normalizePhOld = '';
      flag = false;
    }
    phoneData.flag = flag;
    phoneData.phone = normalizePhOld;
    //console.log("normalizePhOld-Provider-phoneData>>" + JSON.stringify(phoneData));
    return phoneData;

  }
  removePlus(n) {
    return n.replace(/\+/g, '');
  }


  normalizeMyanPhone(phone) {
    if (phone != "" && phone != null && phone != undefined) {
      if (num.test(phone)) {
        if (phone.indexOf("+") == 0 && (phone.length == "12" || phone.length == "13" || phone.length == "11")) {
          flag = true;
          normalizePhOld = phone;
        }
        if ((phone.length == "12" || phone.length == "13" || phone.length == "11")) {
          flag = true;
        }
        else if (phone.indexOf("၇") == 0 && phone.length == "9") {
          normalizePhOld = '+၉၅၉' + phone;
          flag = true;
        }
        else if (phone.indexOf("၉") == 0 && phone.length == "9") {
          normalizePhOld = '+၉၅၉' + phone;
          flag = true;
        }
        else if (phone.indexOf("၇") != 0 && phone.indexOf("၉") != 0 && (phone.length == "8" || phone.length == "9" || phone.length == "7")) {
          normalizePhOld = '+၉၅၉' + phone;
          flag = true;
        }
        else if (phone.indexOf("၀၉") == 0 && (phone.length == 10 || phone.length == 11 || phone.length == 9)) {
          normalizePhOld = '+၉၅၉' + phone.substring(2);
          flag = true;
        }
        else if (phone.indexOf("၉၅၉") == 0 && (phone.length == 11 || phone.length == 12 || phone.length == 10)) {
          normalizePhOld = '+၉၅၉' + phone.substring(3);
          flag = true;
        }
      }
    } else {
      phone = '';
      flag = false;
    }
    let myanphone: any;
    let myanmarNumbers = require("myanmar-numbers");
    phone = myanmarNumbers(myanphone);
    //console.log(normalizePhOld);
    normalizePhOld = '+' + phone;
    //console.log(normalizePhOld)
    phoneData.flag = flag;
    phoneData.phone = normalizePhOld;
    //console.log("normalizePhOld>>" + JSON.stringify(phoneData));
    return phoneData;
  }

  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  } 

  // removePlus(n) {
  //   return n.replace(/\+/g, '');
  //}

  setInstitutionOther() {
    if (this.global.appCode == 'OK') {
      institutionData.fromInstitutionName = this.global.okName2;
      institutionData.fromInstitutionCode = this.global.okDollarCode;
      institutionData.toInstitutionName = this.global.waveName1
      institutionData.toInstitutionCode = this.global.waveMoneyCode;
    } else if (this.global.appCode == 'WM') {
      institutionData.fromInstitutionName = this.global.waveName1;
      institutionData.fromInstitutionCode = this.global.waveMoneyCode;
      institutionData.toInstitutionName = this.global.okName2;
      institutionData.toInstitutionCode = this.global.okDollarCode;
    }
    return institutionData;
  }

  setInstitution() {
    if (this.global.appCode == 'OK') {
      institutionData.fromInstitutionName = this.global.okName2;
      institutionData.fromInstitutionCode = this.global.okDollarCode;
      institutionData.toInstitutionName = this.global.okName2
      institutionData.toInstitutionCode = this.global.okDollarCode;
    } else if (this.global.appCode == 'WM') {
      institutionData.fromInstitutionName = this.global.waveName1;
      institutionData.fromInstitutionCode = this.global.waveMoneyCode;
      institutionData.toInstitutionName = this.global.waveName1;
      institutionData.toInstitutionCode = this.global.waveMoneyCode;
    }
    return institutionData;
  }

  checkTwoStringAreEqual(firstString, secondString) {
    if (firstString.trim().equal(secondString.trim())) {
      return true;
    } else {
      return false;
    }
  }

  checkInputIsEmpty(dataFromUser) {
    if (dataFromUser == undefined || dataFromUser == null || dataFromUser.trim().length == 0) {
      return true;
    } else {
      return false;
    }
  }

  checkAmountIsZero(amountFromUser) {
    let amountFloat = parseFloat(amountFromUser);

    if (amountFloat == 0) {
      return true;
    } else {
      return false;
    }
  }
  getRDatePickerDate(dt) {
    if (dt != null) {
   // var datestring = dt.date.year + ("0" + (dt.date.month)).slice(-2) + ("0" + dt.date.day).slice(-2);
    var datestring= ("0" + dt.date.day).slice(-2)+ ("0" + (dt.date.month)).slice(-2)+dt.date.year
    return datestring;
    } else {
    return "";
    }
    }

  checkAmountIsLowerThanZero(amountFromUser) {
    let amountFloat = parseFloat(amountFromUser);

    if (amountFloat < 0) {
      return true;
    } else {
      return false;
    }
  }

  checkNumberOrLetter(amountFromUser) {
    if (isNaN(amountFromUser)) {
      return true;
    } else {
      return false;
    }
  }

  checkPlusSign(amountFromUser) {
    let amountString = amountFromUser.toString();
    let result = amountString.includes("+");
    //console.log("RE checkInputIncludesPlusSign result:" + JSON.stringify(result));
    return result;
  }

  checkMinusSign(amountFromUser) {
    let amountString = amountFromUser.toString();
    let result = amountString.includes("-");
    //console.log("RE checkInputIncludesPlusSign result:" + JSON.stringify(result));
    return result;
  }

  checkStartZero(amountFromUser) {
    let amountString = amountFromUser.toString();
    let result = amountString.startsWith("0");
    //console.log("RE checkInputIncludesPlusSign result:" + JSON.stringify(result));
    return result;
  }

  checkOnlyNumber(amountFromUser) {
    var reg = new RegExp(/^\d+$/);
    let result = reg.test(amountFromUser);
    //console.log("RE checkInputIsOnlyNumber result:" + JSON.stringify(result));
    return result;
  }

  checkOnlyEng(amountFromUser) {
    var reg = new RegExp(/^[a-z0-9 ]*$/i);
    let result = reg.test(amountFromUser);
    return result;     
  }
 
  getImageName() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + ""
      + (currentdate.getMonth() + 1) + ""
      + currentdate.getFullYear() + ""
      + currentdate.getHours() + ""
      + currentdate.getMinutes() + ""
      + currentdate.getSeconds();
    return datetime;
  }

  /* gettting today date and time.
  return example is like that -> "14/5/2018 @ 14:32:40" */
  getTodayDateAndTime() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
      + (currentdate.getMonth() + 1) + "/"
      + currentdate.getFullYear() + " @ "
      + currentdate.getHours() + ":"
      + currentdate.getMinutes() + ":"
      + currentdate.getSeconds();
    return datetime;
  }

  /* normalizePhone(phoneNumber) {
    let normalizePhone;
    let regExpForPhone = /^[+]{0,1}[0-9]{7,12}$/;

    if (phoneNumber != undefined && phoneNumber != null && phoneNumber != "" && regExpForPhone.test(phoneNumber)) {
      if (phoneNumber.indexOf("09") == 0 && phoneNumber.length >= 9 && phoneNumber.length <= 11) {
        normalizePhone = "+95" + phoneNumber.substring(1);
        flag = true;
      } else if (phoneNumber.indexOf("9") == 0 && phoneNumber.length >= 8 && phoneNumber.length <= 10) {
        normalizePhone = "+95" + phoneNumber;
        flag = true;
      } else if (phoneNumber.indexOf("959") == 0 && phoneNumber.length >= 10 && phoneNumber.length <= 12) {
        normalizePhone = "+" + phoneNumber;
        flag = true;
      } else if (phoneNumber.indexOf("+959") == 0 && phoneNumber.length >= 11 && phoneNumber.length <= 13) {
        normalizePhone = phoneNumber;
        flag = true;
      } else if (phoneNumber.length >= 7 && phoneNumber.length <= 9) {
        normalizePhone = "+959" + phoneNumber;
        flag = true;
      } else {
        normalizePhone = '';
        flag = false;
      }
    } else {
      normalizePhone = '';
      flag = false;
    }

    phoneData.flag = flag;
    phoneData.phone = normalizePhone;
    //console.log("normalizePhone-Provider-phoneData>>" + JSON.stringify(phoneData));

    return phoneData;
  } */

  /* normalizePhone(phoneNumber) {
    let normalizePhone = "";

    if (phoneNumber != undefined && phoneNumber != null && phoneNumber != "") {
      if (phoneNumber.indexOf("09") == 0 && (phoneNumber.length == 9 || phoneNumber.length == 10 || phoneNumber.length == 11)) {
        normalizePhone = "+95" + phoneNumber.substring(1);
        flag = true;
      } else if (phoneNumber.indexOf("959") == 0 && (phoneNumber.length == 10 || phoneNumber.length == 11 || phoneNumber.length == 12)) {
        normalizePhone = "+" + phoneNumber;
        flag = true;
      } else if (phoneNumber.indexOf("+959") == 0 && (phoneNumber.length == 11 || phoneNumber.length == 12 || phoneNumber.length == 13)) {
        normalizePhone = phoneNumber;
        flag = true;
      } else if (phoneNumber.length == 7 || phoneNumber.length == 8 || phoneNumber.length == 9) {
        normalizePhone = "+959" + phoneNumber;
        flag = true;
      }
    } else {
      normalizePhone = '';
      flag = false;
    }

    phoneData.flag = flag;
    phoneData.phone = normalizePhone;
    //console.log("normalizePhone-Provider-phoneData>>" + JSON.stringify(phoneData));

    return phoneData;
  } */
  normalizePhone(phone) {
    //if (phone != "" && phone != null && phone != undefined) {
      let normalizePhone = "";
    if (num.test(phone)) {
      if (phone.indexOf("+") == 0 && (phone.length == "12" || phone.length == "13" || phone.length == "11")) {
        flag = true;
        normalizePhone = phone;
      }
      else if (phone.indexOf("7") == 0 && phone.length == "9") {
        normalizePhone = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("9") == 0 && phone.length == "9") {
        normalizePhone = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("09") == 0 && (phone.length == 10 || phone.length == 11 || phone.length == 9)) {
        normalizePhone = '+959' + phone.substring(2);
        flag = true;
      }
      else if (phone.indexOf("7") != 0 && phone.indexOf("9") != 0 && (phone.length == "8" || phone.length == "9" || phone.length == "7")) {
        normalizePhone = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("959") == 0 && (phone.length == 11 || phone.length == 12 || phone.length == 10)) {
        normalizePhone = '+959' + phone.substring(3);
        flag = true;
      }
      else {
        normalizePhone = '';
        flag = false;
      }
    } else {
      normalizePhone = '';
      flag = false;
    }
    phoneData.flag = flag;
    phoneData.phone = normalizePhone;
    // console.log("normalizePhone-Provider-phoneData>>" + JSON.stringify(phoneData));
    return phoneData;

  }
  SessiontimeoutAlert(msgDesc) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: msgDesc,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            // this.storage.remove('phonenumber');
            this.storage.remove('username');
            this.storage.remove('userData');
            this.storage.remove('firstlogin');
            this.appCtrl.getRootNav().setRoot(Login);
          }
        }
      ]
    })
    confirm.present();    
  } 
}
